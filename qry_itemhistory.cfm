<cfif j eq "ItemHistory">
	<cfset queryName = "ItemHistory">

	<cfquery name="#queryName#" datasource="#attributes.datasource#">

	SELECT
		CONVERT(varchar, AccountID) + '^' + 
		CONVERT(varchar, ItemID) + '^' + 
		CONVERT(varchar, YTDPurchaseQty) + '^' +
		CONVERT(varchar, YTDPurchaseDol, 0) + '^' +
		CONVERT(varchar, OnOrderQty) + '^' +
		CONVERT(varchar, OnOrderDol, 0) + '^' +
		CONVERT(varchar, YTDReturnQty) + '^' +
		CONVERT(varchar, YTDReturnDol, 0) + '^' +
		CONVERT(varchar, YTDOrderCount) + '^' +
		CONVERT(varchar(32),LastOrderDate)
	FROM ItemHistory
	WHERE AccountID IN (SELECT AccountID 
				FROM DistributorAccountReps 
				WHERE DistRepID = #URL.ID# AND
				DistributorID = #getDistributor.DistributorID#)
	AND ItemID IN (SELECT ItemID 
			FROM DistributorItems 
			WHERE (DeletedFlag = 0 OR DeletedFlag IS Null))       
	</cfquery>

	<cfset queryColumnList = "AccountID,ItemID,YTDPurchaseQty,YTDPurchaseDol,OnOrderQty,OnOrderDol,YTDReturnQty,YTDReturnDol,YTDOrderCount,LastOrderDate">
</cfif>