<cfsetting enablecfoutputonly="yes">

<cftry>
<cfif isdefined("cgi.query_string")>
	<cfloop list="#cgi.query_string#" delimiters="&" index="valuepair">
		<cfset urlname = "#ListGetAt(valuepair, 1, "=")#">
		<cfif not isdefined( 'attributes.' & urlname )>
			<cfset setvariable("attributes.#urlname#" , "#evaluate("url."&"#urlname#")#")>
		</cfif>
	</cfloop>
</cfif>
<cfcatch type="any">
</cfcatch>
</cftry>

<cftry>
<cfif isdefined("form.fieldnames")>
	<cfloop list="#form.fieldnames#" index="field">
		<cfif not isdefined( 'attributes.' & field )>
			<cfset setvariable("attributes.#field#" , "#evaluate("form."&"#field#")#")>
		</cfif>
	</cfloop>
</cfif>
<cfcatch type="any">
</cfcatch>
</cftry>

<cftry>
	<cfquery datasource="#attributes.datasource#" name="insertHeader">
		Insert into AccountsReceivableTranHeader (DistRepID, Transdate)
		values (#url.id#, GetDate())
	</cfquery>
	<cfquery datasource="#attributes.datasource#" name="getTranID">
		select max(ARTranID) TID from AccountsReceivableTranHeader where DistRepID = #url.id#
	</cfquery>
	<cfset artranid = getTranID.TID>
	<cfscript>
	InvoiceStruct = structNew();
	for(i=1; i lte attributes.total; i=i+1)
	{
		"InvoiceSubStruct#i#" = StructNew();
		structInsert(evaluate("InvoiceSubStruct#i#"), "AccountID", evaluate("attributes.A#i#"));
		structInsert(evaluate("InvoiceSubStruct#i#"), "Invoice", evaluate("attributes.B#i#"));
		structInsert(evaluate("InvoiceSubStruct#i#"), "Amount", evaluate("attributes.C#i#"));
		structInsert(evaluate("InvoiceSubStruct#i#"), "PaymentType", evaluate("attributes.D#i#"));
		structInsert(evaluate("InvoiceSubStruct#i#"), "CheckNumber", evaluate("attributes.E#i#"));
		structInsert(evaluate("InvoiceSubStruct#i#"), "Attr1", evaluate("attributes.F#i#"));
		structInsert(evaluate("InvoiceSubStruct#i#"), "Attr2", evaluate("attributes.G#i#"));
		StructInsert(InvoiceStruct, "Invoice#i#", evaluate("InvoiceSubStruct#i#"));
	}
	</cfscript>
	<cfloop collection="#InvoiceStruct#" item="j">
		<cfquery datasource="#attributes.datasource#" name="updateInvoice">
			Update AccountsReceivable
			set AmountCollected = AmountCollected + #InvoiceStruct[j].Amount#,
			updatedate = getDate()
			where accountID = #InvoiceStruct[j].AccountID#
			and InvoiceNo = '#InvoiceStruct[j].Invoice#'
		</cfquery>
		<cfquery datasource="#attributes.datasource#" name="addTran">
			Insert Into AccountsReceivableTran(ARTranID, DistRepID, AccountID, InvoiceNo, PaymentType, AmountCollected, UpdateDate, CheckNumber, Attr1, Attr2)
			values(#artranid#, #url.id#, #InvoiceStruct[j].AccountID#,'#InvoiceStruct[j].Invoice#',#InvoiceStruct[j].PaymentType#, #InvoiceStruct[j].Amount#,getDate(), '#InvoiceStruct[j].CheckNumber#', '#InvoiceStruct[j].Attr1#', '#InvoiceStruct[j].Attr2#')
		</cfquery>
	</cfloop>

	<cfcatch type="any">
		<cfset compressedOutput = 0>
		
	 	<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject="Error updating invoice">
			Template: updateInvoice.cfm
			AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
			Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
			Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
			Type: #cfcatch.type#
			Message: #cfcatch.message#
			Details: #cfcatch.detail#
			
			<CFLOOP index=i from=1 to = #ArrayLen(CFCATCH.TAGCONTEXT)#>
				<CFSET sCurrent = #CFCATCH.TAGCONTEXT[i]#>
				<BR>#i# #sCurrent["ID"]# (#sCurrent["LINE"]#,#sCurrent["COLUMN"]#) #sCurrent["TEMPLATE"]#
			</CFLOOP>
			
			<cfif isDefined("form.fieldnames")>
				<cfloop list="#form.fieldnames#" index="i">
					#i#: #evaluate("form.#i#")#
				</cfloop>
			</cfif>
		</cfmail>		
	</cfcatch>
</cftry>

<cfif compressedOutput eq "">
	<cfset compressedOutput = 1>
</cfif>

<cfinclude template="act_zipoutput2.cfm">