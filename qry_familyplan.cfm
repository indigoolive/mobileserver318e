<!--- qry_familyplan.cfm --->

<cfif j eq "FamilyPlan">

	<cfset queryName = "getFamilyPlan">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
	select a.ItemID, a.CatalogID, FPCode1, isNull(TierID1, 0), isNull(CasePrice1, 0.0), FPCode2, isNull(TierID2, 0), isNull(CasePrice2, 0.0)
	  from CatalogItems a left outer join Family b 
	    on a.CatalogID = b.CatalogID
	   and a.ItemID = b.ItemID 
	 where a.CatalogID in (select distinct b.CatalogID
			 	 from DistributorAccountReps a, AccountCatalogs b, Family c
				where a.AccountID = b.AccountID
				  and b.CatalogID = c.CatalogID
				  and a.DistrepID = #url.ID#)
	</cfquery>

<cfset queryColumnList = "ItemID,CatalogID,FPCode1,TierID1,CasePrice1,FPCode2,TierID2,CasePrice2">
</cfif>