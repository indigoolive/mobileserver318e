<cfif j eq "Inventory">
	<cfset queryName = "getItems">

	<cfquery datasource="#attributes.datasource#" name="selectWarehouse">
		select LocationList 
		  from AccountPermissions 
		 where accountID = #url.ID# 
		   and ( LocationList is not NULL or LocationList <> '' )
	</cfquery>
	
	<cfquery name="getLastUD" datasource="#attributes.datasource#">
		select max(convert(datetime,AttributeValue)) lastUD from AccountAttributes where AccountID = #URL.ID# and AttributeName in ('ItemsUD', 'InventoryUD')
	</cfquery>

	<cfif getLastUD.RecordCount EQ 0>
		<cfset lastupdatedate = Now()>
	<cfelse>
		<cfset lastupdatedate = getLastUD.lastUD>
	</cfif>
	
	<cfif selectWarehouse.recordcount eq 0>
		<cfset warehouses = 1>
		<cfset warehousecount = 1>
	<cfelse>
		<cfset warehouses = ValueList(selectWarehouse.locationlist)>
		<cfif warehouses EQ "">
			<cfset warehouses = 1>
			<cfset warehousecount = 1>
		<cfelse>
			<cfset warehousecount = ListLen(warehouses)>
		</cfif>
	</cfif>

	<cfquery datasource="#attributes.datasource#" name="GetAllWhse">
		select warehouse
		from warehouse
		where warehouse in (#warehouses#)
	</cfquery>

	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar(255), di.ItemID) 
			+ '^' +	CONVERT(varchar,isNull(sum(WarehouseItems.QtyOnHand), 0))
			+ '^' + CONVERT(varchar,isNull(sum(WareHouseItems.parlevel), 0)) 
			+ '^' +	CONVERT(varchar,isNull(sum(WareHouseItems.ReserveQty), 0))
			+ '^' +	CONVERT(varchar,isNull(sum(warehouseitems.QtyOnOrder), 0))
			<cfif warehousecount gt 1 or options.AcctSpecificWarehouses EQ 1>
				<cfloop query="getAllWhse">
				+ '^' + CONVERT(varchar, isNull((select WareHouseItems.QtyOnHand from warehouseitems where itemID = di.itemID and warehouse = #GetAllWhse.warehouse#), 0)) 
				+ '^' + CONVERT(varchar, isNull((select WareHouseItems.QtyOnOrder from warehouseitems where itemID = di.itemID and warehouse = #GetAllWhse.warehouse#), 0)) 
				+ '^' + CONVERT(varchar, isNull((select WareHouseItems.ReserveQty from warehouseitems where itemID = di.itemID and warehouse = #GetAllWhse.warehouse#), 0)) 
				+ '^' + CONVERT(varchar, isNull((select WareHouseItems.ParLevel from warehouseitems where itemID = di.itemID and warehouse = #GetAllWhse.warehouse#), 0)) 
				+ '^' + CONVERT(varchar, ISNULL ((Select ItemStatus1 from WarehouseItemStatus where ItemID = di.ItemID and warehouse = #GetAllWhse.warehouse#), 0))
				</cfloop>
			</cfif>
		FROM DistributorItems di, WarehouseItems
		WHERE di.DistributorID = #check_login.distributorID# 
		AND di.ItemID = WarehouseItems.ItemID
		AND WarehouseItems.Warehouse in (#warehouses#)
		AND di.InvUpdateDate > #CreateODBCDateTime(lastupdatedate)#
		Group By di.ItemID
		Order By di.ItemID
	</cfquery>

	<cfset queryColumnList = "itemID,QTYOnHand,ParLevel,ReserveQty,OnOrderQty">
	<cfif warehousecount gt 1 or options.AcctSpecificWarehouses EQ 1>
		<cfloop query="getAllWhse">
		<cfset queryColumnList = listappend(queryColumnList, "Whse#getAllWhse.warehouse#QtyOnHand")>
		<cfset queryColumnList = listappend(queryColumnList, "Whse#getAllWhse.warehouse#OnOrderQty")>
		<cfset queryColumnList = listappend(queryColumnList, "Whse#getAllWhse.warehouse#ReserveQty")>
		<cfset queryColumnList = listappend(queryColumnList, "Whse#getAllWhse.warehouse#ParLevel")>
		<cfset queryColumnList = listappend(queryColumnList, "Whse#getAllWhse.warehouse#ItemStatus")>		
		</cfloop>
	</cfif>
</cfif>