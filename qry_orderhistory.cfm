<cfif j eq "OrderHistory">
	<cfset queryName = "OrderHistory">
	<cfif isDefined("url.UD")>
		<cfquery datasource="#attributes.datasource#" name="getLastDate">
		select attributevalue from accountattributes where accountID=#url.ID# and attributename='#j#UD'
		</cfquery>
	</cfif>

	<cfquery datasource="#attributes.datasource#" name="ExceptionFlag">
		select AttributeName from AccountAttributes where AccountID = #getDistributor.DistributorID# and AttributeName='ExceptionsByCount'
	</cfquery>

	<cfquery datasource="#attributes.datasource#" name="NoExtraARHistoryFlag">
		select AttributeName from AccountAttributes where AccountID = #getDistributor.DistributorID# and AttributeName='NoExtraARHistory'
	</cfquery>

	<cfquery datasource="#attributes.datasource#" name="AltHistoryDays">
		Select AttributeValue
		  from AccountAttributes
		 where AttributeName = 'AltHistoryDays'
		   and AccountID = #URL.ID#
	</cfquery>

	<cfif isDefined("url.MID") AND url.MID NEQ "">
		<cfset ddif = -366>
	<cfelseif AltHistoryDays.recordcount GT 0>
		<cfset ddif = AltHistoryDays.AttributeValue * -1>
	<cfelseif options.OrderHistoryDays NEQ "">
		<cfset ddif = options.OrderHistoryDays * -1>
	<cfelse>
		<cfset ddif = -30>
	</cfif>						

	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT Distinct isNull(convert(varchar, OrderNumber), '')+ '^' + 
		CASE
		WHEN len(DistInvoice) <> '' THEN
			DistInvoice
		WHEN len(DistOrder) <> '' THEN
			DistOrder
		ELSE
			convert(varchar, OrderNumber) 
		END
		+ '^' + isNull(convert(varchar(50), SpecialFlag), '')
		+ '^' + isNull(convert(varchar(30), totalorder), 0) 
		+ '^' + isNull(convert(varchar(30), NetOrder), 0) 
		+ '^' + isNull(convert(varchar(32), TransDate), '01-01-1900') 
		+ '^' + isNull(convert(varchar(32), ActivateDate), '01-01-1900') 
		+ '^' + isNull(convert(varchar(32), CancellationDate), '01-01-1900') 
		+ '^' + isNull(convert(varchar(32), FullfilledDate), '01-01-1900') 
		+ '^' + isNull(convert(varchar(32), TransmitDate), '01-01-1900')
		+ '^' + convert(varchar(8), dsraccount)
		+ '^' + (select convert(varchar, isNull(sum(CQty), 0)) 
				+ '^' + convert(varchar, isNull(sum(SQty), 0))
				+ '^' + convert(varchar, isNull(sum(CQtyShipped), 0))  
				+ '^' + convert(varchar, isNull(sum(SQtyShipped), 0)) 
				from masterorderitems where ordernumber = masterorder.ordernumber)
		+ '^' + convert(varchar(8), masterorder.AccountID)  
		<cfif ExceptionFlag.recordcount NEQ 0>
			+ '^' + (select CASE WHEN (sum(cqty) <> sum(cqtyshipped)) OR (sum(sqty) <> sum(sqtyshipped))
					THEN '1' ELSE '0' END
					FROM masterorderitems where ordernumber = masterorder.ordernumber)
		<cfelse>
			+ '^' + convert(varchar, isnull( (select count(*) ct from MasterOrderItems where MasterOrderItems.OrderNumber = MasterOrder.OrderNumber and (SComment <> '' OR CComment <> '')),0))
		</cfif>
		+ '^' + isNull((Select Signature from Signatures where OrderNumber = Signatures.OrderNumber), '')
		+ '^' + isNull(convert(varchar, SiteID), '')
		+ '^' + isNull(OrderNotes, '')
		+ '^' + isNull(TSOrderType, '')

        	As DataString
		FROM MasterOrder,
			DistributorAccountReps,
			Accounts
		WHERE MasterOrder.AccountID = DistributorAccountReps.AccountID
		and DistributorAccountReps.DistRepID = #url.id#
		and masterorder.AccountID = Accounts.AccountID
		and (Accounts.DeletedFlag is NULL or Accounts.DeletedFlag = 0) 
		and cancellationdate is null
		and (masterorder.ActivateDate >= dateadd(day, #ddif#, getdate()) 
		<cfif NoExtraARHistoryFlag.recordcount EQ 0>
			OR DistInvoice in (select InvoiceNo from AccountsReceivable where AccountID = Masterorder.AccountID)
		</cfif>
			)   <!--- This ) is needed --->

		<cfif options.FilterHistoryByRep>
			and exists (select ordernumber from masterorderitems where (masterorderitems.Salesman = DistributorAccountReps.Salesman or masterorderitems.Salesman is null) and masterorderitems.ordernumber = masterorder.ordernumber)
		</cfif>
		<cfif options.FilterB1Orders EQ 1>
			and ((siteid >= 3 and datediff(day, transdate, getdate()) >= 1) or (siteid < 3 and ((datediff(day, transdate, getdate()) < 1 ) or (transmitdate is null))))
		</cfif> 
		<cfif options.FilterB1Orders EQ 2> <!--- Only Site ID 3 Orders or errors--->
			and (siteid >= 3 or (select count(ordernumber) from MasterOrderItems where masterorderitems.ordernumber = masterorder.ordernumber and (SComment <> '' or CComment <> '')) > 0 )
		</cfif>
		<cfif options.FilterB1Orders EQ 3> <!--- Want SiteID 3 orders and all SiteID 1 orders that don't have SiteID 3 copies--->
			and (SiteID >= 3 or (siteid < 3 and not exists (Select a.OrderNumber from masterorder a where a.DistInvoice = masterOrder.distinvoice and siteid >= 3) ))
		</cfif> 
		<cfif options.FilterB1Orders EQ 4> <!--- (DistORder) Want SiteID 3 orders and all SiteID 1 orders that don't have SiteID 3 copies--->
			and (SiteID >= 3 or (siteid < 3 and not exists (Select a.OrderNumber from masterorder a where a.DistOrder is not NULL and a.DistOrder = masterOrder.distorder and siteid >= 3) ))
		</cfif> 
		<cfif options.FilterB1Orders EQ 5> <!--- (DistOrder and OpenOrder) Want SiteID 3 orders and all SiteID 1 orders that don't have SiteID 3 or Open Order copies--->
			and (SiteID >= 3 
			or (siteid < 3 
				and not exists (Select a.OrderNumber from masterorder a where a.DistOrder is not NULL and a.DistOrder = masterOrder.distorder and siteid >= 3) 
				and not exists (Select oo.InvoiceNumber from OpenOrders oo where oo.InvoiceNumber = MasterOrder.DistOrder)
			))
		</cfif> 

		<cfif isDefined("getLastDate") and getLastDate.recordcount neq 0>
			and datediff(day, transdate, GetDate()) < = 1
		</cfif>

	</cfquery>

<cfset queryColumnList = "OrderNumber,DistInvoice,SpecialFlag,TotalOrder,NetOrder,TransDate,ActivateDate,CancellationDate,FulfilledDate,TransmitDate,DSRAccount,OrderedCases,OrderedSingles,DeliveredCases,DeliveredSingles,AccountID,ExceptionFlag,Signature,SiteID,OrderNotes,TSOrderType">
</cfif>
