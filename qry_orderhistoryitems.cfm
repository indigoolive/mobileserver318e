<cfif j eq "OrderHistoryItems">
	<cfset queryName = "Querytemp">

	<cfif isDefined("url.UD")>
		<cfquery datasource="#attributes.datasource#" name="getLastDate">
		select attributevalue from accountattributes where accountID=#url.ID# and attributename='#j#UD'
		</cfquery>
	</cfif>

	<cfquery datasource="#attributes.datasource#" name="NoExtraARHistoryFlag">
		select AttributeName from AccountAttributes where AccountID = #getDistributor.DistributorID# and AttributeName='NoExtraARHistory'
	</cfquery>

	<cfquery datasource="#attributes.datasource#" name="AltHistoryDays">
		Select AttributeValue
		  from AccountAttributes
		 where AttributeName = 'AltHistoryDays'
		   and AccountID = #URL.ID#
	</cfquery>

	<cfif isDefined("url.MID") AND url.MID NEQ "">
		<cfset ddif = -366>
	<cfelseif AltHistoryDays.recordcount GT 0>
		<cfset ddif = AltHistoryDays.AttributeValue * -1>
	<cfelseif options.OrderHistoryDays NEQ "">
		<cfset ddif = options.OrderHistoryDays * -1>
	<cfelse>
		<cfset ddif = -30>
	</cfif>

	<cfquery datasource="#attributes.datasource#" name="TurnOffSpecialFlag">
		select AttributeName from AccountAttributes where AccountID = #getDistributor.DistributorID# and AttributeName='TurnOffSpecial'
	</cfquery>

	<cfquery datasource="#attributes.datasource#" name="TurnOffDiscountFlag">
		select AttributeName from AccountAttributes where AccountID = #getDistributor.DistributorID# and AttributeName='TurnOffDiscount'
	</cfquery>

	<cfquery datasource="#attributes.datasource#" name="TurnOffOverrideFlag">
		select AttributeName from AccountAttributes where AccountID = #getDistributor.DistributorID# and AttributeName='TurnOffOverride'
	</cfquery>
						

	<cfquery datasource="#attributes.datasource#" name="querytemp">
		SELECT Distinct
			convert(varchar, moi.OrderNumber)  + '^' +
			Convert(varchar, moi.itemID) + '^' +
			Convert(varchar, isnull(moi.CQty, 0)) + '^' +
			Convert(varchar, isnull(moi.CPrice, 0)) + '^' +
			Convert(varchar, isnull(moi.SQty, 0)) + '^' +
			Convert(varchar, isnull(moi.SPrice, 0)) + '^' +
			isNull(moi.SComment, '') + '^' +
			isNull(moi.CComment, '') + '^' +
			Convert(varchar, moi.LineItemNumber) + '^' +
	
			<cfif TurnOffSpecialFlag.recordcount GT 0>
				' ^' +
			<cfelse>
				isNull(moa1.attributevalue, ' ') + '^' +
			</cfif>
			<cfif TurnOffDiscountFlag.recordcount GT 0>
				' ^' +
			<cfelse>
				isNull(moa2.attributevalue, ' ') + '^' +
			</cfif>
			<cfif TurnOffOverrideFlag.recordcount GT 0>
				' ^' +
			<cfelse>
				isNull(moa3.attributevalue, ' ') + '^' +
			</cfif>
			isNull(convert(varchar,moi.ExtPrice),0) + '^' +
			Convert(varchar, isnull(moi.par, 0)) + '^' +
			Convert(varchar, isnull(moi.inv, 0)) + '^' + 
			isNull(convert(varchar(32), mo.TransDate), '01-01-1900') + '^' +
			Convert(varchar, isnull(moi.SQtyShipped, 0)) + '^' +
			Convert(varchar, isnull(moi.CQtyShipped, 0))
		FROM masterorderitems moi, 
			masterorder mo, 
			distributoraccountreps dar,
			<cfif TurnOffSpecialFlag.recordcount EQ 0>
				masterorderitemattributes moa1,
			</cfif>
			<cfif TurnOffDiscountFlag.recordcount EQ 0>
				masterorderitemattributes moa2, 
			</cfif>
			<cfif TurnOffOverrideFlag.recordcount EQ 0>
				masterorderitemattributes moa3,
			</cfif>
			accounts a
			 
		WHERE moi.ordernumber = mo.ordernumber
			and mo.cancellationdate is null
			and mo.accountid = dar.accountid
			and dar.distrepid = #url.id#
			and mo.accountid = a.accountid
			and (a.DeletedFlag is NULL or a.DeletedFlag = 0)

			<cfif TurnOffSpecialFlag.recordcount EQ 0>
				and ( moi.ordernumber *= moa1.ordernumber
				and   moi.LineItemNumber *= moa1.LineItemNumber
				and   moa1.AttributeName = 'SpecialFlag' )
			</cfif>

			<cfif TurnOffDiscountFlag.recordcount EQ 0>
				and ( moi.ordernumber *= moa2.ordernumber
				and   moi.LineItemNumber *= moa2.LineItemNumber
				and   moa2.AttributeName = 'DiscountAmount' )
			</cfif>

			<cfif TurnOffOverrideFlag.recordcount EQ 0>
				and ( moi.ordernumber *= moa3.ordernumber
				and   moi.LineItemNumber *= moa3.LineItemNumber
				and   moa3.AttributeName = 'PriceOverride' )
			</cfif>

		<cfif options.FilterHistoryByRep>
			and ( moi.salesman = dar.salesman or moi.salesman is null)
		</cfif>

		and (mo.ActivateDate >= dateadd(day, #ddif#, getdate())

		<cfif NoExtraARHistoryFlag.recordcount EQ 0>
		 	OR mo.DistInvoice in (select InvoiceNo from AccountsReceivable where AccountID = mo.AccountID) 
		</cfif>
			)  <!--- This ) is needed --->

		<cfif options.FilterB1Orders EQ 1>  <!--- SiteID 1 today, 3 for yesterday and back --->
			and ((siteid >= 3 and datediff(day, transdate, getdate()) >= 1) or (siteid < 3 and ((datediff(day, transdate, getdate()) < 1 ) or (transmitdate is null))))
		</cfif> 

		<cfif options.FilterB1Orders EQ 2>  <!--- Only Site ID 3 Orders or errors --->
			and (siteid >= 3 or SComment <> '' or CComment <> '' )
		</cfif>

		<cfif options.FilterB1Orders EQ 3>  <!--- Want SiteID 3 orders and all SiteID 1 orders that don't have SiteID 3 copies--->
			and (SiteID >= 3 or (siteid < 3 and not exists (Select a.OrderNumber from masterorder a where a.DistInvoice = mo.distinvoice and siteid >= 3) ))
		</cfif>

		<cfif options.FilterB1Orders EQ 4>  <!--- (DistOrder) Want SiteID 3 orders and all SiteID 1 orders that don't have SiteID 3 copies--->
			and (SiteID >= 3 or (siteid < 3 and not exists (Select a.OrderNumber from masterorder a where a.DistOrder is not NULL and a.DistOrder = mo.distorder and siteid >= 3) ))
		</cfif>

		<cfif options.FilterB1Orders EQ 5>  <!--- (DistOrder) Want SiteID 3 orders and all SiteID 1 orders that don't have SiteID 3 copies--->
			and (SiteID >= 3 
				or (siteid < 3 
					and not exists (Select a.OrderNumber from masterorder a where a.DistOrder is not NULL and a.DistOrder = mo.distorder and siteid >= 3)
					and not exists (Select oo.InvoiceNumber from OpenOrders oo where oo.InvoiceNumber = mo.DistOrder)
				 ))
		</cfif>
 
		<cfif isDefined("getLastDate") and getLastDate.recordcount neq 0>
			and datediff(day, transdate, GetDate()) <= 1
		</cfif>
	</cfquery>
	<cfset queryColumnList = "OrderNumber,itemID,CQty,CPrice,SQty,SPrice,SComment,CComment,LineItemNumber,SpecialFlag,DiscountAmount,PriceOverride,ExtPrice,par,inv,transdate,SQtyShipped,CQtyShipped">
</cfif>
