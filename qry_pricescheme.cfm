<cfif j eq "PricingScheme">

	<cfquery datasource="#attributes.datasource#" name="getDiv">
		select DivisionList
		from AccountPermissions
		where accountID = #url.id#
	</cfquery>

	<cfquery datasource="#attributes.datasource#" name="selectWarehouse">
		select LocationList 
		from AccountPermissions 
		where accountID = #url.ID# 
		and ( LocationList is not NULL or LocationList <> '' )
	</cfquery>

	<cfif selectWarehouse.recordcount eq 0>
		<cfset Warehouses = 1>
	<cfelse>
		<cfset Warehouses = ValueList(selectWarehouse.locationlist)>
		<cfif Warehouses EQ "">
			<cfset Warehouses = 1>
		</cfif>
	</cfif>

	
	<cfset queryName = "PricingScheme">

<!--- List Price --->
<cfif options.PricingModel EQ 0>
	<cfif options.RelationalPricingScheme EQ 0>
	   <cfquery datasource="#attributes.datasource#" name="getAllTiers">
    		select tierID
    		from pricingtiers
    		where distributorID = #check_login.DistributorID#
    		</cfquery>
    	
    		<cfset sql="Select convert(varchar,ooo.ItemID) + '^' + CONVERT(varchar,SplitCaseCharge) +  '^' + CONVERT(varchar,CatalogID)">
    		<cfloop query="GetAllTiers">
    			<cfset sql = sql & " + '^' + (select CONVERT(varchar,isnull(CasePrice, 0.00)) + '^' + CONVERT(varchar,isnull(SinglePrice, 0.00)) from PricingScheme T#TierID# where T#TierID#.itemID =* ooo.ItemID and T#TierID#.CatalogID =* ooo.CatalogID and TierID = #TierID#)">
    		</cfloop>
    		
    		<cfset sql=sql & " as DataString from CatalogItems ooo, DistributorItems where ooo.catalogID in ( #CatalogList# ) and ooo.ItemID = DistributorItems.ItemID and DistributorItems.DeletedFlag <> 1">
    		<cfif (getDiv.recordcount neq 0) and (trim(getdiv.DivisionList) neq "")>
    			<cfset sql = sql & " and DistributorItems.DivisionID in (#getdiv.DivisionList#)">
    		</cfif>
    		
    		<cfquery name="#QueryName#" datasource="#attributes.datasource#">
    		#PreserveSingleQuotes(sql)#
    		</cfquery>
    		
    		<cfset queryColumnList = "itemID,splitcasecharge,CatalogID">
    		<cfloop query="getAllTiers">
    			<cfset queryColumnList = queryColumnList & ",CPrice#evaluate("getAllTiers.tierID-100")#,SPrice#evaluate("getAllTiers.tierID-100")#">
    		</cfloop>
		</cfif>
		
	<cfif options.RelationalPricingScheme EQ 1>
            <cfquery name="#QueryName#" datasource="#attributes.datasource#">
         	Select distinct PricingScheme.ItemID,
                    SplitCaseCharge,
                    PricingScheme.CatalogID,
        			TierID,
        			isnull(SinglePrice, 0.00),
        			isnull(CasePrice, 0.00)
               from PricingScheme, 
                    DistributorItems
        	 where PricingScheme.ItemID = DistributorItems.ItemID 
        	   and CatalogID in ( #CatalogList# )
        	<cfif (getDiv.recordcount neq 0) and (trim(getdiv.DivisionList) neq "")>
        	   and DistributorItems.DivisionID in (#getdiv.DivisionList#)"
        	</cfif>
        	   and DistributorItems.DeletedFlag <> 1
        	   and (caseprice <> 0 or singleprice <> 0)
             order by PricingScheme.itemID, Pricingscheme.catalogID, PricingScheme.tierID asc
        	</cfquery>
        	<cfset QueryColumnList = "ItemID,SplitCaseCharge,CatalogID,TierID,SPrice0,CPrice0">
        </cfif>        
		
	<!--- Catalog Horizontal --->
	<cfif options.RelationalPricingScheme EQ 2>
		<cfquery name="#QueryName#" datasource="#attributes.datasource#">
					select di.ItemID,
				       isnull(p1.CasePrice,0),
				       isnull(p1.SinglePrice,0),
				       isnull(p1.SplitCaseCharge,0),
				       isnull(p2.CasePrice,0),
				       isnull(p2.SinglePrice,0),
				       isnull(p2.SplitCaseCharge,0),
				       isnull(p3.CasePrice,0),
				       isnull(p3.SinglePrice,0),
				       isnull(p3.SplitCaseCharge,0),
				       isnull(p4.CasePrice,0),
				       isnull(p4.SinglePrice,0),
				       isnull(p4.SplitCaseCharge,0),
				       isnull(p5.CasePrice,0),
				       isnull(p5.SinglePrice,0),
				       isnull(p5.SplitCaseCharge,0),
				       isnull(p6.CasePrice,0),
				       isnull(p6.SinglePrice,0),
				       isnull(p6.SplitCaseCharge,0),
				       isnull(p7.CasePrice,0),
				       isnull(p7.SinglePrice,0),
				       isnull(p7.SplitCaseCharge,0),
				       isnull(p8.CasePrice,0),
				       isnull(p8.SinglePrice,0),
				       isnull(p8.SplitCaseCharge,0),
				       isnull(p9.CasePrice,0),
				       isnull(p9.SinglePrice,0),
				       isnull(p9.SplitCaseCharge,0),
				       isnull(p10.CasePrice,0),
				       isnull(p10.SinglePrice,0),
				       isnull(p10.SplitCaseCharge,0)
				from DistributorItems di,
				     PricingScheme p1,
				     PricingScheme p2,
				     PricingScheme p3,
				     PricingScheme p4,
				     PricingScheme p5,
				     PricingScheme p6,
				     PricingScheme p7,
				     PricingScheme p8,
				     PricingScheme p9,
				     PricingScheme p10
				where di.ItemID *= p1.ItemID and p1.CatalogID = 10
				  and di.ItemID *= p2.ItemID and p2.CatalogID = 11
				  and di.ItemID *= p3.ItemID and p3.CatalogID = 12
				  and di.ItemID *= p4.ItemID and p4.CatalogID = 13
				  and di.ItemID *= p5.ItemID and p5.CatalogID = 14
				  and di.ItemID *= p6.ItemID and p6.CatalogID = 15
				  and di.ItemID *= p7.ItemID and p7.CatalogID = 16
				  and di.ItemID *= p8.ItemID and p8.CatalogID = 17
				  and di.ItemID *= p9.ItemID and p9.CatalogID = 18
				  and di.ItemID *= p10.ItemID and p10.CatalogID = 19
				  and (di.DeletedFlag = 0 or di.DeletedFlag is null)
		</cfquery>
		<cfset queryColumnList = "ItemID,CPrice0,Sprice0,SplitCharge0,CPrice1,Sprice1,SplitCharge1,CPrice2,Sprice2,SplitCharge2,CPrice3,Sprice3,SplitCharge3,CPrice4,Sprice4,SplitCharge4,CPrice5,Sprice5,SplitCharge5,CPrice6,Sprice6,SplitCharge6,CPrice7,Sprice7,SplitCharge7,CPrice8,Sprice8,SplitCharge8,CPrice9,Sprice9,SplitCharge9">
	</cfif>
</cfif>


<cfif options.PricingModel EQ 1>
	<!--- New Tier Based Pricing --->
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar, PricingScheme.ItemID) + '^' +
			CONVERT(varchar, PricingScheme.SplitCaseCharge) + '^' +
   	                CONVERT(varchar, PricingScheme.CatalogID) + '^' +
			CONVERT(varchar, PricingScheme.TierID)  + '^' +
			CONVERT(varchar, isnull(SinglePrice, 0.00)) + '^' +
			CONVERT(varchar, isnull(CasePrice, 0.00))
		As DataString
		FROM PricingScheme, PricingTiers, DistributorItems
		WHERE PricingScheme.TierID = PricingTiers.TierID
		AND PricingScheme.ItemID = DistributorItems.ItemID
		AND PricingScheme.CatalogID in (#CatalogList#)
		AND PricingTiers.DeletedFlag = 0
		AND DistributorItems.DeletedFlag = 0
	</cfquery>
	<cfset queryColumnList = "ItemID,SplitCaseCharge,CatalogID,TierID,SPrice0,CPrice0">
</cfif>


<cfif options.PricingModel EQ 2>
        <!--- Qty Based Pricing --->

    	<cfif options.RelationalPricingScheme EQ 0>
    		<cfquery datasource="#attributes.datasource#" name="getAllTiers">
	    		select tierID
    			from pricingtiers
    			where distributorID = #check_login.DistributorID#
    		</cfquery>
    	
    		<cfset sql="Select convert(varchar,ooo.ItemID) + '^' + CONVERT(varchar,SplitCaseCharge) +  '^' + CONVERT(varchar,CatalogID)">

    		<cfloop query="GetAllTiers">
    			<cfset sql = sql & " + '^' + (select CONVERT(varchar,isnull(CasePrice, 0.00)) + '^' + CONVERT(varchar,isnull(SinglePrice, 0.00)) from PricingScheme T#TierID# where T#TierID#.itemID = ooo.ItemID and T#TierID#.CatalogID = ooo.CatalogID and TierID = #TierID#)">
    		</cfloop>
    		
    		<cfset sql=sql & " as DataString from CatalogItems ooo, DistributorItems where ooo.catalogID in ( #CatalogList# ) and ooo.ItemID = DistributorItems.ItemID and DistributorItems.DeletedFlag <> 1">

    		<cfif (getDiv.recordcount neq 0) and (trim(getdiv.DivisionList) neq "")>
    			<cfset sql = sql & " and DistributorItems.DivisionID in (#getdiv.DivisionList#)">
    		</cfif>
    		
    		<cfquery name="#QueryName#" datasource="#attributes.datasource#">
	    		#PreserveSingleQuotes(sql)#
    		</cfquery>
    		
    		<cfset queryColumnList = "itemID,splitcasecharge,CatalogID">
    		<cfloop query="getAllTiers">
    			<cfset queryColumnList = queryColumnList & ",CPrice#evaluate("getAllTiers.tierID-100")#,SPrice#evaluate("getAllTiers.tierID-100")#">
    		</cfloop>
    	</cfif>
    	
    	<cfif options.RelationalPricingScheme EQ 1>
        	<cfquery name="#QueryName#" datasource="#attributes.datasource#">
			Select distinct
				PricingScheme.CatalogID,
				PricingScheme.ItemID,
				TierID,
				isnull(SinglePrice, 0.00) as SinglePrice,
				isnull(CasePrice, 0.00) as CasePrice,
				SplitCaseCharge
			from PricingScheme, DistributorItems, WarehouseItems
			where PricingScheme.ItemID = DistributorItems.ItemID 
				and WarehouseItems.ItemID = DistributorItems.ItemID 
				and CatalogID in ( #CatalogList# )
				and WarehouseItems.warehouse in ( #Warehouses# )
			<cfif (getDiv.recordcount neq 0) and (trim(getdiv.DivisionList) neq "")>
				and DistributorItems.DivisionID in (#getdiv.DivisionList#)"
			</cfif>
				and DistributorItems.DeletedFlag <> 1
				and (caseprice <> 0 or singleprice <> 0)
			order by PricingScheme.itemID, Pricingscheme.catalogID, PricingScheme.tierID asc
        	</cfquery>

        	<cfset QueryColumnList = "ItemID,SplitCaseCharge,CatalogID">

        	<cfloop from="0" to="#evaluate("options.MaxPriceBreaks - 1")#" index="pBreak">
        		<cfset QueryColumnList = QueryColumnList & ",Qty#pBreak#,Cprice#pBreak#,SPrice#pBreak#">
        	</cfloop>

    	</cfif>
    	
	<cfif options.RelationalPricingScheme EQ 2>
        	<cfquery name="#QueryName#" datasource="#attributes.datasource#">
			Select distinct
				PricingScheme.CatalogID,
				PricingScheme.ItemID,
				TierID,
				isnull(SinglePrice, 0.00) as SinglePrice,
				isnull(CasePrice, 0.00) as CasePrice,
				SplitCaseCharge,
				PriceFlag
			from PricingScheme, DistributorItems, WarehouseItems
			where PricingScheme.ItemID = DistributorItems.ItemID 
				and PricingScheme.ItemID = WarehouseItems.ItemID
				and CatalogID in ( #CatalogList# )
				and WarehouseItems.warehouse in ( #Warehouses# )
				and (caseprice <> 0 or singleprice <> 0)
				<cfif (getDiv.recordcount neq 0) and (trim(getdiv.DivisionList) neq "")>
					and DistributorItems.DivisionID in (#getdiv.DivisionList#)"
				</cfif>
			order by PricingScheme.itemID, Pricingscheme.catalogID, PricingScheme.tierID asc
        	</cfquery>

        	<cfset QueryColumnList = "ItemID,SplitCaseCharge,CatalogID">

        	<cfloop from="0" to="#evaluate("options.MaxPriceBreaks - 1")#" index="pBreak">
        		<cfset QueryColumnList = QueryColumnList & ",Qty#pBreak#,CPrice#pBreak#,SPrice#pBreak#,PriceFlag#pBreak#">
        	</cfloop>

    	</cfif>
    
</cfif>
    
</cfif>
