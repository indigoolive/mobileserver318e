<!--- qry_crediterms.cfm --->

<cfif j eq "CreditTerms">
	<cfset queryName = "getCreditTerms">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
   		SELECT CONVERT(varchar, DistributorID) + '^'
			+ CONVERT(varchar, CreditID) + '^'
			+ DistCreditID + '^'
			+ CreditDescription
			 as DataString
		FROM CreditTerms
		WHERE DistributorID = #getDistributor.DistributorID#
	</cfquery>

<cfset queryColumnList = "DistribuotrID,CreditID,DistCreditID,CreditDescription">
</cfif>