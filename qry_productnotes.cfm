<cfif j eq "ProductNotes">
	<cfquery datasource="#attributes.datasource#" name="getDiv">
	select DivisionList
	from AccountPermissions
	where accountID = #url.id#
	</cfquery>
	<cfset queryName = "TastingNotes">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar, productnotes.itemID) 
			+ '^' + productnotes.note1
			+ '^' + productnotes.note2
			+ '^' + productnotes.note3
		As DataString
		FROM productnotes, distributoritems
		WHERE productnotes.itemID = distributorItems.itemID
			<cfif (getDiv.recordcount neq 0) and (trim(getdiv.DivisionList) neq "")>
			and DistributorItems.DivisionID in (#getDiv.DivisionList#)
			</cfif>
			and distributoritems.deletedflag <> 1 
			and productnotes.deletedflag <> 1
	</cfquery>
<cfset queryColumnList = "itemID,note1,note2,note3">
</cfif>

