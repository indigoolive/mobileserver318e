<cfsetting enablecfoutputonly="no">
<!---
KEY
Imp{n}=AccountID^Time^Type^EntryCount^InvEntryCount^ErrorCount^AddCount
N={count}
--->

<cfif isDefined("form.fieldnames")>
<cftry>
	<cfif NOT isdefined("form.N")>
		<cfset compressedOutput = "<ERROR>ERROR 1001: Transaction incomplete please try again.</ERROR>">
	<cfelse>
		<cfloop from="1" to="#form.N#" index="i">
			<cfset line = Evaluate("form.L#i#")>
			<cfset DataArray = ListToArray(line,"^")>

			<!--- new message --->
			<cfquery name="SaveData" datasource="#attributes.datasource#">
				Insert into InvImportInfo
					(DistRepID, AccountID,
						ImportTime, ImportType,
						EntryCount, InvEntryCount,
						OrderGuideCount, ErrorCount,
						AddCount, ReceivedTime)
					  values 
					( #url.id#, #DataArray[1]#,
						'#DataArray[2]#', #DataArray[3]#,
						#DataArray[4]#, #DataArray[5]#,
						#DataArray[6]#, #DataArray[7]#,
						#DataArray[8]#, GetDate())
			</cfquery>
		</cfloop>
		<cfset compressedOutput = "SUCCESS">
	</cfif>

<cfcatch type="any">
	<cfset compressedOutput = "<ERROR>Error 5000: General Error, Support has been notified.</ERROR>">
	
 	<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject="Error 5000 Import Info Error">
		Error 5000: General Error
		Template: UpdateImports.cfm
		AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
		Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
		Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
		Type: #cfcatch.type#
		Message: #cfcatch.message#
		Details: #cfcatch.detail#
		
		SQL: <cfif isdefined("cfcatch.Sql")>#cfcatch.Sql#</cfif>

		<CFLOOP index=i from=1 to = #ArrayLen(CFCATCH.TAGCONTEXT)#>
			<CFSET sCurrent = #CFCATCH.TAGCONTEXT[i]#>
			<BR>#i# #sCurrent["ID"]# (#sCurrent["LINE"]#,#sCurrent["COLUMN"]#) #sCurrent["TEMPLATE"]#
		</CFLOOP>

		<cfif isDefined("form.fieldnames")>
			<cfloop list="#form.fieldnames#" index="i">
				#i#: #evaluate("form.#i#")#
			</cfloop>
		</cfif>
	</cfmail>
</cfcatch>

</cftry>
</cfif>

<cfinclude template="act_zipoutput2.cfm">