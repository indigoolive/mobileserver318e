<cfif j eq "OrderDiscounts">
	<cfset queryName = "OrderDiscounts">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar, ID) 
		       + '^' + isNull(Description, '')
		AS DataString 
		FROM OrderDiscounts
	</cfquery>
	<cfset queryColumnList = "ID,Description">
</cfif>