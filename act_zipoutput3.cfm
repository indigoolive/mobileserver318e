<cfparam name="compressedoutput" default="">

<cfset TempDir = "P:\RENEW_TMP\">
<cfset tempFileName = GetTempFile(#TempDir#, "gz_")>

<cffile action="WRITE" file="#tempFileName#" output="#compressedOutput#" addnewline="no">

<cftry>
	<cfset gzFileName = tempFileName & ".gz">

	<cfinvoke component = "Zip" method = "gzipAddFile" gzipFilename = "#gzFileName#" inputFilename = "#tempFileName#">

	<cffile action="delete" file="#tempFileName#">

	<cfset gzFileSize = createObject("java","java.io.File").init("#gzFileName#").length()>

	<cfset NameOutput = #GetFilefromPath(gzFileName)# & #chr(13)# & #chr(10)# & #gzFileSize#>

	<cfset NameFileName = tempFileName & ".name">
	<cffile action="WRITE" file="#NameFileName#" output="#NameOutput#" addnewline="no">	
	<cfcontent deletefile="yes" file="#NameFileName#" type="text/plain">
        
<cfcatch type="any">
	<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject="GZ Download Error">
	Error 6600: General Error (cfinvoke Failed)
	Template: act_zipoutput3.cfm
	AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
	Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
	Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
	Type: #cfcatch.type#
	Message: #cfcatch.message#
	SQL: <cfif isdefined("cfcatch.sql")>#cfcatch.sql#<cfelse>N/A</cfif>
	Details: #cfcatch.detail#

	<CFLOOP index=i from=1 to = #ArrayLen(CFCATCH.TAGCONTEXT)#>
		<CFSET sCurrent = #CFCATCH.TAGCONTEXT[i]#>
		<BR>#i# #sCurrent["ID"]# (#sCurrent["LINE"]#,#sCurrent["COLUMN"]#) #sCurrent["TEMPLATE"]#
	</CFLOOP>

	<cfif isDefined("form.fieldnames")>
		<cfloop list="#form.fieldnames#" index="i">
		#i#: #evaluate("form.#i#")#
		</cfloop>
	</cfif>
	
	<cfif isDefined("tempFileName")>
		tempFileName is #tempFileName#
	</cfif>
	</cfmail>

</cfcatch>
</cftry> 	
