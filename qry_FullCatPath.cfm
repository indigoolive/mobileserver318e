<!--- qry_FullCatPath.cfm --->

<cfif j eq "FullCatPath">

	<cfset queryName = "GetFullCatPath">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar, CategoryID)
			+ '^' + IsNull(FullPath, '')
		FROM FullCatPath
	</cfquery>
	<cfset queryColumnList = "CategoryID,FullPath">
</cfif>