<cfif j eq "ManagerReps">
	<cfset queryName = "ManagerRepsQuery">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar, da.AccountID) 
		       + '^' + isNull(a.ContactName, '') 
		       + '^' + isNull(a.Password, '')
		AS DataString 
		FROM DistributorAccounts da, Accounts a
		WHERE da.AccountID = a.AccountID
		AND da.DistributorID = #GetDistributor.DistributorID#
		AND da.ManagerID = #URL.MID#
		AND a.AccountType in (2,4)
		AND EXISTS (SELECT TOP 1 DistributorAccountReps.AccountID
				FROM DistributorAccountReps, Accounts
				WHERE DistributorAccountReps.AccountID = Accounts.AccountID
				AND DistributorAccountReps.DistributorID = #GetDistributor.DistributorID#
				AND DistributorAccountReps.DistRepID = da.AccountID
				AND (Accounts.DeletedFlag IS NULL
					OR Accounts.DeletedFlag = 0)) 
	</cfquery>
	<cfset queryColumnList = "RepID,Name,Password">
</cfif>