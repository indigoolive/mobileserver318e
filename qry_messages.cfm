<cfif j eq "messages">
<!---
Status codes
0=unread
1=read
2=replied
9=deleted
--->
	<cfset queryName = "messages">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
select convert(varchar,MessageID)
     + '^' + convert(varchar,FromID)
	 + '^' + convert(varchar,FromStatus)
	 + '^' + a.ContactName
	 + '^' + convert(varchar,ToID)
	 + '^' + convert(varchar,ToStatus)
	 + '^' + b.ContactName
	 + '^' + subject
	 + '^' + message
	 + '^' + convert(varchar,entrydate,120) as DataString
from Messages,Accounts a, Accounts b 
where ((ToID = #url.id# and ToStatus < 9) or (FromID = #url.id# and FromStatus < 9))
and FromID = a.AccountiD
and ToID = b.AccountID
	</cfquery>
	<cfset queryColumnList = "MessageID,FromID,FromStatus,FromName,ToID,ToStatus,ToName,Subject,Message,EntryDate">
</cfif>


	