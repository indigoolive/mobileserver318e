<!--- <cfmodule template="/Common/other/logger.cfm" accountID="#url.ID#" accounttype="#getDatasource.accounttype#" moduletype="1"> --->
<cfsetting enablecfoutputonly="Yes">
<cftry>
	<cfif not IsDefined("form.T")>
		<cfthrow type="JasonExecption"
			 message = "Survey Answers Missing Total"
			 detail = "Form T is missing from the sent answers there could have been a communication error">
	<cfelse>
		<!--- Check for existence of survey first --->
		<cfif form.B EQ "">
			<cfthrow type="JasonExecption"
				 message = "Missing SurveyID"
				 detail = "The SurveyID is missing from the sent answers there could have been a communication error">
		<cfelse>
			<cfquery name = "GetSurvey" datasource = "#attributes.datasource#">
				SELECT *
				  FROM Survey
				 WHERE SurveyID = #form.B#
			</cfquery>
		</cfif>

		<cfif GetSurvey.RecordCount EQ 0>
			<cfthrow type="JasonExecption"
				 message = "Invalid SurveyID"
				 detail = "There was an attempt to send answers for a survey that does not exist for this distributor">
		<cfelse>
			<cfif form.D EQ "">
				<cfset BeginDate = "01-01-1970">
			<cfelse>
				<cfset BeginDate = form.D>
			</cfif>
			<cfif form.E EQ "">
				<cfset EndDate = "01-01-1970">
			<cfelse>
				<cfset EndDate = form.E>
			</cfif>
			<cfquery name = "InsertDoneSurvey" datasource = "#attributes.datasource#">

				INSERT INTO DoneSurveys (SurveyID, DistRepID, AccountID, Completed, Startdate, Enddate) VALUES (#form.B#,
								#URL.ID#,
								#form.A#,
								#form.C#,
								'#BeginDate#',
								'#EndDate#')
			</cfquery>

			<cfquery name = "GetDoneID" datasource = "#attributes.datasource#">
				SELECT max(DoneID) as DoneID
				  FROM DoneSurveys
				 WHERE SurveyID = #form.B#
			</cfquery>

			<cfloop   index = "AnswerID"   from = "1"   to = "#form.T#">
				<cfset Record = evaluate("form.R#AnswerID#")>
				<cfset Type = ListGetAt(Record, 1, "^")>
				<cfset Answer = ListGetAt(Record, 2, "^")>
				<cfset Comment = ListGetAt(Record, 3, "^")>

				<cfquery name = "InsertAnswers" datasource = "#attributes.datasource#">
					INSERT INTO Answers
						(SurveyID, QuestionID, DoneID, Type, Completed, Answers, Comment)
					VALUES
						(#form.B#, #AnswerID#, #GetDoneID.DoneID#, #Type#, #form.C#, '#Answer#', '#Comment#')
				</cfquery>
			</cfloop>
		</cfif>
	</cfif>
	<cfset compressedOutput = 1>

<cfcatch type="JasonExecption">
	<cfset compressedOutput = 0>
	<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject=#cfcatch.message#>
		Template: TakeSurveys.cfm
		RepID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
		AccountID: <cfif isDefined("form.a")>#form.A#<cfelse>N/A</cfif>
		Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
		Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
		Details: #cfcatch.detail#
	</cfmail>
</cfcatch>

<cfcatch type="any">
	<cfset compressedOutput = 0>
	
	<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject="Error adding data to BeverageOne database">
		Error 2000: Error adding data to BeverageOne database
		Template: TakeSurveys.cfm
		RepID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
		Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
		Form A: <cfif isDefined("form.A")>#form.A#<cfelse>N/A</cfif>
		Form B: <cfif isDefined("form.B")>#form.B#<cfelse>N/A</cfif>
		Form C: <cfif isDefined("form.C")>#form.C#<cfelse>N/A</cfif>
		Form D: <cfif isDefined("form.D")>#form.D#<cfelse>N/A</cfif>
		Form E: <cfif isDefined("form.E")>#form.E#<cfelse>N/A</cfif>
		Type: <cfif isDefined("Type")>#Type#<cfelse>N/A</cfif>
		Answers: <cfif isDefined("Answers")>#Answers#<cfelse>N/A</cfif>
		DoneID: <cfif isDefined("TempDoneID")>#TempDoneID#<cfelse>N/A</cfif>
		QuestionID: <cfif isDefined("GetQuestions.QuestionID")>#GetQuestions.QuestionID#<cfelse>N/A</cfif>
		Record: <cfif isDefined("Record")>#Record#<cfelse>N/A</cfif>
		Comment: <cfif isDefined("Comment")>#Comment#<cfelse>N/A</cfif>
		Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
		Type: #cfcatch.type#
		Message: #cfcatch.message#
		Details: #cfcatch.detail#
	</cfmail>
</cfcatch>
</cftry>

<cfinclude template="act_zipoutput2.cfm">