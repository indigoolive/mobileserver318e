 <cfsetting enablecfoutputonly="yes">

<cfquery datasource="#attributes.datasource#" name="check_login">
select DistributorAccountReps.accountID,
       DistributorAccountReps.DistributorID
  from Accounts,
       DistributorAccountReps
 where Accounts.accountID=#url.ID#
<cfif attributes.password NEQ masterpass>     
   and Accounts.password='#url.p#'
</cfif>
   and Accounts.LoginEnabled = 'Y'
   and Accounts.AccountID *= DistributorAccountReps.DistRepID
</cfquery>
<cfif check_login.recordcount eq 0>
	<cfabort>
</cfif>

<cfset tables = "Items">


<cfset compressedOutput = "">
<cfset inc = 1>
<cfloop list="#tables#" index="j">
	<cfinclude template="qry_recentupdates.cfm">

	<cfset queryRecordCount = evaluate("#queryName#.recordcount")>
	<cfset compressedOutput = "<Data>
		<DataTable>#j#</DataTable>
		<ColumnList>#queryColumnList#</ColumnList>
		<RecordCount>#queryRecordCount#</RecordCount>	
		<RowData>">
	<cfx_concat query="#queryName#">
	<cfset compressedOutput = compressedOutput & "</RowData>
	</Data>">
	<cfif inc neq ListLen(tables)>
		<cfset compressedOutput = compressedOutput & "######DATA######">
	</cfif>
	<cfset inc = inc + 1>
</cfloop>


<cfset tempDir = "e:\domains\beverageone\wwwroot">
<cfset tempFileName = GetTempFile(tempDir, "gz_")>
<cffile action="WRITE" file="#tempFileName#" output="#compressedOutput#" addnewline="no">
<cfx_gzip action="gzip" Infile="#tempFileName#" outfile="#tempFileName#.gz" level="9">
<cffile action="delete" file="#tempFileName#">
<cfcontent deletefile="yes" file="#tempFileName#.gz" type="text/plain">
<cfif fileExists("#tempFileName#.gz")>
	<cffile action="delete" file="#tempFileName#">
</cfif>