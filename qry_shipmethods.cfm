<cfif j EQ "ShipMethods">
	<cfset queryName = "GetMethods">
    <!--- Get the Area --->
    <cfquery name="GetRepArea" datasource="#Attributes.Datasource#">
    	select AreaID from DistRepArea where DistRepID = #URL.ID#
    </cfquery>
    
    <cfset theArea = GetRepArea.AreaID>
    
    <cfif theArea EQ "">
    	<cfquery name="GetRepArea" datasource="#Attributes.Datasource#">
    		select min(AreaID) AreaID from Areas
    	</cfquery>
        <cfset theArea = GetRepArea.AreaID>
    </cfif>
    
    <cfquery name="GetMethods" datasource="#attributes.datasource#">
    	select convert(varchar,ShipMethods.MethodID) + '^' + isnull(Description,'NULL')
    	  from ShipMethodArea, ShipMethods
    	 where ShipMethodArea.AreaID = #theArea#
    	   and ShipMethodArea.MethodID = ShipMethods.MethodID
    </cfquery>
    
    <cfset queryColumnList = "MethodID,Description">
</cfif>