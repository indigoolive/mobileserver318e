
<cfparam Name="attributes.Query" type="query">
<cfparam Name="attributes.ID" type="integer">
<cfparam Name="attributes.maxBreaks" type="integer">
<cfparam Name="attributes.rowReturn" type="string">
	
<cfset QPQuery = attributes.Query>
<cfset QPID = attributes.ID>
<cfset QPmaxBreaks = attributes.maxBreaks>
<cfset QProwReturn = attributes.rowReturn>
	
<cfset rowCount = QPQuery.recordcount>
<cfset compData = "">
	
<cfset Delimiter = "^">

<cfset strFiller = "-1" & Delimiter & "0" & Delimiter & "0">
	
<cfset TotalRows = 0>
	
<cfset TheData = "">
<cfset buffer=ArrayNew(1)>
	
<cfset currentItem = "">
<cfset currentCatalog = "">
<cfset currentbreak = 0>
	
<cfloop query="QPQuery">
	<cfif (currentItem EQ QPQuery.ItemID) && (currentCatalog EQ QPQuery.CatalogID)>
		<cfset ArrayAppend(buffer,QPQuery.TierID & Delimiter)>
					
		<!--- caseprice & singleprice --->
		<cfset cs = QPQuery.CasePrice>
		<cfif Find(".",cs) NEQ 0>
			<cfset cs = Left(cs,Find(".",cs)+3)>
		</cfif>
		<cfset ArrayAppend(buffer,cs & Delimiter)>

			
		<cfset ss = QPQuery.SinglePrice>
		<cfif Find(".",ss) NEQ 0>
			<cfset ss = Left(ss,Find(".",ss)+3)>
		</cfif>
		<cfset ArrayAppend(buffer,ss)> <!--- No delimiter! --->
		
		<cfif currentbreak EQ QPmaxBreaks>
			<cfset ArrayAppend(buffer,QProwReturn)>
			<cfset currentbreak = 1>
		<cfelse>
			<cfset ArrayAppend(buffer,Delimiter)>
			<cfset currentbreak = currentbreak + 1>
		</cfif>		
	<cfelse> <!--- new item or catalog --->
		<cfif currentbreak GT 0>
			<cfif currentbreak LTE QPmaxBreaks> <!--- Finish line with filler --->
				<cfloop from="#currentbreak#" to="#QPmaxBreaks#" index="brkFiller">
					<cfset ArrayAppend(buffer,strFiller)>
					
					<cfif brkFiller LT QPmaxBreaks>
						<cfset ArrayAppend(buffer,Delimiter)>
					<cfelse> <!--- brkFiller EQ QPmaxBreaks --->
						<cfset ArrayAppend(buffer,QProwReturn)>
					</cfif>
				</cfloop>
			</cfif> <!--- Filled previous line --->
		</cfif> <!--- There is a previous line to fill --->
			
		<cfset currentItem = QPQuery.ItemID>
		<cfset currentCatalog = QPQuery.CatalogID>
			
		<cfset currentbreak = 1>
		
		<cfset TotalRows = TotalRows + 1>
		
		<cfset ArrayAppend(buffer,QPQuery.ItemID & Delimiter & QPQuery.SplitCaseCharge & Delimiter & QpQuery.CatalogID & Delimiter)>
		<cfset ArrayAppend(buffer,QPQuery.TierID & Delimiter)>
		
		<!--- caseprice & singleprice --->
		<cfset cs = QPQuery.CasePrice>
		<cfif Find(".",cs) NEQ 0>
			<cfset cs = Left(cs,Find(".",cs)+3)>
		</cfif>
		<cfset ArrayAppend(buffer,cs & Delimiter)>
			
		<cfset ss = QPQuery.SinglePrice>
		<cfif Find(".",ss) NEQ 0>
			<cfset ss = Left(ss,Find(".",ss)+3)>
		</cfif>
		<cfset ArrayAppend(buffer,ss)> <!--- No delimiter! --->
			
		<cfif currentbreak EQ QPmaxBreaks>
			<cfset ArrayAppend(buffer,QProwReturn)>
		<cfelse>
			<cfset ArrayAppend(buffer,Delimiter)>
			<cfset currentbreak = currentbreak + 1>
		</cfif>		
			
	</cfif> <!-- current item & catalog -->
		
</cfloop> <!--- query --->

<!--- Fill out last line --->
<cfif currentbreak LT QPmaxBreaks> <!--- Finish line with filler --->
	<cfloop from="#currentbreak#" to="#QPmaxBreaks#" index="brkFiller">
		<cfset ArrayAppend(buffer,strFiller)>
					
		<cfif brkFiller LT QPmaxBreaks>
			<cfset ArrayAppend(buffer,Delimiter)>
		<cfelse> <!--- brkFiller EQ QPmaxBreaks --->
			<cfset ArrayAppend(buffer,QProwReturn)>
		</cfif>
	</cfloop>
</cfif> <!--- Filled previous line --->

<!--- Convert array mess to huge string --->
<cfset TheData = ArrayToList(buffer,"")>

<!--Pass data back --->
<cfset Caller.rowCount = ToString(TotalRows+1)> <!--- Add 1 for appended rowcount ??? --->
<cfset Caller.compData = TheData>
			
