<cfif j eq "DistributorInfo">
	<cfset queryName = "DistributorInfo">
	<cfquery name="getpercatalogs" datasource="#attributes.datasource#">
		select cataloglist from accountpermissions where accountID=#url.ID#
	</cfquery>
	<cfset cataloglist = getpercatalogs.cataloglist>
	<cfif cataloglist eq "">
		<cfquery name="getcataloglist" datasource="#attributes.datasource#">
			select Distinct CatalogID 
			  from AccountCatalogs, DistributorAccountReps 
			 where AccountCatalogs.AccountID = DistributorAccountReps.AccountID 
			   and DistRepID = #url.id#
		</cfquery>
		<cfset cataloglist = valuelist(getcataloglist.catalogid)>
	</cfif>
	
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT Distinct Isnull(Distributors.CompanyName,'') 
		    + '^' + IsNull(Distributors.ContactName,'')
			+ '^' + IsNull(Accounts.Address1,'')
			+ '^' + IsNull(Accounts.Address2,'')
			+ '^' + IsNull(Accounts.City,'')
			+ '^' + IsNull(Accounts.State,'')
			+ '^' + IsNull(Accounts.Zip,'')
			+ '^' + IsNull(Accounts.Phone,'')
			+ '^' + IsNull(RepAccts.ContactName,'')
		    + '^' + isNull(Distributors.EmailAddress, '') 
			+ '^' + '#cataloglist#'
		    + '^' + IsNull(Distributors.DeliverDays,'1111111')
			+ '^' + IsNull(Distributors.AllowSplitCase,'Y')
			+ '^' + Convert(varchar(4), isnull(Distributors.Hold,0))
			+ '^' + isNull(Distributors.externalURL, '') 
			+ '^' + isNull((Select AttributeValue from AccountAttributes where AccountID=#url.ID# and AttributeName='GuideSortType'), -1) 
			+ '^' + ISNULL((Select AttributeValue from AccountAttributes where AccountID=#url.ID# and AttributeName='MinimumOrder'), -1) 
            As DataString
		FROM Distributors, 
		    DistributorAccounts,
			Accounts, Accounts RepAccts
		WHERE Distributors.DistributorID = DistributorAccounts.DistributorID
		and DistributorAccounts.AccountID = #url.ID#
		and DistributorAccounts.AccountID = RepAccts.AccountID
		and Accounts.AccountID = Distributors.DistributorID
	</cfquery>
<cfset queryColumnList = "CompanyName,ContactName,Address1,Address2,City,State,Zip,Phone,RepName,EmailAddress,catalogIDs,DeliverDays,AllowSplitCase,Hold,externalURL,GuideSortType,MinimumOrder">
</cfif>
