<cfif j eq "AccountSalesData">
	<cfset queryName = "getAccountSalesData">
	<!--- Source of quota data has yet to be determined --->

	<cfquery datasource="#attributes.datasource#" name="queryTemp">
		SELECT a.AccountID,
			convert(varchar, isNull((select sum(InvoiceAmount-AmountCollected) from accountsreceivable where accountID = a.accountID), 0)) AmountBalance,
			convert(varchar, isNull((select sum(totalorder) from masterorder where dsraccount = dar.DistRepID and accountID = a.accountID and siteid = 3 and year(activatedate) = year(getdate()) and month(activatedate) = month(getdate())), 0)) AmountMTD,
			convert(varchar, isNull((select sum(totalorder) from masterorder where dsraccount = dar.DistRepID and accountID = a.accountID and siteid = 3 and year(activatedate) = year(getdate()) and datepart(qq,activatedate) = datepart(qq,getdate())), 0)) AmountQTD,
			convert(varchar, isNull((select sum(totalorder) from masterorder where dsraccount = dar.DistRepID and accountID = a.accountID and siteid = 3 and year(activatedate) = year(getdate())), 0))  AmountYTD,
			convert(varchar, isNull((select sum(totalorder) from masterorder where dsraccount = dar.DistRepID and accountID = a.accountID and siteid = 3 and activatedate between DateAdd(yy, DateDiff(yy, 0, GetDate()) -1, 0) and DateAdd(yy, -1, GetDate()) ), 0)) AmountLYTD,
			convert(varchar, isNull((select sum(InvoiceAmount) from accountsreceivable where accountID = a.accountID and invoiceduedate < getDate() and invoiceduedate <> '1900-01-01 00:00:00'), 0)) AmountPastDue,
			0 QuotaBalance,
			0 QuotaMTD,
			0 QuotaQTD,
			0 QuotaYTD,
			0 QuotaLYTD,
			0 QuotaPastDue
		FROM Accounts a, DistributorAccountReps dar
		where a.accountID = dar.AccountID
		and dar.DistRepID = #url.id#
		and (a.DeletedFlag is NULL or a.DeletedFlag = 0)
	</cfquery>
	<cfset queryName = "AccountSalesData">
	<cfset AccountSalesData = QueryNew("LineData")>
	
	<cfloop query="queryTemp">
		<cfset tLineData = queryTemp.AmountBalance & '^' &
    	queryTemp.AmountMTD & '^' &
	    queryTemp.AmountQTD & '^' &
		queryTemp.AmountYTD & '^' &
		queryTemp.AmountLYTD & '^' &
		queryTemp.AmountPastDue & '^' & 
    	queryTemp.QuotaBalance & '^' &
	    queryTemp.QuotaMTD & '^' &
		queryTemp.QuotaQTD & '^' &
		queryTemp.QuotaYTD & '^' &
		queryTemp.QuotaLYTD & '^' &
		queryTemp.QuotaPastDue & '^'>

		<cfloop from= "1" to="10" index="i">
			<cfset tLineData = tLineData & '0^' &
			'0^' &
			'0^' &
			'0^'>
		</cfloop>

		<cfset tLineData = tLineData & queryTemp.AccountID>
		<CFSET temp = QueryAddRow(AccountSalesData)>
		<cfset temp = QuerySetCell(AccountSalesData, "LineData", tLineData)>
	</cfloop>

<cfset queryColumnList = "AmountBalance,AmountMTD,AmountQTD,AmountYTD,AmountLYTD,AmountPastDue,QuotaBalance,QuotaMTD,QuotaQTD,QuotaYTD,QuotaLYTD,QuotaPastDue,Top1,TopCases1,TopSingles1,TopSales1,Top2,TopCases2,TopSingles2,TopSales2,Top3,TopCases3,TopSingles3,TopSales3,Top4,TopCases4,TopSingles4,TopSales4,Top5,TopCases5,TopSingles5,TopSales5,Top6,TopCases6,TopSingles6,TopSales6,Top7,TopCases7,TopSingles7,TopSales7,Top8,TopCases8,TopSingles8,TopSales8,Top9,TopCases9,TopSingles9,TopSales9,Top10,TopCases10,TopSingles10,TopSales10,AccountID">
</cfif>