<!--- qry_openorders.cfm --->

<cfif j eq "OpenOrders">
	<cfif isDefined("url.UD")>
		<cfquery datasource="#attributes.datasource#" name="getLastDate">
		select attributevalue from accountattributes where accountID=#url.ID# and attributename='#j#UD'
		</cfquery>
	</cfif>

	<cfset queryName = "getOpenOrders">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
   		SELECT InvoiceNumber + '^' +
			CONVERT(varchar, InvoiceDate) + '^' +
			CONVERT(varchar, InvoiceTotal) + '^' +
			CONVERT(varchar, AccountID) + '^' +
			CONVERT(varchar, count(InvoiceNumber)) + '^' +
			IsNull(TSOrderType, '')
		AS DataString
		FROM OpenOrders
		WHERE DistRepID=#url.id#
		<cfif isDefined("getLastDate") and getLastDate.recordcount neq 0>
			and UpdateDate > '#getLastDate.attributevalue#'
		</cfif>
		GROUP BY InvoiceNumber, InvoiceDate, InvoiceTotal, AccountID, TSOrderType
	</cfquery>

	<cfset queryColumnList = "InvoiceNumber,InvoiceDate,InvoiceTotal,AccountID,ItemCount,TSOrderType">
</cfif>