<cfif j eq "AR">
	<cfset queryName = "AR">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT Convert(varchar, AccountID)
			+ '^' + 
			CASE WHEN DistRepID IS NULL THEN '0'
			WHEN DistRepID = #URL.ID# THEN convert(varchar, DistRepID)
			ELSE '0'
			END
		/*	+ '^' + isNull(convert(varchar, DistRepID), '0') */
			+ '^' + InvoiceNo
			+ '^' + isNull(convert(varchar, InvoiceDate, 101), '01-01-1900') 
			+ '^' + isNull(convert(varchar, InvoiceDueDate, 101), '01-01-1900') 
			+ '^' + isNull(Convert(varchar, InvoiceAmount), 0)
			+ '^' + isNull(Convert(varchar, PaymentType), 0)
			+ '^' + isNull(Convert(varchar, AmountCollected), '0')
			+ '^' + 
			CASE WHEN AmountCollected <> InvoiceAmount THEN
				'0'
			ELSE
				'1'
			END 
			+ '^' + isNull(Convert(varchar, PaymentStatus), 0)
			+ '^' + isNull(Comments, '')
		AS DataString
		FROM AccountsReceivable
		WHERE AccountID in (#valuelist(check_login.accountID)#)
	</cfquery>

	<cfset queryColumnList = "AccountID,DistRepID,InvoiceNo,InvoiceDate,InvoiceDueDate,InvoiceAmount,PaymentType,AmountCollectedTotal,collected,PaymentStatus,Comments">
</cfif>
