<cfif j eq "CatalogItems">
	<cfquery datasource="#attributes.datasource#" name="getDiv">
	select DivisionList
	from AccountPermissions
	where accountID = #url.id#
	</cfquery>
	<cfset queryName = "getCatalogItems">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
   SELECT CONVERT(varchar(8), CatalogItems.itemID) + '^' +
	      CONVERT(varchar(8), CatalogItems.catalogID) + '^' +
		  CONVERT(varchar(1), CatalogItems.caseonly) AS DataString
	 FROM CatalogItems,
	      DistributorCatalog,
		  DistributorItems
	WHERE CatalogItems.CatalogID = DistributorCatalog.CatalogID
	  and DistributorCatalog.DistributorID = #check_login.DistributorID#
	  and CatalogItems.CatalogID in (#CatalogList#)
	  and distributoritems.itemID = catalogItems.itemID
		<cfif (getDiv.recordcount neq 0) and (trim(getdiv.DivisionList) neq "")>
		and DistributorItems.DivisionID in (#getDiv.DivisionList#)
		</cfif>
		and (distributorItems.deletedflag is null or distributorItems.deletedflag = 0)
	</cfquery>
<cfset queryColumnList = "itemID,catalogID,caseonly">
</cfif>