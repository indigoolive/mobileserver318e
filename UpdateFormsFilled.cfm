<cfsetting enablecfoutputonly="yes">



<cftry>
	<cfset compressedOutput = "">
	<cfif NOT isdefined("form.FormID") OR
			NOT isdefined("form.AccountID") OR
			NOT isdefined("form.QuestionID") OR
			NOT isdefined("form.Answer")>
		<cfset compressedOutput = "<ERROR>ERROR 1001: Transaction incomplete please try again.</ERROR>">
	<cfelse>
		<cfset FormAnswer = form.Answer>
		<cfset FormAnswer = Replace(FormAnswer, "~|A", "&", "ALL")>
		<cfset FormAnswer = Replace(FormAnswer, "'", "''", "ALL")>

		<cfquery datasource="#attributes.datasource#" name="FindEntry">
			SELECT FormID
			FROM FormsFilled
			WHERE FormID = #form.FormID#
			AND DistRepID = #url.ID#
			AND AccountID = #form.AccountID#
	   		AND QuestionID = #form.QuestionID#
		</cfquery>
		
		<cfif FindEntry.RecordCount GT 0>
			<cfquery datasource="#attributes.datasource#" name="UpDateEntry">
				UPDATE FormsFilled
				SET Answer = '#FormAnswer#', Updated = 1, UpdateDate = GetDate()
				WHERE FormID = #form.FormID#
				AND DistRepID = #url.ID#
				AND AccountID = #form.AccountID#
	   			AND QuestionID = #form.QuestionID#
			</cfquery>
		<cfelse>
			<cfquery datasource="#attributes.datasource#" name="InsertEntry">
				INSERT INTO FormsFilled (FormID, DistrepID, AccountID, QuestionID, Updated, UpdateDate, Answer)
				VALUES (#form.FormID#, #url.ID#, #form.AccountID#, #form.QuestionID#, 1, GetDate(), '#FormAnswer#')
			</cfquery>
		</cfif>
	</cfif>

<cfcatch type="any">
	<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject="Error updating Forms">
		Error 2002: Error adding data to BeverageOne database
		Template: UpdateFormsFilled.cfm
		AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
		Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
		Type: #cfcatch.type#
		Message: #cfcatch.message#
		
		SQL: <cfif isdefined("cfcatch.Sql")>#cfcatch.Sql#<cfelse>N/A</cfif>
		
		Details: #cfcatch.detail#
		
		<CFLOOP index=i from=1 to = #ArrayLen(CFCATCH.TAGCONTEXT)#>
			<CFSET sCurrent = #CFCATCH.TAGCONTEXT[i]#>
			<BR>#i# #sCurrent["ID"]# (#sCurrent["LINE"]#,#sCurrent["COLUMN"]#) #sCurrent["TEMPLATE"]#
		</CFLOOP>

		<cfif isDefined("form.fieldnames")>
			<cfloop list="#form.fieldnames#" index="i">
				#i#: #evaluate("form.#i#")#
			</cfloop>
		</cfif>
	</cfmail>
	
	<cfset compressedOutput = "<ERROR>Some error has occured in trasmission. Support has been notified.</ERROR>">
</cfcatch>
</cftry>

<cfif compressedOutput eq "">
	<cfset compressedOutput = "SUCCESS">
</cfif>

<cfinclude template="act_zipoutput2.cfm">
