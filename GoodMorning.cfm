<cfmodule template="/Common/other/logger.cfm" accountID="#url.ID#" accounttype="#getDatasource.accounttype#" moduletype="7">
<CFSETTING ENABLECFOUTPUTONLY="Yes">


<cfset XMLString = "">

<cfquery datasource="#attributes.datasource#" name="check_login">
select DistributorAccountReps.accountID,
       DistributorAccountReps.DistributorID
  from Accounts,
       DistributorAccountReps
 where Accounts.accountID=#url.ID#
<cfif url.p NEQ masterpass>     
   and Accounts.password='#url.p#'
</cfif>
   and Accounts.LoginEnabled = 'Y'
   and Accounts.AccountID = DistributorAccountReps.DistRepID
</cfquery>
<cfif check_login.recordcount eq 0>
	<cfset XMLString = "0">
</cfif>

<cfif XMLString eq "">
	<cfset XMLString="<B1XML Version=1.0 Type=GM>">
	<cfset XMLString = XMLString & "<DATA>">
	<cfset XMLString = XMLString & "<YEAR>" & datePart("yyyy", dateconvert('local2Utc', now())) &"</YEAR>">
	<cfset XMLString = XMLString & "<MONTH>" & datePart("m", dateconvert('local2Utc', now())) &"</MONTH>">
	<cfset XMLString = XMLString & "<DAY>" & datePart("d", dateconvert('local2Utc', now())) &"</DAY>">
	<cfset XMLString = XMLString & "<HOUR>" & datePart("h", dateconvert('local2Utc', now())) &"</HOUR>">
	<cfset XMLString = XMLString & "<MINUTE>" & datePart("n", dateconvert('local2Utc', now())) &"</MINUTE>">
	<cfset XMLString = XMLString & "<SECOND>" & datePart("s", dateconvert('local2Utc', now())) &"</SECOND>">
	<cfset XMLString = XMLString & "</DATA>">
	<cfset XMLString = XMLString & "</B1XML>">
</cfif>

<cfset compressedoutput = XMLSTRING>

<!---
<cfset tempFileName = GetTempFile(GetTempDirectory(), "gz_")>
<cffile action="WRITE" file="#tempFileName#" output="#compressedOutput#" addnewline="no">
<cfx_gzip action="gzip" Infile="#tempFileName#" outfile="#tempFileName#.gz" level="9">
<cffile action="delete" file="#tempFileName#">
<cfcontent deletefile="yes" file="#tempFileName#.gz" type="text/plain">
<cfif fileExists("#tempFileName#.gz")>
	<cffile action="delete" file="#tempFileName#">
</cfif>
--->

<cfset TempDir = "P:\RENEW_TMP\">
<cfset tempFileName = GetTempFile(#TempDir#, "gz_")>

<cftry>
	<cfinvoke component = "Zip" method = "gzipAddFile" gzipFilename = "#tempFileName#.gz" inputFilename = "#tempFileName#">

	<cffile action="delete" file="#tempFileName#">

    	<cfcontent deletefile="yes" file="#tempFileName#.gz" type="text/plain">
	
<cfcatch type="any">
	<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject="GZ Download Error">
		Error 16600: General Error (cfinvoke or cfcontent Failed)
		Template: GoodMorning.cfm
		AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
		Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
		Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
		Type: #cfcatch.type#
		Message: #cfcatch.message#

		SQL: <cfif isdefined("cfcatch.sql")>#cfcatch.sql#<cfelse>N/A</cfif>

		Details: #cfcatch.detail#

		<CFLOOP index=i from=1 to = #ArrayLen(CFCATCH.TAGCONTEXT)#>
			<CFSET sCurrent = #CFCATCH.TAGCONTEXT[i]#>
			<BR>#i# #sCurrent["ID"]# (#sCurrent["LINE"]#,#sCurrent["COLUMN"]#) #sCurrent["TEMPLATE"]#
		</CFLOOP>

		<cfif isDefined("form.fieldnames")>
			<cfloop list="#form.fieldnames#" index="i">
			#i#: #evaluate("form.#i#")#
			</cfloop>
		</cfif>
	
		<cfif isDefined("tempFileName")>
			tempFileName is #tempFileName#
		</cfif>
	</cfmail>
</cfcatch>
</cftry> 	
