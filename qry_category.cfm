<cfif j eq "Category">
	<cfset queryName = "Category">
	<cfquery name="#queryName#" datasource="#attributes.datasource#">
		Select Distinct Convert(varchar, cat.CategoryID) + '^' + 
			CASE WHEN cat.Parent is null THEN
				Convert(varchar, cat.categoryID)
			ELSE
				Convert(varchar, cat.Parent)
			END  + '^' +
			cat.Description + '^' +
			CASE WHEN (select count(*) from category where category.parent = cat.categoryID) > 0 THEN
				'1'
			ELSE
				'0'
			END as DataString
			FROM Category cat
			WHERE (CategoryID > 99 and Parent is NULL)
			or (CategoryID > 99 and HasChildren = 1)
			or (CategoryID in (select distinct categoryid
						from distributoritems
						where deletedflag <> 1
						or deletedflag is null)
				and Parent is not NULL)


<!----    where CategoryID > 99 and ( categoryid in ( select distinct categoryid from distributoritems where deletedflag <> 1 or deletedflag is null) and Parent is not NULL) ---->
<!----            or ( Parent is NULL and CategoryID in ( select distinct Parent from category, DistributorItems where DistributorItems.CategoryID = Category.CategoryID and deletedflag <> 1 or deletedflag is null)) --->
	</cfquery>
	<cfset queryColumnList = "categoryID,Parent,Description,HasChildren">
</cfif>
