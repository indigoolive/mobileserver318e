<cftry>

<cfmodule template="/Common/other/logger.cfm" accountID="#url.ID#" accounttype="#getDatasource.accounttype#" moduletype="6">
<cfsetting enablecfoutputonly="yes">

<cfif compressedOutput eq "">
	<cfquery name="GetCatalogList" datasource="#attributes.datasource#">
		select * from AccountPermissions where AccountID = #Url.ID#
	</cfquery>
	
	<cfif isDefined("GetCatalogList.CatalogList") AND trim(GetCatalogList.CatalogList) NEQ "">
		<cfset CatalogList = GetCatalogList.CatalogList>
	<cfelse>
		<cfquery name="GetValidCatalogs" datasource="#attributes.datasource#">
			Select Distinct CatalogID 
			  from AccountCatalogs
			 where AccountID in ( 1,#valuelist(check_login.accountID)# )
		</cfquery>
		<cfset CatalogList = valuelist(GetValidCatalogs.CatalogID)>
	</cfif>
	
	<cfif not isDefined("url.tablenames")>
		<cfset tables = "Accounts,Items,Division,ApportionedItems,Category,PricingScheme,DistributorRepNotes,ProductNotes,WorkOrder,WorkOrderItems,OrderHistory,OrderHistoryItems,DistributorPromotions,CatalogItems,DistributorInfo,ar,AccountSalesData,ItemHistory,OpenOrders,OpenOrderItems,AccountAttributes,Surveys,Questions,RIP,RIPItems,Program,ProgramItems,OrderTerms,OrderDiscounts">
	<cfelse>
		<cfset tables = url.tablenames>
	</cfif>

	<cfset XMLString = "">
	<cfset inc = 1>
	
	<cfloop list="#tables#" index="j">
		<cfinclude template="qry_accounts.cfm">
		<cfinclude template="qry_getitems.cfm"> 
	 	<cfinclude template="qry_divisions.cfm">
	 	<cfinclude template="qry_apportioned.cfm">
		<cfinclude template="qry_category.cfm">
		<cfinclude template="qry_pricescheme.cfm"> 
		<cfinclude template="qry_repnotes.cfm">
		<cfinclude template="qry_productnotes.cfm">
		<cfinclude template="qry_workorder.cfm">
		<cfinclude template="qry_workorderitems.cfm">
		<cfinclude template="qry_orderhistory.cfm">
		<cfinclude template="qry_orderhistoryitems.cfm">
		<cfinclude template="qry_promotions.cfm">
		<cfinclude template="qry_catalogItems.cfm">
		<cfinclude template="qry_distributor.cfm">
		<cfinclude template="qry_getinvenonly.cfm">
		<cfinclude template="qry_salesdata.cfm">
		<cfinclude template="qry_ar.cfm">
		<cfinclude template="qry_warehouse.cfm">
		<cfinclude template="qry_shipmethods.cfm">
		<cfinclude template="qry_messages.cfm">
		<cfinclude template="qry_itemhistory.cfm">
		<cfinclude template="qry_familyplan.cfm">
		<cfinclude template="qry_openorders.cfm">
		<cfinclude template="qry_openorderitems.cfm">
		<cfinclude template="qry_accountattributes.cfm">
		<cfinclude template="qry_ItemAttributes.cfm">
		<cfinclude template="qry_surveys.cfm">
		<cfinclude template="qry_questions.cfm">
		<cfinclude template="qry_rip.cfm">
		<cfinclude template="qry_ripitems.cfm">
		<cfinclude template="qry_Program.cfm">
		<cfinclude template="qry_ProgramItems.cfm">
		<cfinclude template="qry_OrderTerms.cfm">
		<cfinclude template="qry_OrderDiscounts.cfm">
		<cfinclude template="qry_ManagerReps.cfm">
		<cfinclude template="qry_PricingTiers.cfm">
		<cfinclude template="qry_FullCatPath.cfm">
		<cfinclude template="qry_Forms.cfm">
		<cfinclude template="qry_FormQuestions.cfm">
		<cfinclude template="qry_FormsFilled.cfm">
		<cfinclude template="qry_Filters.cfm">
 
		<cfset queryRecordCount = evaluate("#queryName#.recordcount")>
		
		<cfset compressedOutput = compressedOutput & j & chr(10) & queryColumnList & chr(10)>
		
		<cfif j neq "PricingScheme" OR (options.pricingmodel neq 2 AND j eq "PricingScheme")>
			<cfset compressedOutput = compressedOutput & queryRecordCount & chr(10)>
		</cfif>
		
		<cfif evaluate("#queryName#.recordcount") neq 0>
			<cfif (j neq "PricingScheme") or (options.pricingmodel neq 2)>
				<cfx_concatmx query="#queryname#" ID="#url.id#">
				<cfset compressedOutput = compressedOutput & compData>
			<cfelse>
			
				<cfif options.RelationalPricingScheme neq 2>
					<!--- <cfx_QtyPricingMX Query="pricingscheme" ID="#url.id#" maxBreaks="#options.maxpricebreaks#" rowReturn="#chr(10)#"> --->
					<cf_QuanPricing Query="#pricingscheme#" ID="#url.id#" maxBreaks="#options.maxpricebreaks#" rowReturn="#chr(13) & chr(10)#">
				<cfelse>
					<cfx_QtyFlagPricingMX Query="pricingscheme" ID="#url.id#" maxBreaks="#options.maxpricebreaks#" rowReturn="#chr(10)#">
				</cfif>
				
				<cfset compressedOutput = compressedOutput & rowcount & chr(10)>
				<cfset compressedOutput = compressedOutput & compData>
			</cfif>
		<cfelse>
			<cfset compressedOutput = compressedOutput & " ">
		</cfif>
		
		<cfif inc neq ListLen(tables)>
			<cfset compressedOutput = compressedOutput & "######DATA######">
		</cfif>
		
		<cfset inc = inc + 1>
		
		<cfif j EQ "Items">
			<cfquery datasource="#attributes.datasource#" name="deleteUpdateAttribute">
				delete from accountattributes where accountID=#url.ID# and attributename='InventoryUD'
			</cfquery>

			<cfquery datasource="#attributes.datasource#" name="insertUpdateAttribute">
				insert into accountattributes(accountID,attributename,attributevalue) values(#url.ID#,'InventoryUD',convert(varchar, getdate(), 109))
			</cfquery>		
		</cfif>
		
		<cfquery datasource="#attributes.datasource#" name="deleteUpdateAttribute">
			delete from accountattributes where accountID=#url.ID# and attributename='#j#UD'
		</cfquery>

		<cfquery datasource="#attributes.datasource#" name="insertUpdateAttribute">
			insert into accountattributes(accountID,attributename,attributevalue) values(#url.ID#,'#j#UD',convert(varchar, getdate(), 109))
		</cfquery>
	</cfloop>
</cfif>

<cfcatch type="any">
		<cfset compressedOutput = "<ERROR>General Data Error. BeverageOne has been notified.</ERROR>">
		
		<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject="General Data Access Error">
			Error 5000: General Error (Truncate Failed)
			Template: GrabData.cfm
			AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
			Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
			Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
			Type: #cfcatch.type#
			Message: #cfcatch.message#
			
			SQL: <cfif isdefined("cfcatch.sql")>#cfcatch.sql#<cfelse>N/A</cfif>
			
			Details: #cfcatch.detail#

			<CFLOOP index=i from=1 to = #ArrayLen(CFCATCH.TAGCONTEXT)#>
				<CFSET sCurrent = #CFCATCH.TAGCONTEXT[i]#>
				<BR>#i# #sCurrent["ID"]# (#sCurrent["LINE"]#,#sCurrent["COLUMN"]#) #sCurrent["TEMPLATE"]#
			</CFLOOP>

			<cfif isDefined("form.fieldnames")>
				<cfloop list="#form.fieldnames#" index="i">
					#i#: #evaluate("form.#i#")#
				</cfloop>
			</cfif>

		</cfmail>
	</cfcatch>
</cftry> 

<cfinclude template="act_zipoutput2.cfm">
