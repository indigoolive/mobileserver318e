<!-----------------------------------------------------------------------------------
*
*        Application: newsight Zip Component
*          File Name: Zip.cfc
* CFC Component Name: Zip
*            Support: ColdFusion MX 6.0, ColdFusion MX 6.1, ColdFusion MX 7
*         Created By: Artur Kordowski - info@newsight.de
*            Created: 16.06.2005
*        Description: A collections of functions that supports the Zip and GZip functionality by using the
*                     Java Zip file API.
*
*    Version History: [dd.mm.yyyy]   [Version]   [Author]        [Comments]
*                      16.06.2005     0.1 Beta    A. Kordowski    Beta status reached.
*                      27.06.2005     1.0         A. Kordowski    Component complete.
*                      07.08.2005     1.1         A. Kordowski    Fixed some bugs. Add GZip functionality. New functions
*                                                                 gzipAddFile() and gzipExtract().
*                      02.10.2005     1.2         A.Kordowski     Fixed bug for ColdFusion MX 6.
*
*           Comments: [dd.mm.yyyy]   [Version]   [Author]        [Comments]
*                      27.06.2005     0.1 Beta    A. Kordowski    Thanks a lot to Warren Sung for testing the Component with
*                                                                 ColdFusion MX 6.1 on Linux Debian 3.1.
*                      27.06.2005     0.1 Beta    A. Kordowski    Component tested with ColdFusion MX 7 on Windows XP Professional.
*                      29.06.2005     1.0         A. Kordowski    Created documentation.
*                      01.07.2005     1.0         A. Kordowski    Release component.
*                      08.08.2005     1.1         A. Kordowski    Update documentation.
*
*               Docs: http://livedocs.newsight.de/com/Zip/
*
*             Notice: For comments, bug reports or suggestions to optimise this component, feel free to send
*                     a E-Mail: info@newsight.de
*
*            License: THIS IS A OPEN SOURCE COMPONENT. YOU ARE FREE TO USE THIS COMPONENT IN ANY APPLICATION,
*                     TO COPY IT OR MODIFY THE FUNCTIONS FOR YOUR OWN NEEDS, AS LONG THIS HEADER INFORMATION
*                     REMAINS IN TACT AND YOU DON'T CHARGE ANY MONEY FOR IT. USE THIS COMPONENT AT YOUR OWN
*                     RISK. NO WARRANTY IS EXPRESSED OR IMPLIED, AND NO LIABILITY ASSUMED FOR THE RESULT OF
*                     USING THIS COMPONENT.
*
*                     THIS COMPONENT IS LICENSED UNDER THE CREATIVE COMMONS ATTRIBUTION-SHAREALIKE LICENSE.
*                     FOR THE FULL LICENSE TEXT PLEASE VISIT: http://creativecommons.org/licenses/by-sa/2.5/
*
			 extends	 = "mura.cfobject"

* ----------------------------------------------------------------------------------->

<cfcomponent	displayname	= "Zip Component"
             	hint        	= "A collections of functions that supports the Zip and GZip functionality by using the Java Zip file API."
		output		= "false"
>

	<cfscript>
		/* Create Objects */
		ioInput     = CreateObject("java","java.io.FileInputStream");
		ioOutput    = CreateObject("java","java.io.FileOutputStream");
		gzInput     = CreateObject("java","java.util.zip.GZIPInputStream");
		gzOutput    = CreateObject("java","java.util.zip.GZIPOutputStream");

	</cfscript>

	<!--- gzipAddFile --->
	<cffunction name="gzipAddFile" access="public" output="no" returntype="boolean" hint="Create a new GZip file archive.">

		<!--- Function Arguments --->
		<cfargument name="gzipFilename" required="yes" type="string" hint="GZip file to create.">
		<cfargument name="inputFilename"   required="yes" type="string" hint="File to add to the GZip file.">

		<cfscript>

			/* Default variables */
			var l = 0;
			var buffer     = RepeatString(" ",1024).getBytes();

			try
			{

				/* Set output gzip file name */
				ioInput.init(arguments.inputFilename);
				ioOutput.init(arguments.gzipFilename);

				gzOutput.init(ioOutput);

				l = ioInput.read(buffer);

				while(l GT 0)
				{
					gzOutput.write(buffer, 0, l);
					l = ioInput.read(buffer);
				}

				/* Close the GZip file */
				gzOutput.close();
				ioOutput.close();
				ioInput.close();

				/* Return true */
				return true;
			}

			catch(Any expr)
			{ return false; }

		</cfscript>

	</cffunction>
</cfcomponent>
