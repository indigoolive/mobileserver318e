<cfif j eq "WorkOrderItems">
	<cfset queryName = "WorkOrderItems">
	<cfquery datasource="#attributes.datasource#" name="getDiv">
	select DivisionList
	  from AccountPermissions
	 where accountID = #url.id#
	</cfquery>
	<cfquery datasource="#attributes.datasource#" name="isTeleMktRep">
		Select AttributeValue
		  from AccountAttributes
		 where AttributeName = 'TeleMarketRep'
		   and AccountID = #URL.ID#
	</cfquery>

	<cfquery datasource="#attributes.datasource#" name="UseOrderCountFlag">
		select AttributeName from AccountAttributes where AccountID = #getDistributor.DistributorID# and AttributeName='UseOrderCount'
	</cfquery>

	<cfquery datasource="#attributes.datasource#" name="UseOpenOrderCountFlag">
		select AttributeName from AccountAttributes where AccountID = #getDistributor.DistributorID# and AttributeName='UseOpenOrderCount'
	</cfquery>

	<cfquery datasource="#attributes.datasource#" name="TurnOffSpecialFlag">
		select AttributeName from AccountAttributes where AccountID = #getDistributor.DistributorID# and AttributeName='TurnOffSpecial'
	</cfquery>

	<cfquery datasource="#attributes.datasource#" name="TurnOffDiscountFlag">
		select AttributeName from AccountAttributes where AccountID = #getDistributor.DistributorID# and AttributeName='TurnOffDiscount'
	</cfquery>

	<cfquery datasource="#attributes.datasource#" name="TurnOffOverrideFlag">
		select AttributeName from AccountAttributes where AccountID = #getDistributor.DistributorID# and AttributeName='TurnOffOverride'
	</cfquery>



 	<cfquery datasource="#attributes.datasource#" name="#queryName#">
	SELECT Convert(varchar,isnull(woi.AccountID,0)) + '^' +
		Convert(varchar,isnull(woi.WorkOrderID,0))+ '^' +
		Convert(varchar,isnull(woi.ItemID,0)) + '^' +
		'0^' +
		'0^' +
		<cfif options.SpecialFlagMatchesTier>
			Convert(varchar,isnull(p.caseprice,0)) + '^' +
			Convert(varchar,isnull(p.singleprice,0)) + '^' +
		<cfelse>
			Convert(varchar, isnull(CPrice,0)) + '^' + 
			Convert(varchar, isnull(SPrice,0)) + '^' + 
		</cfif>

		Convert(varchar,isnull((CLastQty*QtyPerCase)+SLastQty,0)) + '^' +
		<cfif isTeleMktRep.recordcount GT 0>
			Convert(varchar,isnull((select count(*)
						  from MasterOrder, MasterOrderItems
						 where MasterOrder.OrderNumber = MasterOrderItems.OrderNumber
						   and MasterOrderItems.ItemID = DistributorItems.ItemID
						   and MasterOrder.AccountID = da.AccountId
						   and MasterOrderItems.distrepid = #URL.ID#
						   and (SiteID >= 3 or (siteid < 3 and masterorder.distinvoice not in (select distinvoice from masterorder where siteid >= 3)))
						   and datediff(day, transdate, GetDate()) < #isTeleMktRep.AttributeValue#)
						,0)) + '^' +
		<cfelseif UseOrderCountFlag.recordcount GT 0>
			Convert(varchar,isnull((select count(*)
						  from MasterOrder, MasterOrderItems
						 where MasterOrder.OrderNumber = MasterOrderItems.OrderNumber
						   and MasterOrderItems.ItemID = DistributorItems.ItemID
						   and MasterOrder.AccountID = da.AccountId
				   		   <cfif options.FilterHistoryByRep>
						   and MasterOrderItems.distrepid = #URL.ID#
						   </cfif>
						   and SiteID = 3 
						   and datediff(day, transdate, GetDate()) < #options.orderhistorydays#)
						,0)) + '^' +
		<cfelseif UseOpenOrderCountFlag.recordcount GT 0>
			Convert(varchar, isnull ((select sum(QtyOrdered) 
						from OpenOrders 
						where ItemID = woi.ItemID 
						and AccountID = woi.AccountID)
						, 0)) + '^' +
		<cfelse>
			'0^' +
		</cfif>
		Convert(varchar,isnull(woi.LineItemNumber,0)) + '^' +
		<cfif TurnOffSpecialFlag.recordcount GT 0>
			' ^' +
		<cfelse>
			isnull(attr1.AttributeValue, ' ') + '^' +
		</cfif>
		<cfif TurnOffDiscountFlag.recordcount GT 0>
			'0.00^' +
		<cfelse>
			isnull(attr2.AttributeValue, '0.00') + '^' +
		</cfif>
		<cfif TurnOffOverrideFlag.recordcount GT 0>
			'0.00^' +
		<cfelse>
			isnull(attr3.AttributeValue, '0.00') + '^' +
		</cfif>
		Convert(varchar,isnull(ac.catalogID, 10)) + '^' +
		Convert(varchar,isnull(woi.par1, 0)) + '^' +
		Convert(varchar,isnull(woi.inv, 0)) + '^' +
		Convert(varchar,isnull(woi.RetailPrice, 0)) + '^' +
		Convert(varchar,isnull(woi.warehouse, 0)) + '^' +
		<!--- Filter --->
		Convert(varchar, 0) 

	AS DataString
	FROM WorkOrder wo,
		WorkOrderItems woi,
		DistributorAccountReps da,
		<cfif TurnOffSpecialFlag.recordcount EQ 0>
			WorkOrderItemAttributes attr1,
		</cfif>
		<cfif TurnOffDiscountFlag.recordcount EQ 0>
			WorkOrderItemAttributes attr2,
		</cfif>
		<cfif TurnOffOverrideFlag.recordcount EQ 0>
			WorkOrderItemAttributes attr3,
		</cfif>
		<cfif options.SpecialFlagMatchesTier>
			PricingScheme p,
		</cfif>
		DistributorItems di,
		AccountCatalogs ac
	where wo.accountid = da.accountid
		and wo.salesman  = (select min(Salesman) from DistributorAccountReps dar where dar.DistRepID = #url.id# and dar.AccountID = wo.AccountID)
		and wo.salesman  = da.salesman
		and da.DistRepID = #url.id#
		and wo.AccountID = woi.AccountID
		and wo.WorkOrderID = woi.WorkOrderID
		and wo.wotype = 1
		and woi.ItemID = di.ItemID
		and (di.DeletedFlag is NULL or di.DeletedFlag = 0)
		and ac.accountid = wo.accountid
		<cfif TurnOffSpecialFlag.recordcount EQ 0>
			and ( woi.WorkOrderID *= attr1.WorkOrderID
			and   woi.LineItemNumber *= attr1.LineItemNumber
			and   woi.AccountID *= attr1.AccountID 
			and   attr1.AttributeName='SpecialFlag' )
		</cfif>
		<cfif TurnOffDiscountFlag.recordcount EQ 0>
			and ( woi.WorkOrderID *= attr2.WorkOrderID
			and   woi.LineItemNumber *= attr2.LineItemNumber
			and   woi.AccountID *= attr2.AccountID 
			and   attr2.AttributeName='DiscountAmount' )
		</cfif>
		<cfif TurnOffOverrideFlag.recordcount EQ 0>
			and ( woi.WorkOrderID *= attr3.WorkOrderID
			and   woi.LineItemNumber *= attr3.LineItemNumber
			and   woi.AccountID *= attr3.AccountID 
			and   attr3.AttributeName='PriceOverride' )
		</cfif>
		<cfif options.SpecialFlagMatchesTier>
			and woi.itemID = p.itemID
			and woi.catalogID = p.catalogID
			and p.tierID = isNull((select attributevalue from WorkOrderItemAttributes where workorderID=woi.workorderID and accountID=woi.accountID and lineitemnumber=woi.lineitemnumber and attributeName='SpecialFlag'), 100)
		</cfif>
		<cfif trim(CatalogList) NEQ "">
			and ac.catalogID in (#CatalogList#)
		</cfif>
		<cfif (getDiv.recordcount neq 0) and (trim(getdiv.DivisionList) neq "")>
			and di.DivisionID in (#getDiv.DivisionList#)
		</cfif>
	</cfquery>

	<cfset queryColumnList = "AccountID,WorkOrderID,ItemID,CaseQTY,SingleQTY,CDistPrice,SDistPrice,LastQty,OrderCount,LineItemNumber,SpecialFlag,DiscountAmount,PriceOverride,catalogID,par,inv,RetailPrice,Warehouse,Filter">
</cfif>


