<cfif j eq "Questions">
	<cfset queryName = "Questions">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT SurveyID,
		       QuestionID,
		       Type,
		       AllowComment,
		       Mandatory,
		       Question
		  FROM Questions
		 WHERE SurveyID in (SELECT SurveyID from Survey where (DistRepID = #URL.ID# or DistRepID is NULL))
	</cfquery>

	<cfset QuestionAndSelections = QueryNew("LineData")>

	<cfloop query = "#queryName#">
		<cfset temp = QueryAddRow(QuestionAndSelections)>
		<cfset tLineData = SurveyID>

		<cfset tLineData = tLineData & '^' & Questions.QuestionID>
		<cfset tLineData = tLineData & '^' & Questions.Type>
		<cfset tLineData = tLineData & '^' & Questions.Question>
		<cfset tLineData = tLineData & '^' & Questions.AllowComment>
		<cfset tLineData = tLineData & '^' & Questions.Mandatory>

		<cfif #Questions.Type# EQ 1>
			<cfset tLineData = tLineData & '^'>
		<cfelse>
			<cfquery datasource="#attributes.datasource#" name="Selections">
				SELECT Selection
				  FROM Selections
				 WHERE SurveyID = #SurveyID#
				   AND QuestionID = #QuestionID#
			</cfquery>

			<cfset tLineData = tLineData & '^'>
	
			<cfloop query = "Selections">
				<cfset tLineData = tLineData & '|' & Selection>
			</cfloop>
		</cfif>

		<cfset temp = QuerySetCell(QuestionAndSelections, "LineData", tLineData)>
	</cfloop>

	<cfset queryColumnList = "SurveyID,QuestionID,Type,Question,AllowComment,Mandatory,Selections">
	<cfset queryName = "QuestionAndSelections">

</cfif>