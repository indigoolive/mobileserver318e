<cfif j eq "DistributorRepNotes">
	<cfset queryName = "DistributorRepNotes">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar, DistRepID) 
		    + '^' + CONVERT(varchar, AccountID) 
		    + '^' + CONVERT(varchar, DateEntered,120) 
		    + '^' + replace(replace(Comment,char(13),char(32)), char(10), char(32))
			+ '^0' AS DataString
		FROM DistributorRepNotes
		where DistRepID = #url.ID#
	</cfquery>
<cfset queryColumnList = "distrepID,accountID,dateEntered,Comment,Updated">
</cfif>