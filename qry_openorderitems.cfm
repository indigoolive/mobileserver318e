<!--- qry_openorderitems.cfm --->

<cfif j eq "OpenOrderItems">

	<cfif isDefined("url.UD")>
		<cfquery datasource="#attributes.datasource#" name="getLastDate">
			select attributevalue from accountattributes where accountID=#url.ID# and attributename='#j#UD'
		</cfquery>
	</cfif>

	<cfset queryName = "getOpenOrderItems">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
   		SELECT InvoiceNumber + '^' + 
			CONVERT(varchar, LineNumber) + '^' +
			CONVERT(varchar, AccountID) + '^' + 
			CONVERT(varchar, ItemID) + '^' + 
			CONVERT(varchar, QtyOrdered) + '^' + 
			CONVERT(varchar, CaseFlag) + '^' + 
			CONVERT(varchar(12), ExtPrice)
		AS DataString 
		FROM OpenOrders
		WHERE DistRepID=#url.id#
		<cfif isDefined("getLastDate") and getLastDate.recordcount neq 0>
			and UpdateDate > '#getLastDate.attributevalue#'
		</cfif>
	</cfquery>

<cfset queryColumnList = "InvoiceNumber,LineNumber,AccountID,ItemID,QtyOrdered,CaseFlag,ExtPrice">
</cfif>