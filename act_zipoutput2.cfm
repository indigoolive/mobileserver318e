<cfparam name="compressedoutput" default="">

<cfset TempDir = "P:\RENEW_TMP\">
<cfset tempFileName = GetTempFile(#TempDir#, "gz_")>

<cffile action="WRITE" file="#tempFileName#" output="#compressedOutput#" addnewline="no">

<cftry>
	<cfinvoke component = "Zip" method = "gzipAddFile" gzipFilename = "#tempFileName#.gz" inputFilename = "#tempFileName#">

	<cffile action="delete" file="#tempFileName#">

        <cfcontent deletefile="yes" file="#tempFileName#.gz" type="text/plain">
	
<cfcatch type="any">
	<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject="GZ Download Error">
	Error 6600: General Error (cfinvoke or cfcontent Failed)
	Template: act_zipoutput2.cfm
	AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
	Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
	Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
	Type: #cfcatch.type#
	Message: #cfcatch.message#
	SQL: <cfif isdefined("cfcatch.sql")>#cfcatch.sql#<cfelse>N/A</cfif>
	Details: #cfcatch.detail#

	<CFLOOP index=i from=1 to = #ArrayLen(CFCATCH.TAGCONTEXT)#>
		<CFSET sCurrent = #CFCATCH.TAGCONTEXT[i]#>
		<BR>#i# #sCurrent["ID"]# (#sCurrent["LINE"]#,#sCurrent["COLUMN"]#) #sCurrent["TEMPLATE"]#
	</CFLOOP>

	<cfif isDefined("form.fieldnames")>
		<cfloop list="#form.fieldnames#" index="i">
		#i#: #evaluate("form.#i#")#
		</cfloop>
	</cfif>
	
	<cfif isDefined("tempFileName")>
		tempFileName is #tempFileName#
	</cfif>
	</cfmail>
</cfcatch>
</cftry> 	



