<cfsetting enablecfoutputonly="no">
<!---
KEY
M{n}=MessageID^ToId^ToStatus^FromID^FromStatus^Subject^Message
T={count}
--->

<cfif isDefined("form.fieldnames")>
<cftry>
	<cfif NOT isdefined("form.t")>
		<cfset compressedOutput = "<ERROR>ERROR 1001: Transaction incomplete please try again.</ERROR>">
	<cfelse>
		<cfloop from="1" to="#form.t#" index="i">
			<cfset line = Evaluate("form.m#i#")>
			<cfset marray = ListToArray(line,"^")>
			<cfset MessageID = marray[1]>
			<cfif MessageID EQ 0>
				<!--- new message --->
				<cfset Subject = left(marray[6],55)>
				<cfset Subject = Replace(Subject, "~|A", "&", "ALL")>
				<cfset Subject = Replace(Subject, "~|C", "^", "ALL")>
				<cfset Subject = Replace(Subject, "'", "''", "ALL")>
				<cfset Message = left(marray[7],255)>
				<cfset Message = Replace(Message, "~|A", "&", "ALL")>
				<cfset Message = Replace(Message, "~|C", "^", "ALL")>
				<cfset Message = Replace(Message, "'", "''", "ALL")>

				<cfquery name="addmessage" datasource="#attributes.datasource#">
					Insert into Messages (ToID, FromID, Subject, Message, EntryDate)
					  values ( #marray[2]#, #url.id#, '#Subject#', '#Message#', GetDate())
				</cfquery>
			<cfelse>
				<cfquery name="updatemessage" datasource="#attributes.datasource#">
					Update Messages set ToStatus = #marray[3]# where MessageID = #MessageID# and ToId = #url.id#
				</cfquery>
				<cfquery name="updatemessage" datasource="#attributes.datasource#">
					Update Messages set FromStatus = #marray[5]# where MessageID = #MessageID# and FromId = #url.id#
				</cfquery>
			</cfif>
		</cfloop>
		<cfset compressedOutput = "SUCCESS              ">
	</cfif>
	
<cfcatch type="any">
	<cfset compressedOutput = "<ERROR>Error 5000: General Error, Support has been notified.</ERROR>">
	
	<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject="Error 5000 General Error">
		Error 5000: General Error
		Template: updateMessages.cfm
		AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
		Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
		Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
	</cfmail>
</cfcatch>

</cftry>
</cfif>

<cfinclude template="act_zipoutput2.cfm">