<cfif j eq "Filters">
	<cfset queryName = "Filters">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT 
			CONVERT(varchar, FilterID) + '^' +
			CONVERT(varchar, Filter)  + '^' +
			Description
		AS DataString 
		FROM Filters
		WHERE (DistRepID is NULL or DistRepID = #url.id#)
		ORDER by Filter 
	</cfquery>
	<cfset queryColumnList = "FilterID,Filter,Description">
</cfif>
