<cfif j eq "ItemAttributes">
	<cfset ClientDesignator = "C0">
	<cfset queryName = "ItemAttributes">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar, ItemID) 
			+ '^' + CONVERT(varchar(50), RIGHT(AttributeName, len(AttributeName) - len('#ClientDesignator#')))
			+ '^' + CONVERT(varchar(50), AttributeValue)  AS DataString
		FROM ItemAttributes
		WHERE AttributeName like '#ClientDesignator#%'
	</cfquery>
<cfset queryColumnList = "ItemID,AttributeName,AttributeValue">
</cfif>