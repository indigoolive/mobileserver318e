
<cfif j eq "Promo">
	<cfset queryName = "Prmom">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT  CONVERT(varchar(8), PromoID) 
		    + '^' + CONVERT(varchar(8), LineID) 
			+ '^' + CONVERT(varchar(8), ColCount) 
		    + '^' + ISNULL(description, '') 
		    + '^' + ISNULL(Data1, '')
			+ '^' + ISNULL(Data2, '') 
			+ '^' + ISNULL(Data3, '') 
			+ '^' + ISNULL(Data4, '') 
			+ '^' + ISNULL(Data5, '') 
			+ '^' + ISNULL(Data6, '') 
			+ '^' + ISNULL(Data7, '') 
			+ '^' + ISNULL(Data8, '') 
			+ '^' + ISNULL(Data9, '') 
			+ '^' + ISNULL(Data10, '') 
			+ '^' + ISNULL(Data11, '') 
			+ '^' + ISNULL(Data12, '') 
			+ '^' + ISNULL(convert(varchar,Width1), '0') 
			+ '^' + ISNULL(convert(varchar,Width2), '0') 
			+ '^' + ISNULL(convert(varchar,Width3), '0') 
			+ '^' + ISNULL(convert(varchar,Width4), '0') 
			+ '^' + ISNULL(convert(varchar,Width5), '0') 
			+ '^' + ISNULL(convert(varchar,Width6), '0') 
			+ '^' + ISNULL(convert(varchar,Width7), '0') 
			+ '^' + ISNULL(convert(varchar,Width8), '0') 
			+ '^' + ISNULL(convert(varchar,Width9), '0') 
			+ '^' + ISNULL(convert(varchar,Width10), '0') 
			+ '^' + ISNULL(convert(varchar,Width11), '0') 
			+ '^' + ISNULL(convert(varchar,Width12), '0') 
			+ '^' + ISNULL(convert(varchar,Orientation1), '0') 			
			+ '^' + ISNULL(convert(varchar,Orientation2), '0') 			
			+ '^' + ISNULL(convert(varchar,Orientation3), '0') 			
			+ '^' + ISNULL(convert(varchar,Orientation4), '0') 			
			+ '^' + ISNULL(convert(varchar,Orientation5), '0') 			
			+ '^' + ISNULL(convert(varchar,Orientation6), '0') 			
			+ '^' + ISNULL(convert(varchar,Orientation7), '0') 			
			+ '^' + ISNULL(convert(varchar,Orientation8), '0') 			
			+ '^' + ISNULL(convert(varchar,Orientation9), '0') 			
			+ '^' + ISNULL(convert(varchar,Orientation10), '0') 			
			+ '^' + ISNULL(convert(varchar,Orientation11), '0') 			
			+ '^' + ISNULL(convert(varchar,Orientation12), '0') As DataString
		FROM Promo
		ORDER BY PromoID, LineID
	</cfquery>
	<cfset queryColumnList = "PromoID,LineID,ColCount,Description,Data1,Data2,Data3,Data4,Data5,Data6,Data7,Data8,Data9,Data10,Data11,Data12,Width1,Width2,Width3,Width4,Width5,Width6,Width7,Width8,Width9,Width10,Width11,Width12,Orientation1,Orientation2,Orientation3,Orientation4,Orientation5,Orientation6,Orientation7,Orientation8,Orientation9,Orientation10,Orientation11,Orientation12">
</cfif>

