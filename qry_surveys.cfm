<cfif j eq "Surveys">
	<cfset queryName = "Surveys">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar(8), SurveyID) 
			+ '^' + CONVERT(varchar(32), Name) 
			+ '^' + CONVERT(varchar(255), Description) 
			+ '^' + (isNull(CONVERT(varchar(1), Control), 0))
			+ '^' + CONVERT(varchar(4), QuestionCount)
			+ '^' + Convert(varchar, SurveyType)
		As DataString
		FROM Survey
		WHERE (DistRepID = #url.id# or DistRepID is NULL)
	</cfquery>
	<cfset queryColumnList = "SurveyID,Name,Description,Control,QuestionCount,SurveyType">
</cfif>