<cfif j eq "FormsFilled">
	<cfset queryName = "FormsFilled">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar, FormsFilled.FormID)
			+ '^' + CONVERT(varchar, FormsFilled.AccountID)
			+ '^' + CONVERT(varchar, FormsFilled.QuestionID)
			+ '^0'
 			+ '^' + IsNull(FormsFilled.Answer, '')
		AS DataString 
		FROM FormsFilled, DistributorAccountReps, Accounts
		WHERE FormsFilled.AccountID = DistributorAccountReps.AccountID
			AND FormsFilled.AccountID = Accounts.AccountID
			AND DistributorAccountReps.DistRepID = #url.id#
			AND (Accounts.DeletedFlag is NULL OR Accounts.DeletedFlag = 0)
	</cfquery>
	<cfset queryColumnList = "FormID,AccountID,QuestionID,Updated,Answer">

</cfif>
	