<cfif j eq "FormQuestions">
	<cfset queryName = "FormQuestions">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar, FormID)
			+ '^' + CONVERT(varchar, QuestionID)
 			+ '^' + Question
		AS DataString 
		FROM FormQuestions
	</cfquery>
	<cfset queryColumnList = "FormID,QuestionID,Question">

</cfif>
	