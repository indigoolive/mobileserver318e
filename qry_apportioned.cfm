<cfif j eq "ApportionedItems">

<cfquery datasource="#attributes.datasource#" name="getDiv">
select DivisionList
from AccountPermissions
where accountID = #url.id#
</cfquery>

	<cfset queryName = "ApportionedItems">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar(8), AccountID) 
		    + '^' + CONVERT(varchar(8), DistributorItems.itemID) 
		    + '^' + CONVERT(varchar(5), ApportionedQuantity)  AS DataString
		FROM ApportionedItems,distributoritems
		where accountID in (#valuelist(check_login.accountID)#)
		and apportionedItems.itemID = distributorItems.itemID
		<cfif (getDiv.recordcount neq 0) and (trim(getdiv.DivisionList) neq "")>
		and DistributorItems.DivisionID in (#getDiv.DivisionList#)
		</cfif>
	</cfquery>
<cfset queryColumnList = "accountID,itemID,ApportionedQuantity">
</cfif>
