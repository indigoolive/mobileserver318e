<cfmodule template="/Common/other/logger.cfm" accountID="#url.ID#" accounttype="#getDatasource.accounttype#" moduletype="4">
<cfsetting enablecfoutputonly="yes">

<cfquery datasource="#attributes.datasource#" name="check_login">
select DistributorAccountReps.accountID,
       DistributorAccountReps.DistributorID
  from Accounts,
       DistributorAccountReps
 where Accounts.accountID=#url.ID#
<cfif url.p NEQ masterpass>
   and Accounts.password='#url.p#'
</cfif>
   and Accounts.LoginEnabled = 'Y'
   and Accounts.AccountID *= DistributorAccountReps.DistRepID
</cfquery>
<cfif check_login.recordcount eq 0>
	<cfset compressedoutput = 0>
</cfif>
<cfset compressedoutput = "">
<cftry>
	<cfquery datasource="#attributes.datasource#" name="getMaxID">
	select max(IssueID)+1 as MaxID
	from Issues
	</cfquery>
	<cfif form.wantresponse eq "True">
		<cfset want = 1>
	<cfelse>
		<cfset want = 0>
	</cfif>
	<cfquery datasource="#attributes.datasource#" name="insertIssue">
		insert into Issues(ShortDesc, LongDesc, AccountID, WantResponse)
		values('#form.ShortDesc#', '#form.LongDesc#', #form.AccountID#, #want#)
	</cfquery>

	<cfquery datasource="#attributes.datasource#" name="getName">
		select contactname
		from accounts
		where accountID = #form.AccountID#
	</cfquery>

	<cfquery datasource="#attributes.datasource#" name="GetDist">
		select Companyname
		from Distributors
		where DistributorID = #check_login.DistributorID#
	</cfquery>

	<cfmail to="support@indigoolive.com" subject="Rep Issue" from="support@beverageone.com">
		From Account: <cfif getName.recordcount eq 1>#getName.contactname# </cfif>(#form.AccountID#)
		Distributor: #GetDist.Companyname# (#check_login.DistributorID#)
		Wants Reponse: <cfif want>yes<cfelse>no</cfif>
		Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
			
		Issue: #form.ShortDesc#
			
		Description: #form.LongDesc#
	</cfmail>
	
	<cfset compressedoutput = 1>
 	
	<cfcatch type="any">
		<cfset compressedoutput = 0>
	</cfcatch>
</cftry>

<cfinclude template="act_zipoutput2.cfm">