<cfmodule template="/Common/other/logger.cfm" accountID="#url.ID#" accounttype="#getDatasource.accounttype#" moduletype="9">
<cfsetting enablecfoutputonly="no">
<!---
KEY
A=ACCOUNTID
B=WORKORDERID
N=TOTALITEMS

C{n}=ITEMID
D{n}=CASEQTY
E{n}=SINGLEQTY
F{n}=CDISTPRICE
G{n}=SDISTPRICE
H{n}=LASTCASE
I{n}=LASTSINGLE
J{n}=CATALOG
K{n}=LINEITEMNUMBER
L{n}=SPECIAL CODE
M{n}=DISCOUNTAMOUNT
N{n}=PAR
O{n}=FILTER
P{n}=INV
Q{n}=unused
R{n}=RETAILPRICE
--->

<cfif isDefined("form.fieldnames")>
	<cfquery datasource="#attributes.datasource#" name="preserveLast">
		select itemID,CLastQty, SLastQty
		from workorderitems
		where accountID = #form.A#
		and workorderID in  (select workorderID from workorder where accountID=#form.A# and distrepID=#url.ID#)
	</cfquery>

	<cfscript>
		LastStruct = structNew();
		for(j=1; j lte preserveLast.recordcount; j=j+1)
			{
			if(not structKeyExists(LastStruct, "#preserveLast.itemID[j]#C"))
				{
				if(preserveLast.CLastQty[j] eq "")
					structInsert(LastStruct, "#preserveLast.itemID[j]#C", 0);
				else
					structInsert(LastStruct, "#preserveLast.itemID[j]#C", preserveLast.CLastQty[j]);
				if(preserveLast.SLastQty[j] eq "")
					structInsert(LastStruct, "#preserveLast.itemID[j]#S", 0);
				else
					structInsert(LastStruct, "#preserveLast.itemID[j]#S", preserveLast.SLastQty[j]);
				}
			}
		masterOrderStruct = structNew();
		masterOrderItemsStruct = structNew();
		structInsert(masterOrderStruct, "accountID", form.A);
		structInsert(masterOrderStruct, "workorderID", form.B);
		for(i=1; i lte form.N; i=i+1)
			{
			"itemStruct#i#" = StructNew();
			StructInsert(evaluate("itemStruct#i#"), "itemID", evaluate("form.C#i#"));
			StructInsert(evaluate("itemStruct#i#"), "CaseQTY", evaluate("form.D#i#"));
			StructInsert(evaluate("itemStruct#i#"), "SingleQTY", evaluate("form.E#i#"));
			StructInsert(evaluate("itemStruct#i#"), "CDistPrice", evaluate("form.F#i#"));
			StructInsert(evaluate("itemStruct#i#"), "SDistPrice", evaluate("form.G#i#"));
			StructInsert(evaluate("itemStruct#i#"), "lastCaseQTY", evaluate("form.H#i#"));
			StructInsert(evaluate("itemStruct#i#"), "lastSingleQTY", evaluate("form.I#i#"));
			StructInsert(evaluate("itemStruct#i#"), "catalogID", evaluate("form.J#i#"));
			StructInsert(evaluate("itemStruct#i#"), "priceoverride", evaluate("form.K#i#"));
			StructInsert(evaluate("itemStruct#i#"), "LineItemNumber", i);
			StructInsert(evaluate("itemStruct#i#"), "Discount", evaluate("form.M#i#"));
			StructInsert(evaluate("itemStruct#i#"), "Special", evaluate("form.L#i#"));
			StructInsert(evaluate("itemStruct#i#"), "Par", evaluate("form.N#i#"));
			StructInsert(evaluate("itemStruct#i#"), "Filter", evaluate("form.O#i#"));
			StructInsert(evaluate("itemStruct#i#"), "Inv", evaluate("form.P#i#"));
            		StructInsert(evaluate("itemStruct#i#"), "RetailPrice", evaluate("form.R#i#"));
			StructInsert(masterOrderItemsStruct, "itemID#i#", evaluate("itemStruct#i#"));
			}
	</cfscript>

	<cfif Form.N GT 0>

	<cfquery datasource="#attributes.datasource#" name="getCats">
		select catalogID
		from accountcatalogs
		where accountID = #masterOrderStruct["accountID"]#
	</cfquery>

	<cfset validCatalogs = valueList(getCats.catalogID)>

	<cftransaction action="BEGIN">
 	<cftry>
		<cfset commit = 1>
		<cfquery datasource="#attributes.datasource#" name="deleteItems">
			delete from workorderitemAttributes
			where workorderID = #masterOrderStruct["workorderID"]#
			and accountID = #masterOrderStruct["accountID"]#
		</cfquery>
		<cfquery datasource="#attributes.datasource#" name="deleteItems">
			delete from workorderitems
			where workorderID = #masterOrderStruct["workorderID"]#
			and accountID = #masterOrderStruct["accountID"]#
		</cfquery>
		<cfset line = 1>
		<cfloop collection="#masterOrderItemsStruct#" item="i">
			<cfif ListLen(Validcatalogs) EQ 0>
				<cfset catID = masterOrderItemsStruct[i].catalogID>
			<cfelseif Not ListFind(validcatalogs, masterOrderItemsStruct[i].catalogID )>
				<cfset catID = listfirst(validcatalogs)>
			<cfelse>
				<cfset catID = masterOrderItemsStruct[i].catalogID>
			</cfif>

			<cfif (masterOrderItemsStruct[i].CDistPrice eq 0) and (masterOrderItemsStruct[i].SDistPrice eq 0)>
				<cfquery datasource="#attributes.datasource#" name="getPrice">
					select *
					from pricingscheme
					where itemID = #masterOrderItemsStruct[i].itemID#
					and catalogID = #catID#
				</cfquery>
	
				<cfif getPrice.recordcount neq 0>
					<cfset masterOrderItemsStruct[i].CDistPrice = getPrice.caseprice>
					<cfset masterOrderItemsStruct[i].SDistPrice = getPrice.singleprice>
				</cfif>
			</cfif>
				
			<cfquery datasource="#attributes.datasource#" name="insertItem">
				insert into workorderitems(workorderid, accountid, LineItemNumber, catalogid, itemid, SQty, CQty, SPrice, CPrice, Par1, Filter, Inv, SlastQTY, CLastQTY, RetailPrice)
					values(#masterOrderStruct["workorderID"]#, #masterOrderStruct["accountID"]#,
						#masterOrderItemsStruct[i].LineItemNumber#, #catID#, #masterOrderItemsStruct[i].itemID#,
						#masterOrderItemsStruct[i].singleQTY#, #masterOrderItemsStruct[i].caseQTY#,
						#masterOrderItemsStruct[i].SDistPrice#, #masterOrderItemsStruct[i].CDistPrice#,
						#masterOrderItemsStruct[i].Par#, #masterOrderItemsStruct[i].Filter#, #masterOrderItemsStruct[i].Inv#,
					<cfif structKeyExists(LastStruct, "#masterOrderItemsStruct[i].itemID#S")>
						#LastStruct["#masterOrderItemsStruct[i].itemID#S"]#
					<cfelse>
						#masterOrderItemsStruct[i].lastSingleQTY#
					</cfif>,
					<cfif structKeyExists(LastStruct, "#masterOrderItemsStruct[i].itemID#C")>
						#LastStruct["#masterOrderItemsStruct[i].itemID#C"]#
					<cfelse>
						#masterOrderItemsStruct[i].lastCaseQTY#
					</cfif>,
						#masterOrderItemsStruct[i].RetailPrice#)
			</cfquery>

			<cfif (options.ShowDiscount) and (masterOrderItemsStruct[i].discount neq "" and masterOrderItemsStruct[i].discount neq 0)>
				<cfquery name="insertWOAttribute" datasource="#attributes.datasource#">
					insert into WorkOrderItemAttributes( WorkOrderID, AccountID, LineItemNumber, AttributeName, AttributeValue)
					values (#masterOrderStruct["workorderID"]#, #masterOrderStruct["accountID"]#, #masterOrderItemsStruct[i].LineItemNumber#,
						'DiscountAmount', '#replace(masterOrderItemsStruct[i].discount, "'", "''", "ALL")#')
				</cfquery>
			</cfif>

			<cfif (options.PriceOverride) and (masterOrderItemsStruct[i].priceoverride neq "" and masterOrderItemsStruct[i].priceoverride neq 0)>
				<cfquery name="insertWOAttribute" datasource="#attributes.datasource#">
					insert into WorkOrderItemAttributes(WorkOrderID, AccountID, LineItemNumber, AttributeName, AttributeValue)
					values (#masterOrderStruct["workorderID"]#, #masterOrderStruct["accountID"]#, #masterOrderItemsStruct[i].LineItemNumber#,
						'PriceOverride', '#replace(masterOrderItemsStruct[i].priceoverride, "'", "''", "ALL")#')
				</cfquery>
			</cfif>

			<cfif (options.ShowSpecialFlag) and (masterOrderItemsStruct[i].special neq "" and masterOrderItemsStruct[i].special neq 0)>
				<cfquery name="insertWOAttribute" datasource="#attributes.datasource#">
					insert into WorkOrderItemAttributes(WorkOrderID, AccountID, LineItemNumber, AttributeName, AttributeValue)
					values (#masterOrderStruct["workorderID"]#, #masterOrderStruct["accountID"]#, #masterOrderItemsStruct[i].LineItemNumber#,
						'SpecialFlag', '#replace(masterOrderItemsStruct[i].special, "'", "''", "ALL")#')
				</cfquery>
			</cfif>

			<cfset line = line + 1>

		</cfloop>

		<cfset compressedOutput = "SUCCESS              ">

	<cfcatch type="any">
		<cftransaction action="ROLLBACK">
		<cfset commit = 0>

		<cfset compressedOutput = "<ERROR>ERROR 2000: Error adding data to BeverageOne database.</ERROR>">
		
		<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject="Error adding data to BeverageOne database">
			Error 2000: Error adding data to BeverageOne database
			Template: updateWO.cfm
			AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
			Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
			Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
			Type: #cfcatch.type#
			Message: #cfcatch.message#
			Details: #cfcatch.detail#
			
			SQL: <cfif isdefined("cfcatch.Sql")>#cfcatch.Sql#</cfif>
			
			<CFLOOP index=i from=1 to = #ArrayLen(CFCATCH.TAGCONTEXT)#>
				<CFSET sCurrent = #CFCATCH.TAGCONTEXT[i]#>
				<BR>#i# #sCurrent["ID"]# (#sCurrent["LINE"]#,#sCurrent["COLUMN"]#) #sCurrent["TEMPLATE"]#
			</CFLOOP>
			
			<cfif isDefined("form.fieldnames")>
				<cfloop list="#form.fieldnames#" index="i">
					#i#: #evaluate("form.#i#")#
				</cfloop>
			</cfif>
		</cfmail>
	</cfcatch>

	</cftry>

	<cfif commit>
		<cftransaction action="COMMIT">
	</cfif>
	</cftransaction>

	<cfelse>
		<cfset compressedOutput = "SUCCESS              ">
	</cfif> <!--- form.N > 0 --->

<cfelse>
	<cfset compressedOutput = "<ERROR>Error 3000: Missing Data</ERROR>">
	
	<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject="Error 3000 Missing Data">
		Error 3000:missing data
		Template: updateWO.cfm
		AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
		Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
		Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
	</cfmail>
	
</cfif>

<cfinclude template="act_zipoutput2.cfm">