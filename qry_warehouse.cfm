<cfif j eq "Warehouse">
	<cfquery datasource="#attributes.datasource#" name="getWarehouses">
	select LocationList
	from AccountPermissions
	where accountID = #url.id#
	</cfquery>
	<cfset queryName = "Warehouses">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar, Warehouse) 
		    + '^' + rtrim(Description)
			+ '^' + '1'
		FROM Warehouse
		<cfif getWarehouses.LocationList neq "">
		where Warehouse in (#getWarehouses.LocationList#)
		</cfif>
		UNION
		SELECT CONVERT(varchar, Warehouse) 
		    + '^' + rtrim(Description)
			+ '^' + '0'
		FROM Warehouse
		<cfif getWarehouses.LocationList neq "">
		where Warehouse not in (#getWarehouses.LocationList#)
		</cfif>
		ORDER BY 1
	</cfquery>
<cfset queryColumnList = "WarehouseID,Description,EnableFlag">
</cfif>
