<!---

I believe this to be dead code and so have commented it OUT; WWP; 11 JUN 2016



<cfmodule template="/Common/other/logger.cfm" accountID="#url.ID#" accounttype="#getDatasource.accounttype#" moduletype="8">
<cfsetting enablecfoutputonly="no">
<!---
KEY
A=AccountID
B=SurveyDate
C=Flags
D=Share
E=Score
F=BudCase1
G=BudCase2
H=BudCase3
I=BudDisp1
J=BudDisp2
K=BudDisp3
L=MillerCase1
M=MillerCase2
N=MillerCase3
O=MillerDisp1
P=MillerDisp2
Q=MillerDisp3
R=OtherCase1
S=OtherCase2
T=OtherCase3
U=OtherDisp1
V=OtherDisp2
W=OtherDisp3
X=BudPricing1
Y=BudPricing2
Z=BudPricing3
AA=MillerPricing1
AB=MillerPricing2
AC=MillerPricing3
AD=OtherPricing1
AE=OtherPricing2
AF=OtherPricing3
AG=Comment1
AH=Comment2
AI=Comment3
AJ=Priority1
AK=Priority2
AL=Priority3
AM=Dist1
AN=Dist2
AO=Dist3
AP=Place1
AQ=Place2
AR=Place3
AS=Perm1
AT=Perm2
AU=Perm3
AV=POS1
AW=POS2
AX=POS3
--->



<cfif isDefined("form.fieldnames")>
	<cfscript>
		SurveyStruct = structNew();
		structInsert(SurveyStruct, "accountID", form.A);
		structInsert(SurveyStruct, "SurveyDate", form.B);
		structInsert(SurveyStruct, "Flags", form.C);
		structInsert(SurveyStruct, "Share", form.D);
		structInsert(SurveyStruct, "Score", form.E);
		structInsert(SurveyStruct, "BudCase1", form.F);
		structInsert(SurveyStruct, "BudCase2", form.G);
		structInsert(SurveyStruct, "BudCase3", form.H);
		structInsert(SurveyStruct, "BudDisp1", form.I);
		structInsert(SurveyStruct, "BudDisp2", form.J);
		structInsert(SurveyStruct, "BudDisp3", form.K);
		structInsert(SurveyStruct, "MillerCase1", form.L);
		structInsert(SurveyStruct, "MillerCase2", form.M);
		structInsert(SurveyStruct, "MillerCase3", form.N);
		structInsert(SurveyStruct, "MillerDisp1", form.O);
		structInsert(SurveyStruct, "MillerDisp2", form.P);
		structInsert(SurveyStruct, "MillerDisp3", form.Q);
		structInsert(SurveyStruct, "OtherCase1", form.R);
		structInsert(SurveyStruct, "OtherCase2", form.S);
		structInsert(SurveyStruct, "OtherCase3", form.T);
		structInsert(SurveyStruct, "OtherDisp1", form.U);
		structInsert(SurveyStruct, "OtherDisp2", form.V);
		structInsert(SurveyStruct, "OtherDisp3", form.W);
		structInsert(SurveyStruct, "BudPricing1", form.X);
		structInsert(SurveyStruct, "BudPricing2", form.Y);
		structInsert(SurveyStruct, "BudPricing3", form.Z);
		structInsert(SurveyStruct, "MillerPricing1", form.AA);
		structInsert(SurveyStruct, "MillerPricing2", form.AB);
		structInsert(SurveyStruct, "MillerPricing3", form.AC);
		structInsert(SurveyStruct, "OtherPricing1", form.AD);
		structInsert(SurveyStruct, "OtherPricing2", form.AE);
		structInsert(SurveyStruct, "OtherPricing3", form.AF);
		structInsert(SurveyStruct, "Comment1", form.AG);
		structInsert(SurveyStruct, "Comment2", form.AH);
		structInsert(SurveyStruct, "Comment3", form.AI);
		structInsert(SurveyStruct, "Priority1", form.AJ);
		structInsert(SurveyStruct, "Priority2", form.AK);
		structInsert(SurveyStruct, "Priority3", form.AL);
		structInsert(SurveyStruct, "Dist1", form.AM);
		structInsert(SurveyStruct, "Dist2", form.AN);
		structInsert(SurveyStruct, "Dist3", form.AO);
		structInsert(SurveyStruct, "Place1", form.AP);
		structInsert(SurveyStruct, "Place2", form.AQ);
		structInsert(SurveyStruct, "Place3", form.AR);
		structInsert(SurveyStruct, "Perm1", form.AS);
		structInsert(SurveyStruct, "Perm2", form.AT);
		structInsert(SurveyStruct, "Perm3", form.AU);
		structInsert(SurveyStruct, "POS1", form.AV);
		structInsert(SurveyStruct, "POS2", form.AW);
		structInsert(SurveyStruct, "POS3", form.AX);
	</cfscript>
	
	<cftransaction action="BEGIN">
 	<cftry>
		<cfset commit = 1>
		<cfset line = 1>
		<cfset compressedOutput = "SUCCESS              ">
  	<cfcatch type="any">
		<cftransaction action="ROLLBACK">
		<cfset commit = 0>

		<cfset compressedOutput = "<ERROR>ERROR 2000: Error adding data to BeverageOne database.</ERROR>">
		
		<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject="Error adding data to BeverageOne database">
			Error 2000: Error adding data to BeverageOne database
			Template: updateCS.cfm
			AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
			Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
			Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
			Type: #cfcatch.type#
			Message: #cfcatch.message#
			Details: #cfcatch.detail#
		</cfmail>
	</cfcatch>
	</cftry>
	
	<cfif commit>
		<cftransaction action="COMMIT">
	</cfif>
	</cftransaction>

<cfelse>
		<cfset compressedOutput = "SUCCESS              ">

		<cfset compressedOutput = "<ERROR>Error 3000: Missing Data</ERROR>">
	 	<cfmail to="errorreport@beverageone.com" from="errorreport@beverageone.com" subject="Error 3000 Missing Data">
error 3000:missing data
Template: updateWO.cfm
AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
		</cfmail>
		
</cfif>

<cfinclude template="act_zipoutput.cfm">

--->