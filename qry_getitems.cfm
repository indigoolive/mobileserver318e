<cfif j eq "Items">
	<cfquery datasource="#attributes.datasource#" name="getDiv">
		select DivisionList
		from AccountPermissions
		where accountID = #url.id#
	</cfquery>

	<cfquery name="GetCatalog" datasource="#attributes.datasource#">
		SELECT Distinct CatalogID
		FROM AccountCatalogs
		WHERE AccountID in
			(SELECT AccountID
				FROM DistributorAccountReps
				WHERE DistRepID = #URL.ID#
				AND Distributorid = #GetDistributor.DistributorID#)
	</cfquery>

	<cfquery name="GetHistoryDays" datasource="#attributes.datasource#">
		SELECT OrderHistoryDays
		FROM DistributorOptions
		where DistributorID = #getDistributor.distributorID#
	</cfquery>

	
	<cfset queryName = "GetItems">
	
	<cfquery datasource="#attributes.datasource#" name="selectWarehouse">
		select LocationList 
		from AccountPermissions 
		where accountID = #url.ID# 
		and ( LocationList is not NULL or LocationList <> '' )
	</cfquery>

	<cfif selectWarehouse.recordcount eq 0>
		<cfset warehouses = 1>
		<cfset warehousecount = 1>
	<cfelse>
		<cfset warehouses = ValueList(selectWarehouse.locationlist)>
		<cfif warehouses EQ "">
			<cfset warehouses = 1>
			<cfset warehousecount = 1>
		<cfelse>
			<cfset warehousecount = ListLen(warehouses)>
		</cfif>
	</cfif>

	<cfquery datasource="#attributes.datasource#" name="GetAllWhse">
		select Warehouse from warehouse
		where warehouse in (#warehouses#)
	</cfquery>

	<cfquery datasource="#attributes.datasource#" name="#queryName#">
	SELECT distinct 
		CONVERT(varchar(255), di.ItemID) 
		+ '^' + ISNULL(di.Description, '') 
		+ '^' + CONVERT(varchar, ISNULL( (select sum(WareHouseItems.QtyOnHand) from warehouseitems where ItemID = di.ItemID and warehouse in (#warehouses#)), 0)) 
		+ '^' + CONVERT(varchar, ISNULL( (select sum(WareHouseItems.parlevel) from warehouseitems where ItemID = di.ItemID and warehouse in (#warehouses#)), 0))
		+ '^' + CONVERT(varchar, ISNULL(di.CategoryID, 0))
		+ '^' + CONVERT(varchar, di.DivisionID) 
		+ '^' + ISNULL(di.DistributorSKU, '') 
		+ '^' + ISNULL(di.ItemVolume ,'')
		+ '^' + CONVERT(varchar, di.QTYPerCase) 
		+ '^' + CONVERT(varchar, ISNULL ((Select top 1 ItemStatus1 from WarehouseItemStatus where ItemID = di.ItemID and warehouse in (#warehouses#)), 0))
		+ '^' + CONVERT(varchar, ISNULL(di.SizeCode, 0))
		+ '^' + ISNULL(di.UPC, '')
		+ '^' + CONVERT(varchar, ISNULL((select top 1 CaseOnly from CatalogItems where ItemID = di.ItemID and CatalogID in (#CatalogList#) ) , 0))
		+ '^' + ISNULL(Vintage , '')
		+ '^' + CONVERT(varchar, ISNULL( (select sum(WareHouseItems.QtyOnOrder) from warehouseitems where ItemID = di.ItemID and warehouse in (#warehouses#)), 0))
		+ '^' + ISNULL( CONVERT(varchar, (select max(WareHouseItems.OnOrderExpDate) from warehouseitems where ItemID = di.ItemID), 101), '')
		+ '^' + CONVERT(varchar, ISNULL( (select sum(WareHouseItems.ReserveQty) from warehouseitems where ItemID = di.ItemID and warehouse in (#warehouses#)), 0))
		+ '^' + CONVERT(varchar, di.DeletedFlag)
		+ '^' + ISNULL(di.FamilyCode1, '')
		+ '^' + ISNULL(di.FamilyCode2, '')
		+ '^' + ISNULL(di.CatalogPage, '')
		+ '^' + CONVERT(varchar, ISNULL(di.Rating, -1))
		+ '^' + CONVERT(varchar, ISNULL(di.QtyCheck, 0))
		+ '^' + CONVERT(varchar, ISNULL(di.RetailPrice, 0))
		+ '^' + ISNULL(di.ExtraInfo, '')
		+ '^' + ISNULL(di.ExtraInfo2, '')
		+ '^' + CONVERT(varchar, ISNULL( (select Cost from ProductCost where ItemID = di.ItemID and EndDate = 65535), 0.00))
		+ '^' + ISNULL((select AttributeValue from ItemAttributes where ItemID = di.ItemID and AttributeName = 'LowInventory'), '0')
		+ '^' + CONVERT(varchar, ISNULL(di.ATM, 0))
		+ '^' + ISNULL(CONVERT(varchar, di.OnOrderOrderDate, 101), '')
		<cfset catcount = 1>
		+ '^' + 
		<cfloop list="#CatalogList#" index="i">
			CONVERT(varchar, ISNULL ((Select #i# where exists (select c#catcount#.CatalogID from CatalogItems c#catcount# where c#catcount#.CatalogID=#i# and c#catcount#.ItemID=di.ItemID)), -1) )
			<cfif catcount neq listlen(cataloglist)>
				+ ',' + 
			</cfif>
			<cfset catcount = catcount+1>
		</cfloop>

		<cfif options.HasPriceChange EQ 1>
			+ '^' + CONVERT(varchar, ISNULL( (select PriceChange from PriceChanges where CatalogID = #GetCatalog.CatalogID# and ItemID = di.ItemID), 0))
		<cfelse>
			+ '^0'  
		</cfif>

		<cfif warehousecount GT 1 or options.AcctSpecificWarehouses EQ 1>
			<cfloop query="getAllWhse">
				+ '^' + CONVERT(varchar, ISNULL( (select WareHouseItems.QtyOnHand from warehouseitems where ItemID = di.itemID and warehouse = #GetAllWhse.warehouse#), 0)) 
				+ '^' + CONVERT(varchar, ISNULL( (select WareHouseItems.QtyOnOrder from warehouseitems where ItemID = di.itemID and warehouse = #GetAllWhse.warehouse#), 0)) 
				+ '^' + CONVERT(varchar, ISNULL( (select WareHouseItems.ReserveQty from warehouseitems where ItemID = di.itemID and warehouse = #GetAllWhse.warehouse#), 0)) 
				+ '^' + CONVERT(varchar, ISNULL( (select WareHouseItems.ParLevel from warehouseitems where ItemID = di.itemID and warehouse = #GetAllWhse.warehouse#), 0)) 
				+ '^' + ISNULL(CONVERT(varchar, (select WareHouseItems.OnOrderExpDate from warehouseitems where ItemID = di.itemID and warehouse = #GetAllWhse.warehouse#), 101), '')
				+ '^' + CONVERT(varchar, ISNULL ((Select ItemStatus1 from WarehouseItemStatus where ItemID = di.ItemID and warehouse = #GetAllWhse.warehouse#), 0))
			</cfloop>	
		</cfif>
	AS DataString
	FROM DistributorItems di
	WHERE di.DistributorID = #getDistributor.distributorID#
	<cfif isDefined("url.ItemsUD") and url.ItemsUD NEQ "">
		and di.UpdateDate > #CreateODBCDateTime(url.ItemsUD)#
	</cfif>
	<cfif (getDiv.recordcount neq 0) and (trim(getdiv.DivisionList) neq "")>
		and di.DivisionID in (#getDiv.DivisionList#)
	</cfif>

	AND ((di.DeletedFlag = 0 
			AND Exists (SELECT ItemID FROM CatalogItems WHERE ItemID = di.ItemID AND CatalogID in (#CatalogList#)) 
			AND Exists (SELECT ItemID FROM WarehouseItems WHERE ItemID = di.ItemID AND Warehouse in (#warehouses#)) )
		OR (di.DeletedFlag = 1 
			AND (Exists (SELECT moi.ItemID
					FROM MasterOrderItems moi, MasterOrder mo
					WHERE mo.OrderNumber = moi.OrderNumber
					AND DateDiff(day, mo.ActivateDate, GetDate()) <= #GetHistoryDays.OrderHistoryDays#
					AND moi.ItemID = di.ItemID) 
			OR Exists (SELECT ItemID FROM OpenOrders WHERE ItemID = di.ItemID))
			)
		)

	</cfquery>

	<cfset warehouselist = "">
	<cfset queryColumnList = "ItemID,Description,QTYOnHand,parlevel,CategoryID,divisionID,distributorSKU,itemVolume,QTYPerCase,ItemStatus,SizeCode,UPC,caseonly,Vintage,OnOrderQty,OnOrderDate,ReserveQty,deletedflag,FamilyCode1,FamilyCode2,CatalogPage,Rating,QtyCheck,RetailPrice,ExtraInfo,ExtraInfo2,Cost,LowInventory,ATM,OnOrderOrderDate,ValidCatalogs,PriceChange">

	<cfif warehousecount GT 1 or options.AcctSpecificWarehouses EQ 1>
		<cfloop query="getAllWhse">
			<cfset queryColumnList = listappend(queryColumnList, "Whse#getAllWhse.warehouse#QtyOnHand")>
			<cfset queryColumnList = listappend(queryColumnList, "Whse#getAllWhse.warehouse#OnOrderQty")>
			<cfset queryColumnList = listappend(queryColumnList, "Whse#getAllWhse.warehouse#ReserveQty")>
			<cfset queryColumnList = listappend(queryColumnList, "Whse#getAllWhse.warehouse#ParLevel")>
			<cfset queryColumnList = listappend(queryColumnList, "Whse#getAllWhse.warehouse#OnOrderDate")>
			<cfset queryColumnList = listappend(queryColumnList, "Whse#getAllWhse.warehouse#ItemStatus")>
		</cfloop>
	</cfif>
</cfif>