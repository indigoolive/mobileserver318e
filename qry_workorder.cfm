<cfif j eq "WorkOrder">
	<cfset queryName = "WorkOrder">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar(8), WorkOrder.AccountID) 
		    + '^' + CONVERT(varchar(8), min(WorkOrderID)) 
		    + '^' + CONVERT(varchar(1), WOType) As DataString
		 FROM WorkOrder
		WHERE WOType = 1
		  and WorkOrder.Salesman = (select min(Salesman) from DistributorAccountReps dar where dar.DistRepID = #url.id# and dar.AccountID = WorkOrder.AccountID)
		  group by WorkOrder.Accountid, WOType
	</cfquery>
	<cfset queryColumnList = "accountID,workorderID,wotype">
</cfif>

<!---
	and dar.salesman = (select min(salesman) from distributoraccountreps dar2 where dar2.accountid = dar.accountid and dar2.distrepid = dar.distrepid)
--->
