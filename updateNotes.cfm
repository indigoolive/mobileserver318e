<cfmodule template="/Common/other/logger.cfm" accountID="#url.ID#" accounttype="#getDatasource.accounttype#" moduletype="3">
<cfsetting enablecfoutputonly="yes">

<cftry>
	<cfif form.updatedflag eq 2>
		<cfquery datasource="#attributes.datasource#" name="deleteNote">
		delete from DistributorRepNotes
		 where DistrepID = #url.ID#
		   and AccountID = #form.accountID#
		   and DateEntered = '#form.DateEntered#'
		   and comment = '#form.comment#'
		</cfquery>
	<cfelse>
		<cfquery datasource="#attributes.datasource#" name="FindNote">
		select * 
	   	  from DistributorRepNotes 
		 where DistrepID = #url.ID#
	 	   and AccountID = #form.accountID#
		   and DateEntered = '#form.DateEntered#'
		   and comment = '#form.comment#'
		</cfquery>
		<cfif FindNote.RecordCount GT 0>
			<cfquery datasource="#attributes.datasource#" name="UpdateNote">
			Update DistributorRepNotes set comment = '#form.comment#'
			 where DistrepID = #url.ID#
 	   		   and AccountID = #form.AccountID#
			   and DateEntered = '#form.DateEntered#'
			   and comment = '#form.Comment#'
			</cfquery>
		<cfelse>
			<cfquery datasource="#attributes.datasource#" name="insertNote">
				insert into DistributorRepNotes(DistrepID, AccountID, DateEntered, Comment)
				values(#url.ID#, #form.accountID#, '#form.DateEntered#', '#form.comment#')
			</cfquery>
		</cfif>
	</cfif>
	<cfcatch type="any">
	
		<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject="Error adding data to BeverageOne database">
			Error 2000: Error adding data to BeverageOne database
			Template: updateNotes.cfm
			AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
			Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
			Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
			Type: #cfcatch.type#
			Message: #cfcatch.message#
			Details: #cfcatch.detail#
			
			<CFLOOP index=i from=1 to = #ArrayLen(CFCATCH.TAGCONTEXT)#>
				<CFSET sCurrent = #CFCATCH.TAGCONTEXT[i]#>
				<BR>#i# #sCurrent["ID"]# (#sCurrent["LINE"]#,#sCurrent["COLUMN"]#) #sCurrent["TEMPLATE"]#
			</CFLOOP>
			
			<cfif isDefined("form.fieldnames")>
				<cfloop list="#form.fieldnames#" index="i">
					#i#: #evaluate("form.#i#")#
			</cfloop>
			</cfif>
		</cfmail>
		
		<cfset compressedOutput = 0>
	</cfcatch>
</cftry>

<cfif compressedOutput eq "">
	<cfset compressedOutput = 1>
</cfif>

<cfinclude template="act_zipoutput2.cfm">