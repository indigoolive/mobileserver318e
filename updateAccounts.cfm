<cfmodule template="/Common/other/logger.cfm" accountID="#url.ID#" accounttype="#getDatasource.accounttype#" moduletype="5">
<cfsetting enablecfoutputonly="yes">

 <cftry>
	<cfquery datasource="#attributes.datasource#" name="updateName">
	update accounts
	set 
	<cfif options.EditContactName>
	contactname = '#form.contactname#',
	</cfif>
	<cfif options.EditCompanyname>
	companyname = '#form.companyname#',
	</cfif>
	<cfif options.EditAltContactName>
	altcontactname = '#form.altcontactname#',
	</cfif>
	<cfif options.EditAddress>
	address1 = '#form.address1#',
	address2 = '#form.address2#',
	</cfif>
	<cfif options.EditCity>
	city = '#form.city#',
	</cfif>
	<cfif options.EditState>
	state = '#form.state#',
	</cfif>
	<cfif options.EditZip>
	zip = '#form.zip#',
	</cfif>
	<cfif options.EditPhone>
	phone = '#form.phone#',
	</cfif>
	<cfif options.EditFax>
	fax = '#form.fax#',
	</cfif>
	<cfif options.EditEmail>
	email = '#form.email#',
	</cfif>
	deliverydays = '#form.DeliveryDays#'
	where accountID = #form.AccountID#
	</cfquery>
	<cfquery name="UpdAppt" datasource="#attributes.datasource#">
		Update DistributorAccountReps
			set ApptDays = '#form.ApptDays#',
			ApptTime1 = '#form.ApptTime1#',
			ApptTime2 = '#form.ApptTime2#',
			ApptTime3 = '#form.ApptTime3#',
			ApptTime4 = '#form.ApptTime4#',
			ApptTime5 = '#form.ApptTime5#',
			ApptTime6 = '#form.ApptTime6#',
			ApptTime7 = '#form.ApptTime7#'
		where AccountID	= #form.AccountID#
		and DistRepID = #URL.ID#
	</cfquery>
	
	<cfset compressedOutput = 1>
 	
	<cfcatch type="any">
		<cfset compressedOutput = 0>
		
		<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject="Error adding data to BeverageOne database">
			Error 2000: Error adding data to BeverageOne database
			Template: updateAccounts.cfm
			AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
			Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
			Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
			Type: #cfcatch.type#
			Message: #cfcatch.message#
			Details: #cfcatch.detail#
		</cfmail>
	</cfcatch>
</cftry>

<cfinclude template="act_zipoutput2.cfm">
