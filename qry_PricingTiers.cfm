<cfif j EQ "PricingTiers">
	<cfset queryName = "PricingTiers">

	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar, TierID) +'^'+ isNull(Description, 'Unknown Tier')
		AS DataString 
		FROM PricingTiers
		WHERE DeletedFlag = 0
	</cfquery>

	<cfset queryColumnList = "TierID,Description">
</cfif>