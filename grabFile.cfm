
<cfset TempDir = "P:\RENEW_TMP\">
<cfset gzfile="#url.gz#">

<cfif (gzfile neq "")>
	<cftry>
		<cfset gzFileName = TempDir & gzfile>

		<cfset gzFileSize = createObject("java","java.io.File").init("#gzFileName#").length()>

		<cfheader name="content-length" value="#gzFileSize#">
		<cfcontent deletefile="yes" file="#gzFileName#" type="text/plain">

	<cfcatch type="any">
		<cfmail to="errorreport@indigoolive.com" from="errorreport@beverageone.com" subject="GZ Download Error">
			Error 6700: General Error (cfcontent Failed)
			Template: grabFile.cfm
			AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
			Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
			Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
			Type: #cfcatch.type#
			Message: #cfcatch.message#
			
			SQL: <cfif isdefined("cfcatch.sql")>#cfcatch.sql#<cfelse>N/A</cfif>
			
			Details: #cfcatch.detail#

			<CFLOOP index=i from=1 to = #ArrayLen(CFCATCH.TAGCONTEXT)#>
				<CFSET sCurrent = #CFCATCH.TAGCONTEXT[i]#>
				<BR>#i# #sCurrent["ID"]# (#sCurrent["LINE"]#,#sCurrent["COLUMN"]#) #sCurrent["TEMPLATE"]#
			</CFLOOP>

			<cfif isDefined("form.fieldnames")>
				<cfloop list="#form.fieldnames#" index="i">
				#i#: #evaluate("form.#i#")#
				</cfloop>
			</cfif>
	
		</cfmail>
	</cfcatch>
	</cftry> 	

	<cfif fileExists("#gzFileName#")>
		<cffile action="delete" file="#gzFileName#">
	</cfif>

</cfif>

