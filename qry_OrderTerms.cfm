<cfif j eq "OrderTerms">
	<cfset queryName = "OrderTerms">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT isNull(Code, '') 
		       + '^' + isNull(Description, '')
		AS DataString 
		FROM OrderTerms
	</cfquery>
	<cfset queryColumnList = "Code,Description">
</cfif>