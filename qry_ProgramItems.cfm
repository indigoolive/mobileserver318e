<cfif j eq "ProgramItems">
	<cfset queryName = "ProgramItems">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar, ProgramID) 
		       + '^' + CONVERT(varchar, isNull(ItemID, 0)) 
		       + '^' + CONVERT(varchar, isNull(Quantity, 0))
		AS DataString 
		FROM ProgramItems
	</cfquery>
	<cfset queryColumnList = "ProgramID,ItemID,Quantity">
</cfif>