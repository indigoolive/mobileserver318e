<cfif j eq "AccountAttributes">
	<cfset ClientDesignator = "C0">
	<cfset queryName = "AccountAttributes">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar(50), AccountID) 
		       + '^' + CONVERT(varchar(50), RIGHT(AttributeName, len(AttributeName) - len('#ClientDesignator#')))
		       + '^' + CONVERT(varchar(50), AttributeValue)  AS DataString
		  FROM AccountAttributes
		 WHERE AccountID in (SELECT AccountID
				       FROM DistributorAccountReps
				      WHERE DistRepID = #URL.ID#
					AND DistributorID = #getDistributor.DistributorID#)
		   AND AttributeName like '#ClientDesignator#%'
	</cfquery>
<cfset queryColumnList = "AccountID,AttributeName,AttributeValue">
</cfif>