<cfif j eq "Accounts">
	<cfset queryName = "getAccounts">
	
	<cfquery datasource="#attributes.datasource#" name="CalcBalanceDue">
		select AttributeValue from AccountAttributes where AccountID = #GetDistributor.DistributorID# and AttributeName='CalcBalanceDue'
	</cfquery>
	
	<cfif CheckManager.RecordCount GT 0 >
		<cfquery datasource="#attributes.datasource#" name="querytemp">
			SELECT * 
			FROM Accounts, DistributorAccountReps, 
			    DistributorAccounts
			WHERE DistributorAccountReps.DistRepID in( #ValueList(CheckManager.AccountID)# ) AND 
			    Accounts.AccountID = DistributorAccountReps.AccountID AND 
			    DistributorAccounts.DistributorID = DistributorAccountReps.DistributorID
			     AND DistributorAccounts.AccountID = DistributorAccountReps.AccountID
				and (Accounts.deletedflag is null or Accounts.deletedflag = 0)
			ORDER BY companyname
		</cfquery>
	<cfelse>
		<cfquery datasource="#attributes.datasource#" name="querytemp">
			SELECT distinct Accounts.accountID,
				deliverynotes,
				accounts.deliverydays,
				accountnumber,
				address1,
				address2,
				DistributorAccountReps.apptdays,
				city,
				companyname,
				contactname,
				altcontactname,
				distrepID,
				email,
				liquorlicensenumber,
				nosplitcasecharge,
				phone,
				fax,
				pricingtier,
				state,
				zip,
				acctstatus,
				distributoraccountreps.ApptTime1,
				distributoraccountreps.ApptTime2,
				distributoraccountreps.ApptTime3,
				distributoraccountreps.ApptTime4,
				distributoraccountreps.ApptTime5,
				distributoraccountreps.ApptTime6,
				distributoraccountreps.ApptTime7,
				<cfif CalcBalanceDue.AttributeValue NEQ "1">
					isNull( BalanceDue,'0') BalanceDue,
				<cfelse>
					(select isnull(sum(InvoiceAmount - AmountCollected),0) from AccountsReceivable where AccountID = Accounts.AccountID) BalanceDue,
				</cfif>
				CASE WHEN (LastSaleDate is NULL or LastSaleDate = '1900-01-01 00:00:00.000')
					THEN convert(varchar, (SELECT isNULL(max(transdate), '1970-01-01 00:00:00') from masterorder where AccountID = Accounts.AccountID and SiteID = 1), 120)
					ELSE convert(varchar,LastSaleDate,120)
				END AS LastOrder,
				CASE WHEN (LastPaymentDate is NULL or LastPaymentDate = '1900-01-01 00:00:00.000')
					THEN '1970-01-01 00:00:00'
					ELSE convert(varchar,LastPaymentDate,120)
				END AS LastPaymentDate,
				isNull( YTDSales,'0') YTDSales,
				isNull( YTDReturns,'0') YTDReturns,
				isNull( LYTDSales,'0') LYTDSales,
				isNull( LYTDReturns,'0') LYTDReturns,
				isNull( OnOrderBalance,'0') OnOrderBalance,
				isNull( CreditLimit,'0') CreditLimit,
				isNull( (select creditdescription from creditterms where distributorid = #getDistributor.DistributorID# and distcreditid = Accounts.creditterms),' ') CreditDescription,
				isNull( BillToCompany, ' ') BillToCompany,
				isNull( BillToContact, ' ') BillToContact,
				isNull( BillToAddress1, ' ') BillToAddress1,
				isNull( BillToAddress2, ' ') BillToAddress2,
				isNull( BillToCity, ' ') BillToCity,
				isNull( BillToState, ' ') BillToState,
				isNull( BillToZip, ' ') BillToZip,
				isNull( BillToPhone, ' ') BillToPhone,
				isNull( BillToFax, ' ') BillToFax,
				CustomerType,
				isNull (Warehouse, '0') Warehouse
			FROM Accounts,
				DistributorAccountReps, 
				DistributorAccounts
			WHERE DistributorAccountReps.DistRepID = #url.id#
				AND DistributorAccountReps.Salesman = (Select min(salesman) from DistributorAccountReps where AccountID = accounts.accountid and distrepid = #url.id#)
				AND Accounts.AccountID = DistributorAccountReps.AccountID
				AND DistributorAccounts.DistributorID = DistributorAccountReps.DistributorID
				AND DistributorAccounts.AccountID = DistributorAccountReps.AccountID
				AND (Accounts.deletedflag is null or Accounts.deletedflag = 0)
			ORDER BY companyname
		</cfquery>
	</cfif>
	
<cfset queryColumnList = "accountID,Delivdays,accountnumber,address1,address2,apptdays,city,AcctStatus,ApptTime1,ApptTime2,ApptTime3,ApptTime4,ApptTime5,ApptTime6,ApptTime7,BalanceDue,LastOrder,companyname,contactname,altcontactname,distrepID,email,liquorlicensenumber,nosplitcasecharge,phone,fax,pricingtier,state,zip,Comment1,Comment2,CatalogIDs,LastPaymentDate,YTDSales,YTDReturns,LYTDSales,LYTDReturns,OnOrderBalance,CreditLimit,CreditTerms,BillToCompany,BillToContact,BillToAddress1,BillToAddress2,BillToCity,BillToState,BillToZip,BillToPhone,BillToFax,CustomerType,Warehouse">
<!--- <cfset queryColumnList = "AccountID,catalogID,WorkOrderID,ItemID,CaseQTY,SingleQTY,CDistPrice,SDistPrice,LastCaseQty,LastSingleQty,LineItemNumber,SpecialFlag,DiscountAmount"> --->
<cfset queryName = "Accounts">
<cfset Accounts = QueryNew("LineData")>

<cfloop query="Querytemp">
	<!--- Get Attributes --->
    <cfset attrRoute    = "">
	<cfset attrComment1 = "">
    <cfset attrComment2 = "">
	<cfset catList      = "">

	<cfquery name="GetCatalogIDs" datasource="#attributes.datasource#">
		Select AccountCatalogs.catalogID
		 from Accounts, AccountCatalogs
		 where Accounts.AccountID   = #AccountID#
		 and Accounts.AccountID = AccountCatalogs.AccountID
		 and (Accounts.deletedflag is null or Accounts.deletedflag = 0)
	</cfquery>
	<cfloop query="GetCatalogIDs">
		<cfset catList = ListAppend(catList, GetCatalogIDs.catalogID)>
	</cfloop>
    
	<cfquery name="GetAttr" datasource="#attributes.datasource#">
		Select AttributeName, AttributeValue
		  from AccountAttributes
		 where AccountID   = #AccountID#
		   and AttributeName not like 'C0%'
	</cfquery>

	<cfloop query="GetAttr">
		<cfset "attr#AttributeName#" = AttributeValue>
	</cfloop>

    <!--- jkl_setting attrcomment2 equal to delnote after running thru GetAttr so not overwritten with acctattr value as per andrew's advise --->
    <cfif querytemp.deliverynotes neq "">
        <cfset attrComment2 = querytemp.deliverynotes>
    </cfif>
	
    <!--- Break up route and stop for 000-000 rte/stop for roadnet type routing--->
	<cfif Len(attrRoute) eq 7 AND mid(attrroute,4,1) EQ "-">
		<cfset attrComment1 = "Rte:" & left(attrRoute, 3) & " Stp:" & left(listgetat(attrRoute,2,"-"),2) & " Inv:" & Trim(attrLastInvoice) & " Amt:" & Trim(attrLastInvoiceAmount)>
	</cfif>
    
    <!--- Compatibility Layer --->
    <cfif attrComment1 EQ "" and attrRoute NEQ "">
        <cfset attrComment1 = attrRoute>
    </cfif>
    
	<CFSET temp = QueryAddRow(Accounts)>
	<cfset tLineData = queryTemp.AccountID & '^' &
    	queryTemp.DeliveryDays & '^' &
    	queryTemp.AccountNumber & '^' &
	    queryTemp.Address1 & '^' &
    	queryTemp.Address2 & '^' & 
		queryTemp.apptdays & '^' &
		queryTemp.city & '^' & 
		querytemp.acctstatus & '^' & 
		querytemp.appttime1 & '^' & 
		querytemp.appttime2 & '^' & 
		querytemp.appttime3 & '^' & 
		querytemp.appttime4 & '^' & 
		querytemp.appttime5 & '^' & 
		querytemp.appttime6 & '^' & 
		querytemp.appttime7 & '^' & 
		queryTemp.BalanceDue & '^' &
		queryTemp.LastOrder & '^' &
		queryTemp.companyname & '^' &
		queryTemp.contactname & '^' &
		queryTemp.altcontactname & '^' &
		queryTemp.distrepID & '^' &
		queryTemp.email & '^' &
		queryTemp.liquorlicensenumber & '^'
		>
		<cfif queryTemp.nosplitcasecharge NEQ "">
			<cfset tLineData = tLineData & queryTemp.nosplitcasecharge & '^'>
		<cfelse>
			<cfset tLineData = tLineData & "0" & '^'>
		</cfif>
		<cfset tLineData = tLineData & 
		queryTemp.phone & '^' &
		queryTemp.fax & '^' &
		queryTemp.pricingtier & '^' &
		queryTemp.state & '^' &
		queryTemp.zip
		>
    	<cfset tLineData = tLineData & '^' & attrComment1>
	<cfset tLineData = tLineData & '^' & attrComment2>
	<cfset tLineData = tLineData & '^' & catList>
	<cfset tLineData = tLineData & '^' & queryTemp.LastPaymentDate>
	<cfset tLineData = tLineData & '^' & queryTemp.YTDSales>
	<cfset tLineData = tLineData & '^' & queryTemp.YTDReturns>
	<cfset tLineData = tLineData & '^' & queryTemp.LYTDSales>
	<cfset tLineData = tLineData & '^' & queryTemp.LYTDReturns>
	<cfset tLineData = tLineData & '^' & queryTemp.OnOrderBalance>
	<cfset tLineData = tLineData & '^' & queryTemp.CreditLimit>
	<cfset tLineData = tLineData & '^' & queryTemp.CreditDescription>
	<cfset tLineData = tLineData & '^' & queryTemp.BillToCompany>
	<cfset tLineData = tLineData & '^' & queryTemp.BillToContact>
	<cfset tLineData = tLineData & '^' & queryTemp.BillToAddress1>
	<cfset tLineData = tLineData & '^' & queryTemp.BillToAddress2>
	<cfset tLineData = tLineData & '^' & queryTemp.BillToCity>
	<cfset tLineData = tLineData & '^' & queryTemp.BillToState>
	<cfset tLineData = tLineData & '^' & queryTemp.BillToZip>
	<cfset tLineData = tLineData & '^' & queryTemp.BillToPhone>
	<cfset tLineData = tLineData & '^' & queryTemp.BillToFax>
	<cfset tLineData = tLineData & '^' & queryTemp.CustomerType>
	<cfset tLineData = tLineData & '^' & queryTemp.Warehouse>
	<cfset temp = QuerySetCell(Accounts, "LineData", tLineData)>
</cfloop> 
</cfif>