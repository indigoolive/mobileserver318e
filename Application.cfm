<cfapplication name="MobileServer" sessionmanagement="Yes" setclientcookies="No" sessiontimeout="#createtimespan(0,0,1,0)#">
<cfsetting enablecfoutputonly="Yes">
<cfset compressedOutput = "">
<cfset currentversion = "3.18e">
<cfset masterDataSource = "bev_master">

<cfinclude template="\inc_servername.cfm">

<!--- Connection Tracker --->
<cflock timeout="20" throwontimeout="No" name="B1_Connection_Tracker" type="EXCLUSIVE">
	<cfparam name="SERVER.sessiontracker" default="#StructNew()#">
	<cfif isdefined("session.loginaccount")>
		<cfset lact=#session.loginaccount#>
	<cfelse>
		<cfset lact="URL=#url.id#">
	</cfif>
	<cfif not StructKeyExists(SERVEr.sessiontracker, session.sessionid)>
		<cfset temp = StructInsert(SERVER.sessiontracker, session.sessionid, "#Now()#|#lact#", True)>
	</cfif>
</cflock>

<cfquery name="getDatasource" datasource="#masterDataSource#">
	select *
	from accounts, distributoraccounts,Distributors
	where accounts.accountID = distributoraccounts.accountID
	and distributoraccounts.distributorID = Distributors.DistributorID
	and accounts.accountID = #url.id#
	<cfif url.p NEQ masterpass>
	and password='#url.p#'
	</cfif>
	and LoginEnabled = 'Y'
</cfquery>

<cfquery name="getDistributor" datasource="#masterDataSource#">
	select DistributorID
	from DistributorAccounts
	where accountID = #url.ID#
</cfquery>


<cfif getDatasource.recordcount eq 0>
	<cfset compressedOutput = "<ERROR>Error 1000:Invalid Password</ERROR>">
	
	
	 <cfmail to="errorreport@beverageone.com" from="error2000@beverageone.com" subject="Error 1000 Invalid Password">
Error 1000:Invalid Password
Template: Application.cfm
AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
Distributor: #getDistributor.DistributorID#
Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
	</cfmail>

	<cfinclude template="act_zipoutput2.cfm">
	<cfabort>
<cfelse>
	<cfset attributes.datasource = getDatasource.datasource>
</cfif>
<cfinclude template="act_recordVersion.cfm">

<cfquery name="CheckManager" datasource="#attributes.datasource#">
	Select Distinct a.AccountID 
	From DistributorAccounts a,
		DistributorAccounts b,
		Accounts c
	where a.ManagerID = b.accountID
		and b.AccountID = #URL.ID# 
		and c.AccountID = b.AccountID
		and c.Password = '#url.p#'
		and c.LoginEnabled = 'Y'
		and b.DistributorID = #getDistributor.DistributorID#
		and c.accounttype = 5
</cfquery>

<cfif CheckManager.RecordCount GT 0 >
	<cfquery datasource="#attributes.datasource#" name="check_login">
		SELECT DistributorAccountReps.AccountID,
		       DistributorAccountReps.DistributorID 
		FROM Accounts, DistributorAccountReps, 
		    DistributorAccounts
		WHERE DistributorAccountReps.DistRepID in( #ValueList(CheckManager.AccountID)# ) AND 
		    Accounts.AccountID = DistributorAccountReps.AccountID AND 
		    DistributorAccounts.DistributorID = DistributorAccountReps.DistributorID
		     AND DistributorAccounts.AccountID = DistributorAccountReps.AccountID
			 and DistributorAccountReps.DistributorID = #getDistributor.distributorID#
		ORDER BY companyname
	</cfquery>
	<cfset isManager = 1>
<cfelse>
	<cfquery datasource="#attributes.datasource#" name="check_login">
	select	DistributorAccountReps.AccountID,
		DistributorAccountReps.DistributorID
	  from	Accounts,
		DistributorAccountReps,
		Accounts ac2
	 where	Accounts.AccountID=#url.id#
	   and	Accounts.Password='#url.p#'
	   and	Accounts.LoginEnabled = 'Y'
	   and	Accounts.AccountID = DistributorAccountReps.DistRepID
	   and	DistributorAccountReps.DistributorID = #getDistributor.distributorID#
	   and	ac2.AccountID = DistributorAccountReps.AccountID
	   and	(ac2.DeletedFlag = 0 or ac2.DeletedFlag is NULL)
	</cfquery>
	<cfset isManager = 0>
</cfif>

<cfif check_login.recordcount eq 0>
	<cfset compressedOutput = "<ERROR>2000: No accounts assigned to sales rep</ERROR>">
	
	<cfmail to="errorreport@indigoolive.com" from="error2000@beverageone.com" subject="Error 2000: No accounts assigned to rep">
		Error 2000: No accounts assigned to rep
		Template: Application.cfm
		AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
		Distributor: #getDistributor.DistributorID#
		Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
		Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
	</cfmail>
	<cfinclude template="act_zipoutput2.cfm">
	<cfabort>
</cfif>

<cfquery name="options" datasource="#masterDataSource#">
	select *
	from DistributorOptions
	where distributorID = #getDistributor.DistributorID#
</cfquery>