<cfif j eq "Program">
	<cfset queryName = "Program">
	<cfquery datasource="#attributes.datasource#" name="#queryName#">
		SELECT CONVERT(varchar, ProgramID) 
		       + '^' + isNull(Name, '') 
		       + '^' + isNull(Description, '')
		AS DataString 
		FROM Program
	</cfquery>
	<cfset queryColumnList = "ProgramID,Name,Description">
</cfif>