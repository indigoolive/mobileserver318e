<!--- <cfmodule template="/Common/other/logger.cfm" accountID="#url.ID#" accounttype="#getDatasource.accounttype#" moduletype="1"> --->
<cfsetting enablecfoutputonly="Yes">
<cfif isDefined("form.fieldnames")>

	<cftry>
		<cfquery name="GetRepCustomerAssoc" datasource="#attributes.datasource#">
			Select AccountID
			from DistributorAccountReps
			where DistRepID = #url.ID#
			and AccountID = #form.a#
		</cfquery>

		<cfif GetRepCustomerAssoc.RecordCount EQ 0>
			<cfthrow type = "Rep.Customer.Assoc"
			message = "AccountID #form.a# is not longer assigned to Rep #url.id#">
		</cfif>


		<!--- Check for form.T --->
		<cfif not isdefined("form.t")>
			<cfthrow type = "Data.Transmission.Error"
			message = "Item count (Form.T) missing from Order">
		</cfif>

		<!--- Handle No Sale Orders --->
		<cfif evaluate(form.T) EQ 0>
			
			<cfif form.D eq "No Sale" OR form.D eq "No Visit" OR form.D eq "No Call">
				
				<cfif form.F eq "">
					<cfset UseDateTime = "null">
				<cfelse>
					<cfset UseDateTime = CreateODBCDateTime(form.F)>
				</cfif>
				
				<cfquery name="addorder" datasource="#attributes.datasource#">
					insert into MasterOrder(accountID,
					     CustomerType,
						 dsraccount,
						 siteID,
						 discount,
						 tax,
						 transdate,
						 activatedate,
						 fullfilleddate,
						 OrderNotes,
						 SpecialFlag,
						 EntryDate,
						 Option1,
						 Option2,
						 Option3,
						 Option4,
						 Option5,
						 Option6,
						 Option7,
						 Option8,
						 Option9,
						 Option10,
						 ShipMethod )
					values ( #form.A#,
					0,
					#url.ID#,
					1,
					0,
					0,
					GetDate(),
					GetDate(),
					GetDate(),
					<cfif isdefined("form.c")>
						'#replace(form.C, "'", "''", "ALL")#',
					<cfelse>
						'',
					</cfif>
					'#form.d#',
					#usedatetime#,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0,
					0 )
				</cfquery>

				<cfquery name="GetOrderNumber" datasource="#attributes.datasource#">
					Select max(OrderNumber) NewOrderNumber From MasterOrder where AccountId = #form.a# and DsrAccount = #url.ID#
				</cfquery>
				<cfset useOrderNumber = GetOrderNumber.NewOrderNumber>
				<cfset compressedOutput = "<OK>" & useOrderNumber & "</OK>">
			</cfif>
		<cfelse>

			<cfscript>
				masterOrderStruct = structNew();
				masterOrderItemsStruct = structNew();

				if(isDefined("form.A"))
				{
					structInsert(masterOrderStruct, "accountID", form.A);
				}
				else
				{
					structInsert(masterOrderStruct, "accountID", '');
				}


				if( form.B neq "")
				{
					structInsert(masterOrderStruct, "activateDate", form.B);
				}
				else
				{
					structInsert(masterOrderStruct, "activateDate", Now());
				}

				if(isDefined("form.C"))
				{
					structInsert(masterOrderStruct, "ordernotes", form.C);
				}
				else
				{
					structInsert(masterOrderStruct, "ordernotes", '');
				}

				// This is the Special Flag or PO Number
				if(isDefined("form.D"))
				{
					structInsert(masterOrderStruct, "orderspecial", form.D);
				}
				else
				{
					structInsert(masterOrderStruct, "orderspecial", '');
				}

				// GMontana Special Credit Terms
				// Polaner CO-OP Qty
				if(isDefined("form.E"))
				{
					structInsert(masterOrderStruct, "Attr2", form.E);
				}
				else
				{
					structInsert(masterOrderStruct, "Attr2", '');
				}

				if(isDefined("form.F"))
				{
					structInsert(masterOrderStruct, "DateTimeStamp", form.F);
				}
				else
				{
					structInsert(masterOrderStruct, "DateTimeStamp", Now());
				}

				if(isDefined("form.G"))
				{
					structInsert(masterOrderStruct, "Warehouse", form.G);
				}
				else
				{
					structInsert(masterOrderStruct, "Warehouse", '');
				}
				
				// GroupMontana Discount Code
				if(isDefined("form.H"))
				{
					structInsert(masterOrderStruct, "Attr3", form.H);
				}
				else
				{
					structInsert(masterOrderStruct, "Attr3", '');
				}

				// This is the Cancel Date
				if(isDefined("form.I"))
				{
					structInsert(masterOrderStruct, "Attr1", form.I);
				}
				else
				{
					structInsert(masterOrderStruct, "Attr1", '');
				}

				if (isDefined("form.J"))
				{
					Credit = form.J;
					CCNumber = Trim(ListGetAt(Credit, 1, "~"));
					CCExpDate = Trim(ListGetAt(Credit, 2, "~"));
					CCExpDate = Replace(CCExpDate, "/", "", "ALL");

					if(isDefined("CCNumber"))
					{
							structInsert(masterOrderStruct, "CCNumber", CCNumber);
					}
					else
					{
							structInsert(masterOrderStruct, "CCNumber", '');
					}
					if(isDefined("CCExpDate"))
					{
							structInsert(masterOrderStruct, "CCExpDate", CCExpDate);
					}
					else
					{
							structInsert(masterOrderStruct, "CCExpDate", '');
					}
				}
				else
				{
					structInsert(masterOrderStruct, "CCNumber", '');
					structInsert(masterOrderStruct, "CCExpDate", '');
				}

				if(isDefined("form.K")) 
				{
					structInsert(masterOrderStruct, "ShipMethod", form.K);
				} 
				else 
				{
					structInsert(masterOrderStruct, "ShipMethod", 0);
				}
				
				if (isDefined("form.N"))
				{
					structInsert(masterOrderStruct, "SampleType", form.N);
				} 
				else 
				{
					structInsert(masterOrderStruct, "SampleType", 0);
				}

				if(isDefined("form.S"))
				{
					structInsert(masterOrderStruct, "Signature", form.S);
				}
				else
				{
					structInsert(masterOrderStruct, "Signature", '');
				}

				if(isDefined("form.KK"))
				{
					Opt = replace(evaluate("form.KK"), "||", "|0|", "ALL");
					Opt = replace(Opt, "||", "|0|", "ALL");
					Opt = replace(Opt, "||", "|0|", "ALL");
					Opt = replace(Opt, "||", "|0|", "ALL");
					Opt = replace(Opt, "||", "|0|", "ALL");
					Opt = replace(Opt, "||", "|0|", "ALL");
					Opt = replace(Opt, "||", "|0|", "ALL");
					Opt = replace(Opt, "||", "|0|", "ALL");
					Opt = replace(Opt, "||", "|0|", "ALL");
					Opt = replace(Opt, "||", "|0|", "ALL");
					structInsert(masterOrderStruct, "Option1", listgetat(Opt, 1, "|"));
					structInsert(masterOrderStruct, "Option2", listgetat(Opt, 2, "|"));
					structInsert(masterOrderStruct, "Option3", listgetat(Opt, 3, "|"));
					structInsert(masterOrderStruct, "Option4", listgetat(Opt, 4, "|"));
					structInsert(masterOrderStruct, "Option5", listgetat(Opt, 5, "|"));
					structInsert(masterOrderStruct, "Option6", listgetat(Opt, 6, "|"));
					structInsert(masterOrderStruct, "Option7", listgetat(Opt, 7, "|"));
					structInsert(masterOrderStruct, "Option8", listgetat(Opt, 8, "|"));
					structInsert(masterOrderStruct, "Option9", listgetat(Opt, 9, "|"));
					structInsert(masterOrderStruct, "Option10", listgetat(Opt, 10, "|"));
				}
				else
				{
					structInsert(masterOrderStruct, "Option1", 0);
					structInsert(masterOrderStruct, "Option2", 0);
					structInsert(masterOrderStruct, "Option3", 0);
					structInsert(masterOrderStruct, "Option4", 0);
					structInsert(masterOrderStruct, "Option5", 0);
					structInsert(masterOrderStruct, "Option6", 0);
					structInsert(masterOrderStruct, "Option7", 0);
					structInsert(masterOrderStruct, "Option8", 0);
					structInsert(masterOrderStruct, "Option9", 0);
					structInsert(masterOrderStruct, "Option10", 0);
				}

				//structInsert(masterOrderStruct, "transdate", Now());
				for(i=1; i lte form.t; i=i+1)
				{
					"itemStruct#i#" = StructNew();
					itemString = replace(evaluate("form.L#i#"), "^^", "^ ^", "ALL");
					itemString = replace(itemString, "^^", "^ ^", "ALL");
					itemString = replace(itemString, "^^", "^ ^", "ALL");
					itemString = replace(itemString, "^^", "^ ^", "ALL");
					itemString = replace(itemString, "^^", "^ ^", "ALL");
					itemString = replace(itemString, "^^", "^ ^", "ALL");
					itemString = replace(itemString, "^^", "^ ^", "ALL");
					itemString = replace(itemString, "^^", "^ ^", "ALL");
					itemString = replace(itemString, "^^", "^ ^", "ALL");
					itemString = replace(itemString, "^^", "^ ^", "ALL");
					itemString = replace(itemString, "^^", "^ ^", "ALL");
					itemString = itemString & " ";
					
							//Pull out the values
					itemID=listgetat(itemString, 1, "^");
					cqty=listgetat(itemString, 2, "^");
					sqty=listgetat(itemString, 3, "^");
					cprice=listgetat(itemString, 4, "^");
					sprice=listgetat(itemString, 5, "^");
					Special=listgetat(itemString, 6, "^");
					Discount=listgetat(itemString, 7, "^");
					override=listgetat(itemString, 8, "^");
					par=listgetat(itemString, 9, "^");
					inv=listgetat(itemString, 10, "^");
					ExtPrice=listgetat(itemString, 11, "^");
					RetailPrice=listgetat(itemString, 12, "^");
					LineWarehouse=listgetat(itemString, 13, "^");
					LineComment=listgetat(itemString, 14, "^");
					if( trim(ExtPrice) EQ "")
						ExtPrice = (cqty * cprice) + (sqty * sprice);
					
							//Add to structure
					StructInsert(evaluate("itemStruct#i#"), "itemID", itemID );
					StructInsert(evaluate("itemStruct#i#"), "cprice", cprice );
					StructInsert(evaluate("itemStruct#i#"), "sprice", sprice );
					StructInsert(evaluate("itemStruct#i#"), "cqty", cqty );
					StructInsert(evaluate("itemStruct#i#"), "sqty", sqty );
					StructInsert(evaluate("itemStruct#i#"), "Line", i);
					StructInsert(evaluate("itemStruct#i#"), "Discount", trim(Discount) );
					StructInsert(evaluate("itemStruct#i#"), "Override", trim(override) );
					StructInsert(evaluate("itemStruct#i#"), "Special", Special );
					StructInsert(evaluate("itemStruct#i#"), "Par", par );
					StructInsert(evaluate("itemStruct#i#"), "Inv", inv );
					StructInsert(evaluate("itemStruct#i#"), "ExtPrice", ExtPrice);
					StructInsert(evaluate("itemStruct#i#"), "RetailPrice", RetailPrice);
					StructInsert(evaluate("itemStruct#i#"), "LineWarehouse", LineWarehouse);
					StructInsert(evaluate("itemStruct#i#"), "LineComment", LineComment);
					StructInsert(masterOrderItemsStruct, "itemID#i#", evaluate("itemStruct#i#"));
				}
			</cfscript>

		 	<cfif masterOrderStruct["DateTimeStamp"] eq "">
				<cfset UseDateTime = "null">
			<cfelse>
				<cfset UseDateTime = CreateODBCDateTime(masterOrderStruct["DateTimeStamp"])>
			</cfif>

			<cfquery name="checkForOrder" datasource="#attributes.datasource#">
				select ordernumber
				from masterorder
				where entrydate = #UseDateTime#
				and dsraccount = #url.id#
				and accountID = #masterOrderStruct["accountID"]#
			</cfquery>
			
			<cfquery name="GetPremise" datasource="#attributes.datasource#">
				select CustomerType as Premise from Accounts where AccountID = #masterOrderStruct["accountID"]#
			</cfquery>
			
			<cfquery name="GetSalesman" datasource="#attributes.datasource#">
				select min(Salesman) Salesman 
				from DistributorAccountReps where
				DistRepID = #url.ID# 
				and AccountID = #masterOrderStruct["accountID"]#
			</cfquery>
			
			<cfset CustomerType = GetPremise.Premise>
			<cfif getDistributor.DistributorID eq "109696" AND masterOrderStruct["Option2"] EQ 1>
				<cfset CustomerType = 1>
			</cfif>

			replace(masterOrderStruct["ordernotes"], "'", "''", "ALL")
			
			<cfif LEN(masterOrderStruct["ordernotes"]) GT 254>
				<cfset masterOrderStruct["ordernotes"] = LEFT(masterOrderStruct["ordernotes"], 254)>
			</cfif>

			
			<cfif checkForOrder.recordcount GTE 1>
				<cfset compressedOutput = "<OK>" & checkForOrder.ordernumber & "</OK>">
			<cfelse>
				<cftransaction> 
		
				<cfquery name="insertMaster" datasource="#attributes.datasource#">
					insert into MasterOrder
						(AccountID, 
						CustomerType, 
						dsraccount, 
						siteID, 
						discount, 
						tax, 
						transdate,
						transmitdate, 
						activatedate, 
						OrderNotes
						<cfif options.SelectWhseOnOrder>, Warehouse </cfif>, 
						SpecialFlag,  
						EntryDate, 
						Option1, 
						Option2, 
						Option3, 
						Option4, 
						Option5, 
						Option6, 
						Option7, 
						Option8, 
						Option9, 
						Option10, 
						ShipMethod,
						Attr1,
						Attr2,
						Attr3,
						CCNumber,
						CCExpDate,
						DistInvoice,
						SampleType)
					values 
						(#masterOrderStruct["accountID"]#, 
						#CustomerType#, 
						#url.ID#, 
						1, 
						0, 
						0, 
						GetDate(),
						GetDate(), 
						#createodbcdate(masterOrderStruct["activatedate"])#,
						'#masterOrderStruct["ordernotes"]#'
						<cfif options.SelectWhseOnOrder>,'#replace(masterOrderStruct["warehouse"], "'", "''", "ALL")#'</cfif>,
						'#replace(masterOrderStruct["orderspecial"], "'", "''", "ALL")#', 
						#UseDateTime#, 
						#masterOrderStruct["Option1"]#, 
						#masterOrderStruct["Option2"]#, 
						#masterOrderStruct["Option3"]#, 
						#masterOrderStruct["Option4"]#, 
						#masterOrderStruct["Option5"]#, 
						#masterOrderStruct["Option6"]#, 
						#masterOrderStruct["Option7"]#, 
						#masterOrderStruct["Option8"]#, 
						#masterOrderStruct["Option9"]#, 
						#masterOrderStruct["Option10"]#, 
						#masterOrderStruct["ShipMethod"]#,
						'#replace(masterOrderStruct["Attr1"], "'", "''", "ALL")#',
						'#replace(masterOrderStruct["Attr2"], "'", "''", "ALL")#',
						'#replace(masterOrderStruct["Attr3"], "'", "''", "ALL")#',
						'#replace(masterOrderStruct["CCNumber"], "'", "''", "ALL")#',
						'#masterOrderStruct["CCExpDate"]#',
						'',
						#masterOrderStruct["SampleType"]#)
				</cfquery>

				<cfquery name="GetOrderNumber" datasource="#attributes.datasource#">
					Select max(OrderNumber) NewOrderNumber 
					From MasterOrder 
					where AccountId = #masterOrderStruct["accountID"]# 
					and DsrAccount = #url.ID# 
				</cfquery>
				<cfset useOrderNumber = GetOrderNumber.NewOrderNumber>
				<cfif masterOrderStruct["Signature"] NEQ "">
					<cfquery name="InsertSignature" datasource="#attributes.datasource#">
						insert into MasterOrderSignatures 
							(OrderNumber, Signature)
						values
							(#useOrderNumber#, '#masterOrderStruct["Signature"]#')
					</cfquery>
				</cfif>

				<cfset totalPrice = 0>
				<cfset netprice = 0>
				<cfset OutOfStockItems = "">
				<cfset ApprovalReason = "">
				<cfset ApprovalRequired = 0>
				<cfloop collection="#masterOrderItemsStruct#" item="i">
					<cfset ApprovalReason = "">
					<cfquery name="getExtendedInfo" datasource="#attributes.datasource#">
						SELECT DI.DistributorSKU, 
							DI.Description, 
							isnull(DI.QTYPerCase,1) QTYPerCase, 
							isnull(DI.QTYOnHand,0) QTYOnHand, 
							isnull(WHI.Parlevel,0) Parlevel,
							isnull(AI1.ApportionedQuantity,0) CustomerApportion, 
							isnull(AI2.ApportionedQuantity,0) RepApportion,
							isnull(WHI.reserveqty,0) reserveqty
						FROM DistributorItems DI
						LEFT OUTER JOIN ApportionedItems AI1 
							ON (AI1.ItemID = DI.ItemID AND AI1.AccountID = #masterOrderStruct["accountID"]#)
						LEFT OUTER JOIN ApportionedItems AI2 
							ON (AI2.ItemID = DI.ItemID AND AI2.AccountID = #url.ID#)
						LEFT OUTER JOIN WareHouseItems WHI 
							ON (DI.ItemID = WHI.ItemID)
						WHERE DI.ItemID = #masterOrderItemsStruct[i].itemID#
					</cfquery>

					<cfif getExtendedInfo.RecordCount GT 0>
						<cfif (options.OnReserveApproval) and (evaluate(masterOrderItemsStruct[i].cqty * getExtendedInfo.QtyPerCase + masterOrderItemsStruct[i].sqty) gt evaluate("getExtendedInfo.QtyOnHand - getExtendedInfo.parlevel - getExtendedInfo.reserveqty") and (getExtendedInfo.reserveqty neq 0))>
							<cfset ApprovalReason = ApprovalReason & "This rep has no apportioned qty for this item: #getExtendedInfo.description#">
							<cfset ApprovalRequired = 1>
						<cfelseif options.SelectWhseOnOrder>
							<cfquery name="getWhseInvInfo" datasource="#attributes.datasource#">
								select qtyonhand, parlevel
								from warehouseitems
								where itemID = #masterOrderItemsStruct[i].itemID#
								and warehouse = #masterOrderStruct["warehouse"]#
							</cfquery>
							<cfif (getWhseInvInfo.recordcount neq 0) and (evaluate(masterOrderItemsStruct[i].cqty * getExtendedInfo.QtyPerCase + masterOrderItemsStruct[i].sqty) gt evaluate("getWhseInvInfo.QtyOnHand - getWhseInvInfo.parlevel"))>
								<cfset ApprovalReason = ApprovalReason & "Insufficient inventory in selected warehouse (#getWhseInvInfo.QtyOnHand# in stock).">
								<cfset ApprovalRequired = 1>
							</cfif>
						<cfelseif (options.OutOfStockApproval) and (evaluate(masterOrderItemsStruct[i].cqty * getExtendedInfo.QtyPerCase + masterOrderItemsStruct[i].sqty) gt evaluate("getExtendedInfo.QtyOnHand - getExtendedInfo.parlevel"))>
							<cfset ApprovalReason = ApprovalReason & "Insufficient inventory (#getExtendedInfo.QtyOnHand# in stock).">
							<cfset ApprovalRequired = 1>
						</cfif>
	
						<cfif (options.CustApportionApproval) and (getExtendedInfo.CustomerApportion neq "")>
							<cfset apportionDecrement = masterOrderItemsStruct[i].cqty * getExtendedInfo.QtyPerCase + masterOrderItemsStruct[i].sqty>
							<cfif apportionDecrement gt getExtendedInfo.CustomerApportion>
								<cfset ApprovalReason = ApprovalReason & "exceeds customer apportionment for " & getExtendedInfo.description>
								<cfset ApprovalRequired = 1>
							</cfif>
							<cfif approvalreason EQ "">
								<cfquery name="decrementApportionMent" datasource="#attributes.datasource#">
								update apportionedItems
								set ApportionedQuantity = <cfif apportionDecrement gt getExtendedInfo.CustomerApportion>0<cfelse>#evaluate("getExtendedInfo.CustomerApportion - apportionDecrement")#</cfif>
								where accountID = #masterOrderStruct["accountID"]#
								and itemID = #masterOrderItemsStruct[i].itemID#
								</cfquery>
							</cfif>
						</cfif>
						
						<cfif (options.RepApportionApproval) and (getExtendedInfo.RepApportion neq "")>
							<cfset apportionDecrement = masterOrderItemsStruct[i].cqty * getExtendedInfo.QtyPerCase + masterOrderItemsStruct[i].sqty>
							<cfif apportionDecrement gt getExtendedInfo.RepApportion>
								<cfset ApprovalReason = ApprovalReason & "exceeds Rep apportionment for " & getExtendedInfo.description>
							<cfset ApprovalRequired = 1>
							</cfif>
							<cfif approvalreason EQ "">
								<cfquery name="decrementApportionMent" datasource="#attributes.datasource#">
									update apportionedItems	set ApportionedQuantity = <cfif apportionDecrement gt getExtendedInfo.RepApportion>0<cfelse>#evaluate("getExtendedInfo.RepApportion - apportionDecrement")#</cfif>
									where accountID = #url.ID#
									and itemID = #masterOrderItemsStruct[i].itemID#
								</cfquery>
							</cfif>
						</cfif>
	
						<cfset TotalSingleQTY = (masterOrderItemsStruct[i].cqty * getExtendedInfo.QtyPerCase) + masterOrderItemsStruct[i].sqty>
						<cfquery name="InsertItem" datasource="#attributes.datasource#">
							insert into MasterOrderItems(ordernumber, itemID, cqty, sqty, cprice, sprice, extprice, distributorID, qtypercase, distributorSKU, Description,LineItemNumber, LineComment<cfif options.showPar>,par</cfif><cfif options.showInv>,inv</cfif>,DistrepID<cfif options.AdminApproval>, ApprovalReason</cfif>, SalesMan)
							values (#useOrderNumber#, #masterOrderItemsStruct[i].itemID#, #masterOrderItemsStruct[i].cqty#, #masterOrderItemsStruct[i].sqty#, #masterOrderItemsStruct[i].cprice#, #masterOrderItemsStruct[i].sprice#, #masterOrderItemsStruct[i].extprice#, #check_login.distributorID#, #getExtendedInfo.QtyPerCase#, '#getExtendedInfo.DistributorSKU#', '#getExtendedInfo.Description#',#masterOrderItemsStruct[i].line#, '#masterOrderItemsStruct[i].LineComment#'<cfif options.showPar>,#masterOrderItemsStruct[i].par#</cfif><cfif options.showInv>,#masterOrderItemsStruct[i].inv#</cfif>,#url.ID#<cfif options.AdminApproval>, '#approvalreason#'</cfif>, #GetSalesman.Salesman#)
						</cfquery>

						<cfif (options.ShowDiscount) and (masterOrderItemsStruct[i].discount neq "" and masterOrderItemsStruct[i].discount neq 0)>
	        	            			<cfset tDiscount = replace(masterOrderItemsStruct[i].discount, "'", "''", "ALL")>
		        	        		<cfif tDiscount EQ -1><cfset tDiscount = "FREE"></cfif>
		                			<cfif tDiscount EQ -2><cfset tDiscount = "BTG"></cfif>
							
							<cfquery name="insertDiscount" datasource="#attributes.datasource#">
								insert into MasterOrderItemAttributes(ordernumber, LineItemNumber, AttributeName, AttributeValue)
								values (#useOrderNumber#, #masterOrderItemsStruct[i].line#, 'DiscountAmount', '#tDiscount#')
							</cfquery>
							<cfif options.MinDiscountApproval gt 0>
								<cfset ApprovalReason = ApprovalReason & getExtendedInfo.description & " is manually discounted.">
								<cfset ApprovalRequired = 1>
							</cfif>
						</cfif>

						<cfif (options.ShowSpecialFlag) and (trim(masterOrderItemsStruct[i].special) neq "" and masterOrderItemsStruct[i].special neq 0)>
							<cfquery name="insertSpecial" datasource="#attributes.datasource#">
								insert into MasterOrderItemAttributes(ordernumber, LineItemNumber, AttributeName, AttributeValue)
								values (#useOrderNumber#, #masterOrderItemsStruct[i].line#, 'SpecialFlag', '#replace(masterOrderItemsStruct[i].special, "'", "''", "ALL")#')
							</cfquery>
							<cfif (options.MinCodeApproval gt 0) and (isNumeric(masterOrderItemsStruct[i].special) and masterOrderItemsStruct[i].special gte options.MinCodeApproval)>
								<cfset ApprovalReason = ApprovalReason & "Rep requesting Discount Code above #evaluate("options.MinCodeApproval-1")# for " & getExtendedInfo.description>
								<cfset ApprovalRequired = 1>
							</cfif>
						</cfif>
						
						<cfif (options.PriceOverride) and (masterOrderItemsStruct[i].override neq "" and masterOrderItemsStruct[i].override neq 0)>
				                        <cfset tOverride = replace(masterOrderItemsStruct[i].override, "'", "''", "ALL")>
	                			        <cfif tOverride EQ -1><cfset tOverride = "FREE"></cfif>
				                        <cfif tOverride EQ -2><cfset tOverride = "BTG"></cfif>                        
							<cfquery name="insertOverride" datasource="#attributes.datasource#">
								insert into MasterOrderItemAttributes(ordernumber, LineItemNumber, AttributeName, AttributeValue)
								values (#useOrderNumber#, #masterOrderItemsStruct[i].line#, 'PriceOverride', '#tOverride#')
							</cfquery>
							<cfif (options.MinOverrideApproval gt 0) and (isNumeric(masterOrderItemsStruct[i].override) and masterOrderItemsStruct[i].override gte options.MinOverrideApproval)>
								<cfset ApprovalReason = ApprovalReason & "Rep requesting price override greater than $#options.MinOverrideApproval# for " & getExtendedInfo.description>
								<cfset ApprovalRequired = 1>
							</cfif>
						</cfif>

						<cfif masterOrderItemsStruct[i].RetailPrice neq "" and masterOrderItemsStruct[i].RetailPrice neq 0>
							<cfquery name="insertRetail" datasource="#attributes.datasource#">
								insert into MasterOrderItemAttributes(ordernumber, LineItemNumber, AttributeName, AttributeValue)
								values (#useOrderNumber#, #masterOrderItemsStruct[i].line#, 'RetailPrice', '#replace(masterOrderItemsStruct[i].retailprice, "'", "''", "ALL")#')
							</cfquery>
						</cfif>

						<cfif masterOrderItemsStruct[i].LineWarehouse neq "" and masterOrderItemsStruct[i].LineWarehouse neq 0>
							<cfquery name="checkValidWarehouse" datasource="#attributes.datasource#">
								select Warehouse from Warehouse where Warehouse = #replace(masterOrderItemsStruct[i].LineWarehouse, "'", "''", "ALL")#
							</cfquery>
							<cfif CheckValidWarehouse.RecordCount GT 0>
    							<cfquery name="insertWarehouse" datasource="#attributes.datasource#">
									insert into MasterOrderItemAttributes(ordernumber, LineItemNumber, AttributeName, AttributeValue)
									values (#useOrderNumber#, #masterOrderItemsStruct[i].line#, 'Warehouse', '#replace(masterOrderItemsStruct[i].LineWarehouse, "'", "''", "ALL")#')
								</cfquery>
							</cfif>
						</cfif>
	
						<cfset totalPrice = totalPrice + masterOrderItemsStruct[i].ExtPrice>  <!--- + masterOrderItemsStruct[i].cprice * masterOrderItemsStruct[i].cqty + masterOrderItemsStruct[i].sprice * masterOrderItemsStruct[i].sqty> --->
						<cfset netPrice = netPrice + (masterOrderItemsStruct[i].cprice * masterOrderItemsStruct[i].cqty) + (masterOrderItemsStruct[i].sprice * masterOrderItemsStruct[i].sqty)>
						
						<cfif (getExtendedInfo.parlevel gt 0) and (getExtendedInfo.QTYOnHand - getExtendedInfo.parlevel - TotalSingleQTY lt 0)>
							<cfset OutOfStockItems = ListAppend(OutOfStockItems, masterOrderItemsStruct[i].itemID)>
						</cfif>

						<cfif ApprovalReason NEQ "">
							<cfquery name="UpdateReason" datasource="#attributes.datasource#">
								Update MasterOrderItems 
								   set ApprovalReason = '#ApprovalReason#'
								 where OrderNumber = #useOrderNumber#
								   and LineItemNumber = #masterOrderItemsStruct[i].line#
							</cfquery>
						</cfif>

	
						<!--- Inventory --->
						<cfif IsNumeric(masterOrderItemsStruct[i].cqty)>
							<cfset cases = masterOrderItemsStruct[i].cqty>
						<cfelse>
							<cfset cases = 0>
						</cfif>
						
						<cfif IsNumeric(masterOrderItemsStruct[i].sqty)>
							<cfset sings = masterOrderItemsStruct[i].sqty>
						<cfelse>
							<cfset sings = 0>
						</cfif>
						
						<cfset totalbottles = cases * getExtendedInfo.QtyPerCase>
						<cfset totalbottles = totalbottles + sings>
						
						<cfif ApprovalRequired NEQ 1>
							<cfquery name="FindWhseItem" datasource="#attributes.datasource#">
								select warehouse
								from warehouseitems
								where itemID=#masterOrderItemsStruct[i].itemID#
								order by warehouse asc
							</cfquery>
							
							<cfquery name="UpdateInventory" datasource="#attributes.datasource#">
								Update Warehouseitems
									set UpdateDate = GetDate(),
									QtyOnHand = QtyOnHand - #totalbottles#
								where ItemID = #masterOrderItemsStruct[i].itemID#
								<cfif (options.SelectWhseOnOrder) and (masterOrderStruct["warehouse"] neq "") and isNumeric(masterOrderStruct["warehouse"])>
									and warehouse = #masterOrderStruct["warehouse"]#
								<cfelse>
									<cfif FindWhseItem.recordcount neq 0>
										and warehouse = #FindWhseItem.warehouse#
									<cfelse>
										and warehouse = 1								
									</cfif>
								</cfif>
							</cfquery>
							<cfquery name="RollUpInven"  datasource="#attributes.datasource#">
								Update DistributorItems
								set QtyOnHand = isNull((select sum(WareHouseItems.QtyOnHand) from warehouseitems where itemID = #masterOrderItemsStruct[i].itemID# and QtyOnHand > 0), 0),
								InvUpdateDate = GetDate()
								where ItemID = #masterOrderItemsStruct[i].itemID#
							</cfquery>
						</cfif>
	 					
						<cfquery name="SetLastQty"  datasource="#attributes.datasource#">
							Update WorkOrderItems
								set CLastQty = #masterOrderItemsStruct[i].cqty#,
								SLastQty = #masterOrderItemsStruct[i].sqty#
							where ItemID = #masterOrderItemsStruct[i].itemID#
							and accountID = #masterOrderStruct["accountID"]#
							and workorderID in  (select workorderID 
									from workorder 
									where accountID=#masterOrderStruct["accountID"]# 
									and distrepID=#url.ID#)
						</cfquery>
					</cfif>
				</cfloop>
				<cfset compressedOutput = "<OK>" & useOrderNumber & "</OK>">
				<cfquery name="updateMasterPrices" datasource="#attributes.datasource#">
					UPDATE masterorder
						SET totalOrder = #totalPrice#,
						netOrder = #netPrice#,
						transmitdate = NULL
					<cfif ApprovalRequired EQ 1>
						, repapprovalrequired = 1
					</cfif>
					WHERE ordernumber = #useOrderNumber#
				</cfquery>
				</cftransaction>

				<cfif ApprovalRequired EQ 1>
					<cfquery name="GetNotifyAddress"  datasource="#attributes.datasource#">
						SELECT Email
						FROM Accounts
						WHERE AccountID = #check_login.DistributorID#
					</cfquery>

					<cfif GetNotifyAddress.Email NEQ "">
						<cfmail to="#GetNotifyAddress.Email#"
							from="support@indigoolive.com"
							subject="Order #useOrderNumber# Approval Required">
							Order number #useOrderNumber# from sales person #URL.ID# is requiring approval.
						</cfmail>

						
					</cfif>


				</cfif>



			</cfif>
		</cfif>

		
		<cfcatch type="Data.Transmission.Error">
			<cfset compressedOutput = "<ERROR>Server Message: Data incomplete - possible transmission error.</ERROR>">

			<cfmail to="errorreport@indigoolive.com" from="error2000@beverageone.com" subject="Error 2002 Order transmission error">
				Error 2002: Item line count (Form T) missing from order
				Template: takeOrders.cfm
				AccountID: #url.ID#
				Received Hashed Pass: #url.p#
				Server Name: #attributes.servername#
				Type: #cfcatch.type#
				Message: #cfcatch.message#
				
				SQL: <cfif isdefined("cfcatch.sql")>#cfcatch.sql#<cfelse>N/A</cfif>
				
				Details: #cfcatch.detail#
				<CFLOOP index=i from=1 to = #ArrayLen(CFCATCH.TAGCONTEXT)#>
					<CFSET sCurrent = #CFCATCH.TAGCONTEXT[i]#>
					#i# #sCurrent["ID"]# (#sCurrent["LINE"]#,#sCurrent["COLUMN"]#) #sCurrent["TEMPLATE"]#
				</CFLOOP>

				<cfif isDefined("form.fieldnames")>
					<cfloop list="#form.fieldnames#" index="i">
						#i#: #evaluate("form.#i#")#
					</cfloop>
				</cfif>
			</cfmail>
		</cfcatch>
		
		<cfcatch type="Rep.Customer.Assoc">
			<cfset compressedOutput = "<ERROR>Server Message: Customer no longer assigned to rep.</ERROR>">

			<cfmail to="errorreport@indigoolive.com" from="error2000@beverageone.com" subject="Error 2001 Rep / Customer Assoc">
				Error 2001: Rep / Customer Assoc not valid
				Template: takeOrders.cfm
				AccountID: #url.ID#
				Received Hashed Pass: #url.p#
				Server Name: #attributes.servername#
				Type: #cfcatch.type#
				Message: #cfcatch.message#
				
				SQL: <cfif isdefined("cfcatch.sql")>#cfcatch.sql#<cfelse>N/A</cfif>
				
				Details: #cfcatch.detail#
				<CFLOOP index=i from=1 to = #ArrayLen(CFCATCH.TAGCONTEXT)#>
					<CFSET sCurrent = #CFCATCH.TAGCONTEXT[i]#>
					#i# #sCurrent["ID"]# (#sCurrent["LINE"]#,#sCurrent["COLUMN"]#) #sCurrent["TEMPLATE"]#
				</CFLOOP>

				<cfif isDefined("form.fieldnames")>
					<cfloop list="#form.fieldnames#" index="i">
						#i#: #evaluate("form.#i#")#
					</cfloop>
				</cfif>
			</cfmail>
		</cfcatch>

		<cfcatch type="any">
			<cfset compressedOutput = "<ERROR>Server Message: Error adding data to BeverageOne database.</ERROR>">

			<cfmail to="errorreport@indigoolive.com" from="error2000@beverageone.com" subject="Error 2000 Taking Orders">
				Error 2000: Taking Orders
				Template: takeOrders.cfm
				AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
				Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
				Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
				Type: #cfcatch.type#
				Message: #cfcatch.message#
				
				SQL: <cfif isdefined("cfcatch.sql")>#cfcatch.sql#<cfelse>N/A</cfif>
				
				Details: #cfcatch.detail#
				<CFLOOP index=i from=1 to = #ArrayLen(CFCATCH.TAGCONTEXT)#>
					<CFSET sCurrent = #CFCATCH.TAGCONTEXT[i]#>
					#i# #sCurrent["ID"]# (#sCurrent["LINE"]#,#sCurrent["COLUMN"]#) #sCurrent["TEMPLATE"]#
				</CFLOOP>

				<cfif isDefined("form.fieldnames")>
					<cfloop list="#form.fieldnames#" index="i">
						#i#: #evaluate("form.#i#")#
					</cfloop>
				</cfif>
			</cfmail>
		</cfcatch>
	</cftry>

<cfelse>
	<cfset compressedOutput = "<ERROR>Server Message: Order fields missing from transmission</ERROR>">
	
	<cfmail to="errorreport@indigoolive.com" from="error3000@beverageone.com" subject="Error 3000 Missing Data">
		Error 3000:missing data
		Template: takeOrders.cfm
		AccountID: <cfif isDefined("url.ID")>#url.ID#<cfelse>N/A</cfif>
		Received Hashed Pass: <cfif isDefined("url.p")>#url.p#<cfelse>N/A</cfif>
		Server Name: <cfif isdefined("attributes.servername")>#attributes.servername#</cfif>
		Type: Data transmission error
		Message: Fields missing from order
		
		<cfif isDefined("form.fieldnames")>
			<cfloop list="#form.fieldnames#" index="i">
				#i#: #evaluate("form.#i#")#
			</cfloop>
		</cfif>
	</cfmail>
</cfif>

<cfinclude template="act_zipoutput2.cfm">
